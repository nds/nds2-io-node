# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import logging
import os.path
import socket
import struct

from . import daqd
from . import sock_io


class NDS2ChanInfo:
    """
    A channel info provider that retreives its information from
    a NDS2 based channel information server.
    """

    def __init__(self, host, port):
        """
        Create a NDS2 based channel information provider.

        :param host: hostname to connect to
        :param port: port number to connect to
        """
        self.s = None
        self.host = host
        self.port = port
        self.epoch_start = None
        self.epoch_stop = None
        self.logger = logging.getLogger(__name__)
        self.__last_chan_list = None

    def __send_cmd(self, cmd):
        """
        Send a command to the channel info server.
        :param cmd: Command to send as a string, w/o a training ";\n"
        """
        self.connect()
        self.logger.debug("Sending command '{0}'".format(cmd))
        self.s.write(cmd.encode() + b";\n")
        buf = self.s.read(4)
        if buf != daqd.DAQD_OK:
            raise Exception("Command Failed")

    def __update_epoch(self, start, stop):
        """
        Update the epoch internally and on the channel info server
        [start, stop)

        :param start: start time in GPS seconds
        :param stop: stop time in GPS seconds
        """
        if self.epoch_start == start and self.epoch_stop == stop:
            return
        self.__send_cmd("set-epoch {0}-{1}".format(start, stop))
        self.epoch_start = start
        self.epoch_stop = stop

    def connect(self):
        """
        Connect to the channel info server if not connected
        """
        if self.s is None:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.host, self.port))
            self.s = sock_io.BufferedSocket(s)
            self.s.write(b'authorize\n')
            if self.s.read(4) != daqd.DAQD_OK:
                raise Exception("Unable to connect to information server")

    def close(self):
        """
        Close the connection to the channel info server
        """
        if self.s is not None:
            self.s.close()
            self.s = None

    def channel_avail_info(self, channels, gps, epoch_start, epoch_stop):
        """
        Return channel availability info of the form
        {"chan_name1": [
                {"frame_type": "string",
                 "start": int,
                 "stop": int
                }
            ]
        }
        :param channels: A list of channel info dictionaries (as returned
        by channel_list)
        :param gps: A gps time to check for existance
        :param epoch_start: A epoch start time (inclusive)
        :param epoch_stop: A epoch stop time (exclusive)
        """
        self.logger.debug("Calling update epoch")
        self.__update_epoch(epoch_start, epoch_stop)
        self.logger.debug("Requesting source data list")
        channel_types = {
            "m-trend": [],
            "s-trend": [],
            "other": [],
        }
        for chan in channels:
            if chan["channel_type"] == "m-trend":
                channel_types["m-trend"].append(chan)
            elif chan["channel_type"] == "s-trend":
                channel_types["s-trend"].append(chan)
            else:
                channel_types["other"].append(chan)
        result = {}
        for chan_type in ("m-trend", "s-trend", "other"):
            if len(channel_types[chan_type]) == 0:
                continue
            channel_names = [x["full_name"] for x in channel_types[chan_type]]
            cmd = "get-source-data {0} {{{1}}}".format(gps,
                                                       " ".join(channel_names))
            self.__send_cmd(cmd)
            size = self.s.read_int32()
            data = self.s.read(size).decode()
            self.logger.debug("Received {0} bytes '{1}'".format(size, data))
            done = False
            while not done:
                start = data.find("{")
                stop = data.find("}")
                if start < 0 or stop < 0 or start > stop:
                    break
                ch = data[:start].strip()
                ch_name = [x["full_name"] for x in channel_types[
                    chan_type] if x["channel_name"] == ch][0]
                ranges = data[start + 1:stop]
                data = data[stop + 1:]
                segments = []
                for entry in ranges.split(' '):
                    parts = entry.split(":", 1)
                    if len(parts) != 2:
                        continue
                    frame_type = parts[0]
                    times = parts[1].split("-", 1)
                    segments.append({"frame_type": frame_type,
                                     "start": int(times[0]),
                                     "stop": int(times[1])})
                result[ch_name] = segments
        return result

    def _do_chan_info_passthrough(self, command,
                                  channels, gps,
                                  epoch_start, epoch_stop,
                                  dest):
        """
        Run the channel source/availability pass through commands
        :param command: The nds2 command to issue
        :param channels: list of channels
        :param gps:
        :param epoch_start: Inclusive start time
        :param epoch_stop: Exclusive start time
        :param dest: Object to write results to
        :return:
        """
        self.logger.debug("Calling update epoch")
        self.__update_epoch(epoch_start, epoch_stop)
        self.logger.debug("Requesting source data list")
        cmd = "{0} {1} {{{2}}}".format(command,
                                       gps,
                                       " ".join(channels))
        self.__send_cmd(cmd)
        size = self.s.read_int32()
        dest.write(b'0000')
        dest.write_int32(size)
        while size > 0:
            chunk = 4096
            if size < 4096:
                chunk = size
            data = self.s.read(chunk)
            dest.write(data)
            size -= len(data)

    def channel_avail_info_passthrough(self, channels, gps,
                                       epoch_start, epoch_stop, dest):
        """
        Returns channel availability information writting
        it out to dest in NDS2 format
        :param channels: List of channels
        :param gps:
        :param epoch_start: Inclusive start time
        :param epoch_stop: Exclusive stop time
        :param dest: Object to write results to
        :return:
        """
        self._do_chan_info_passthrough("get-source-data",
                                       channels, gps,
                                       epoch_start,
                                       epoch_stop,
                                       dest)

    def channel_source_info_passthrough(self, channels, gps,
                                        epoch_start, epoch_stop, dest):
        """
        Returns channel source information writting
        it out to dest in NDS2 format
        :param channels: List of channels
        :param gps:
        :param epoch_start: Inclusive start time
        :param epoch_stop: Exclusive stop time
        :param dest: Object to write results to
        :return:
        """
        self._do_chan_info_passthrough("get-source-list",
                                       channels, gps,
                                       epoch_start,
                                       epoch_stop,
                                       dest)

    def channel_crc(self):
        """Return the CRC value of the channel list
        """
        cmd = "get-channel-crc"
        self.__send_cmd(cmd)
        crc = self.s.read(4)
        if len(crc) != 4:
            raise Exception("Invalid CRC length returned")
        return crc

    def channel_list(self, channels, epoch_start, epoch_stop,
                     channel_type="unknown"):
        """
        Return channel information in a structured manner
        :param channels: A list of channel names to match,
        the channel names should be the base name
        An empty list is a special case for all channesl
        :param epoch_start: global time window start
        :param epoch_stop: global time window stop
        :param channel_type: Type of the channel
        :return: A list of structures
        [
            {
                "channel_name": string,
                "channel_type": string,
                "data_type": string,
                "full_name": string,
                "rate": float,
            }
        ]
        """
        if self.__last_chan_list is not None:
            if self.__last_chan_list['epoch_start'] == epoch_start and self.__last_chan_list[
                'epoch_stop'] == epoch_stop and self.__last_chan_list['channel_type'] == channel_type and \
                            self.__last_chan_list['channels'] == channels:
                return self.__last_chan_list['results']
        self.__update_epoch(epoch_start, epoch_stop)
        if channel_type is None:
            channel_type = "unknown"
        if len(channels) == 0:
            pattern = ""
        elif len(channels) == 1:
            pattern = ' {' + channels[0] + '}'
        else:
            # thank you quora for pointing out this one
            # https://www.quora.com/What-is-the-easiest-way-to-find-the-longest-common-prefix-or-suffix-of-two
            # -sequences-in-Python
            root = os.path.commonprefix(channels)
            if len(channels) > 1:
                shortest_name = min(map(lambda name: len(name), channels))
                if shortest_name == len(root) and shortest_name > 0:
                    root = root[:shortest_name-1]
            if root == "":
                pattern = " {{" + ','.join(channels) + '}}'
            else:
                pattern = ' {' + root + '{'
                sep = ''
                rootlen = len(root)
                for chname in channels:
                    pattern += sep + chname[rootlen:]
                    sep = ","
                pattern += "}}"

        cmd = "get-channels 0 {0}{1}".format(channel_type, pattern)
        self.__send_cmd(cmd)
        count = self.s.read_int32()
        results = []
        for i in range(count):
            entry_size = self.s.read_int32()
            line = self.s.read(entry_size).decode()
            parts = line.split()
            # chan_name m-trend 0.0166667 real_8
            data_type = parts[3].strip('\0')
            data = {
                "channel_name": parts[0],
                "channel_type": parts[1],
                "data_type": data_type,
                "full_name": "{0},{1},{2}".format(parts[0],
                                                  parts[1],
                                                  parts[2]),
                "rate": float(parts[2]),
            }
            results.append(data)
        self.__last_chan_list = {
            'epoch_start': epoch_start,
            'epoch_stop': epoch_stop,
            'channel_type': channel_type,
            'channels': channels,
            'results': results,
        }
        return results

    def channel_list_passthrough(self, gps, channel_type, pattern,
                                 epoch_start, epoch_stop, dest):
        """
        Return a channel list matching the given filter parameters.
        Writes output to dest in NDS2 format
        :param gps: Time value
        :param channel_type: The type of the channels
        :param pattern: Glob pattern to match names against
        :param epoch_start: global time window start
        :param epoch_stop: global time window stop
        :param dest: Output channel list here
        """
        self.__update_epoch(epoch_start, epoch_stop)
        cmd = "get-channels {0} {1} {{{2}}}".format(gps, channel_type, pattern)
        self.__send_cmd(cmd)
        dest.write(daqd.DAQD_OK)
        channels = self.s.read_int32()
        buf = struct.pack("!I", channels)
        dest.write(buf)
        buf = b""
        for i in range(channels):
            proxy_buf = self.s.read_int32()
            info = self.s.read(proxy_buf)
            buf += struct.pack("!I", proxy_buf)
            buf += info
            if len(buf) > 4096:
                dest.write(buf)
                buf = b""
        if len(buf) > 0:
            dest.write(buf)

    def channel_count_passthrough(self, gps, channel_type, pattern,
                                  epoch_start, epoch_stop, dest):
        """
        Return a count of channels matching the given filter parameters.
        Writes output to dest in NDS2 format
        :param gps: Time value
        :param channel_type: The type of the channels
        :param pattern: Glob pattern to match names against
        :param epoch_start: global time window start
        :param epoch_stop: global time window stop
        :param dest: Output channel count here
        """
        self.__update_epoch(epoch_start, epoch_stop)
        cmd = "count-channels {0} {1} {{{2}}}".format(
            gps, channel_type, pattern)
        self.__send_cmd(cmd)
        dest.write(daqd.DAQD_OK)
        channels = self.s.read_int32()
        buf = struct.pack("!I", channels)
        dest.write(buf)


_aux_chan_info = {}


def register_chan_info(key, chan_info):
    global _aux_chan_info
    _aux_chan_info[key] = chan_info


def chan_info_factory(uri):
    if uri.startswith("nds2://"):
        name = uri.split("://")[1]
        port = 31200
        if name.find(":") >= 0:
            name, port = name.split(":", 1)
            port = int(port)
        return NDS2ChanInfo(name, port)
    if uri in _aux_chan_info:
        return _aux_chan_info[uri]
    return NDS2ChanInfo('localhost', 31201)

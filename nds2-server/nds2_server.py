# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import socketserver
import binascii
import datetime
import json
import logging
import logging.handlers
import struct

from . import daqd
from . import signal_units
from .chan_info import chan_info_factory
from .data_source import DataSource
from .data_buffer import DataBuffer
from .disk_cache import frame_lookup_factory
from .errors import DAQDException
from .errors import DAQDFrameIOError
from .errors import DAQDFrameErrorChannelNotFound
from .errors import DAQDGenericError
from .errors import DAQDSyntaxError
from .helpers import calc_cache_max_block_size
from .helpers import dtype_to_bytes
from .helpers import get_frame_intersection_info
from .helpers import seconds_to_samples
from .helpers import samples_to_seconds
from .helpers import parse_get_data_line
from .helpers import parse_get_frame_data_line
from .helpers import parse_get_online_data_line
from .memory_cache import memory_cache_factory
from .online_interface import online_factory
from .online_interface import IndexErrorWithName
from .remote_proxy import remote_proxy_factory
from .stats import stats_factory

# stackoverflow reminds that this must be set at the class level...
# https://stackoverflow.com/questions/16433522/socketserver-getting-rid-of-errno-98-address-already-in-use
socketserver.TCPServer.allow_reuse_address = True

class ForkingServer(socketserver.ForkingMixIn, socketserver.TCPServer):
#class ForkingServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    chan_info_uri = "nds2://localhost:31201"
    frame_lookup_uri = "dcache://localhost:20222"
    memory_cache_uri = "memcached://127.0.0.1"
    stats_server_uri = ""
    online_uri = ""
    reader_socket = ""
    concurrent = None

    request_queue_size = 1024


class NDS2ServerLogic:
    """
    The main NDS2 server logic.
    Split out from the handler to allow better testing.
    """

    INFINITE = 1999999999

    def __init__(self, request, frame_lookup_obj,
                 chan_info_obj, mem_cache_obj,
                 stats_obj, online_obj=None, reader_socket="",
                 remote_proxy=None,
                 concurrent=None,
                 epochs={},
                 high_latency_path=""):
        """
        Create a NDS2 server object
        :param request: The socket like object to send/receive data with
        :param frame_lookup_obj: The frame lookup object to use
        :param chan_info_obj: The channel information service object to use
        :param mem_cache_obj: The memory cache object to use
        :param stats_obj: A statistics object to report usage with
        an object in the cache
        """
        self.__buffer = b""
        self.epoch_start = 0
        self.epoch_stop = self.INFINITE
        self.writer_id = 1
        self.epochs = epochs
        self.source = DataSource(chan_info_obj, frame_lookup_obj, reader_socket,
                                 remote_proxy=remote_proxy, high_latency_path=high_latency_path, concurrent=concurrent)
        self.mem_cache = mem_cache_obj
        self.online = online_obj
        self.stats = stats_obj
        self.request = request
        self.__io_logger = logging.getLogger(__name__ + '.clientio')
        self.bytes_written = 0
        self.__error_msg = ""
        self.__channel_cap = 500
        self.__data_plan_cache = None
        self.__get_data_has_units = False

    def __read(self):
        """
        Read from the connection into the internal buffer.
        """
        buf = self.request.recv(1024)
        if len(buf) == 0:
            raise Exception()
        self.__io_logger.debug("read '{0}'".format(buf))
        self.__buffer += buf
        self.__io_logger.debug("buffer = '{0}'".format(self.__buffer))

    def read_line(self):
        """Return the next text line (delimited by \n) from the buffer.
        Reads from the connection if more data is needed."""
        idx = self.__buffer.find(b'\n')
        while idx < 0:
            self.__read()
            idx = self.__buffer.find(b'\n')
        line = self.__buffer[:idx]
        self.__buffer = self.__buffer[idx + 1:]
        return line

    def write(self, data):
        """
        Write the given data to the connection
        :param data: Data to write
        """
        self.request.sendall(data)
        self.bytes_written += len(data)

    def SYNTAX_ERROR(self):
        """Raise a syntax error exception"""
        raise DAQDSyntaxError()

    def write_int32(self, value):
        """
        Write a 32bit integer to the connection in network byte order
        :param value: Integer value to write
        :return:
        """
        data = struct.pack("!I", value)
        self.write(data)

    def handle_authorize(self, line):
        """Handle the authorize command
        :param line: the remainder of the command line, should be empty"""
        self.write(daqd.DAQD_OK)

    def handle_proto_ver(self, line):
        """
        Handle the protocol version command
        :param line:
        :return:
        """
        self.write(daqd.DAQD_OK)
        self.write_int32(1)

    def handle_proto_rev_default(self, line):
        """
        Handle the protocol revision command
        :param line:
        :return:
        """
        self.write(daqd.DAQD_OK)
        self.write_int32(5)

    def handle_proto_rev_specific_version_requested(self, line):
        """
        Handle the protocol revision command where a specific version
        has been requested.
        :param line:
        :return:
        """
        if line == "6;":
            self.write(daqd.DAQD_OK)
            self.write_int32(6)
            self.__get_data_has_units = True
            try:
                self.online.set_protocol_revision(6)
            except:
                pass
        else:
            self.SYNTAX_ERROR()

    def _parse_channel_filter(self, line):
        """
        Parse a nds2 channel filter
        :param line: a nds2 channel filter
        :return: a dictionary {
            "gps": gps time int value
            "channel_type": channel_type (raw|unknown|m-trend|s-trend)
            "pattern":  a pattern to match
        }
        """
        if line[-1] != ';':
            self.SYNTAX_ERROR()
        line = line[:-1]
        match = "*"
        idx = line.find('{')
        if idx > 0:
            if line[-1] != '}':
                self.SYNTAX_ERROR()
            match = line[idx + 1:-1]
            line = line[:idx]

        parts = line.split()
        if len(parts) not in [1,2]:
            self.SYNTAX_ERROR()

        gps = int(parts[0])
        chantype = 'unknown'
        if len(parts) > 1:
            chantype = parts[1]

        if chantype not in ['raw', 'reduced', 'unknown', 'm-trend', 's-trend', 'online', 'test-pt', 'static']:
            self.SYNTAX_ERROR()

        params = {
            "epoch": [self.epoch_start, self.epoch_stop],
            "gps": gps,
            "channel_type": chantype,
            "pattern": match,
        }
        return params

    def handle_count_channels(self, line):
        """
        Handle the count channels command
        :param line:
        :return:
        """
        payload = self._parse_channel_filter(line)
        self.source.channel_count_passthrough(
            payload['gps'],
            payload['channel_type'],
            payload['pattern'],
            self.epoch_start,
            self.epoch_stop,
            self,
        )

    def handle_get_channel_crc(self, line):
        """
        Handle the get-channel-crc command
        :param line:
        :return:
        """
        try:
            chan_crc = self.source.channel_crc().encode()
        except:
            raise DAQDGenericError("Invalid CRC")
        self.write(daqd.DAQD_OK)
        self.write(chan_crc)

    def handle_get_channels(self, line):
        """
        Handle the get channels command
        :param line:
        :return:
        """
        payload = self._parse_channel_filter(line)
        self.source.channel_list_passthrough(
            payload['gps'],
            payload['channel_type'],
            payload['pattern'],
            self.epoch_start,
            self.epoch_stop,
            self,
        )

    def handle_get_source_data(self, line):
        """
        Handle the get source data command
        :param line:
        :return:
        """
        idx = line.find('{')
        if idx < 0 or line[-1] != ';' or line[-2] != '}':
            self.SYNTAX_ERROR()
        parts = line[:idx - 1].strip().split()
        channels = line[idx + 1:-2].strip().split()

        self.stats.add_channel_list(channels)

        self.source.channel_avail_info_passthrough(
            channels,
            int(parts[0]),
            self.epoch_start,
            self.epoch_stop,
            self,
        )

    def handle_get_source_list(self, line):
        """
        Handle the get source data command
        :param line:
        :return:
        """
        idx = line.find('{')
        if idx < 0 or line[-1] != ';' or line[-2] != '}':
            self.SYNTAX_ERROR()
        parts = line[:idx - 1].strip().split()
        channels = line[idx + 1:-2].strip().split()
        self.stats.add_channel_list(channels)
        self.source.channel_source_info_passthrough(
            channels,
            int(parts[0]),
            self.epoch_start,
            self.epoch_stop,
            self,
        )

    def write_channel_info_block(self, channel_info, stride):
        dtype_to_val = {
            'int_2': 1,
            'int_4': 2,
            'int_8': 3,
            'real_4': 4,
            'real_8': 5,
            'uint_4': 7,
        }
        ctype_to_val = {
            'unknown': 0,
            'online': 1,
            'raw': 2,
            'reduced': 3,
            's-trend': 4,
            'm-trend': 5,
            'test-pt': 6,
            'static': 7,
        }
        # reconfigure block
        #  has dummy values for gps, gps nano, and seq
        cur_offset = 0
        out = struct.pack("!LIII", 0xffffffff, 1, 2, 3)
        for chinfo in channel_info:
            ch_span_size = int(seconds_to_samples(chinfo, stride) *
                               dtype_to_bytes(chinfo))
            out += struct.pack("!IIHHfff",
                               ch_span_size,
                               cur_offset,
                               ctype_to_val[chinfo["channel_type"]],
                               dtype_to_val[chinfo["data_type"]],
                               chinfo["rate"],
                               chinfo['units'].offset,
                               chinfo['units'].slope)
            if self.__get_data_has_units:
                unit_str = chinfo['units'].units
                out += struct.pack("!I", len(unit_str)) + unit_str.encode()
            cur_offset += ch_span_size
        out = struct.pack("!I", len(out)) + out
        self.write(out)

    def write_channel_span(self, chan, fetch_plan,
                           start, stride, mem_cache,
                           max_block_size, out_buf):
        """
        Write out a raw data from the given channel over the gps
        timeframe [start, start+stride).
        :param chan: Channel info object to write out
        :param fetch_plan: Data fetch plan for this segment
        :param start: Start gps time
        :param stride: Number of seconds to write out
        :param mem_cache: Cache object
        :param max_block_size: Max cache block size to use (in seconds)
        :param out_buf: File like object to write data to
        :return: a signal_units.signal_units structure with the channel unit meta-data
        """

        cur = start
        stop = cur + stride

        # sample_size = dtype_to_bytes(chan)
        units = None

        while cur < stop:
            data, samples_to_write, cur_seg_units = self.source.fetch_data(
                chan, fetch_plan, cur, stop, mem_cache, max_block_size)

            if units is None:
                units = cur_seg_units

            out_buf.write(data)

            cur += samples_to_seconds(chan, samples_to_write)

        if units is None:
            units = signal_units.default()
        return units

    def write_channel_data_block(self, chan_info, fetch_plan, start, stride, prev_stride, seq_num, cache):
        size = 0
        for chan in chan_info:
            size += int(seconds_to_samples(chan, stride) *
                        dtype_to_bytes(chan))

        # write the block header
        # the 4 byte length at the start is the data size + 16 bytes of block
        # header
        header = struct.pack("!IIIII", size + 16, stride, start, 0, seq_num)

        try:
            with DataBuffer(size) as seg_buf:
                for i in range(len(chan_info)):
                    chan = chan_info[i]
                    chan_info[i]['units'] = self.write_channel_span(chan,
                                            fetch_plan,
                                            start,
                                            stride,
                                            cache,
                                            calc_cache_max_block_size(chan, cache),
                                            seg_buf)
                if prev_stride != stride:
                    self.write_channel_info_block(chan_info, stride)
                self.write(header)
                seg_buf.flush_to(self)
        except DAQDFrameIOError:
            # should this be a general catch everything???
            header = struct.pack("!IIIII", 16, stride, start, 0, seq_num)
            self.write(header)
            raise

    def handle_get_data(self, line):
        """
        :param line: start stop [stride] {chan list}
        :return:
        """

        start, stop, stride, channels = parse_get_data_line(line)
        if len(channels) > self.__channel_cap:
            raise DAQDSyntaxError(
                "Please limit requests to {0} or less channels at a time.  This limit is to keep timeout errors down, "
                "we are working on raising it.".format(
                    self.__channel_cap))

        if start == 0:
            self.handle_get_online_data("{0} {1} {{ {2} }};".format(stop, stride, " ".join(channels)))
            return

        cache = self.mem_cache

        self.stats.add_channel_list(channels)
        chan_info, fetch_plan = self.source.create_data_plan(
            start, stop, stride, channels)

        if stride == 0 or stride > (stop - start):
            stride = stop - start

        self.write(daqd.DAQD_OK)

        self.write(binascii.hexlify(struct.pack("!L", self.writer_id)))
        self.write(struct.pack("!L", 1))

        self.writer_id += 1

        cur = start
        seq = 0

        prev_stride = 0

        while cur < stop:
            if cur + stride > stop:
                stride = stop - cur

            self.write_channel_data_block(chan_info, fetch_plan.sub_plan(
                cur, cur + stride), cur, stride, prev_stride, seq, cache)

            self.source.clear_cache()
            prev_stride = stride
            seq += 1
            cur += stride

    def handle_get_frame_data(self, line):
        """
        :param line: frame_type start stop {chan list}
        :return:
        """
        frame_type, start, stop, channels = parse_get_frame_data_line(line)
        self.write(daqd.DAQD_OK)
        frames = self.source.get_frame_list(frame_type, start, stop)

        for frame in frames:
            in_error = False

            info = get_frame_intersection_info(frame, start, stop)
            header = { "start": info['gps_start'], "stop": info['gps_stop'], "channels": len(channels) }
            header_data = json.dumps(header).encode('utf8')
            header_data = struct.pack(">i", len(header_data)) + header_data
            self.write(header_data)

            index = 0
            for chan in channels:
                data = b""
                units = ""
                status = 1
                try:
                    if not in_error:
                        print("Requesting data")
                        data, samples_to_write, units = self.source.fetch_frame_data(
                            chan=chan, fname=frame, start=info['gps_start'], stop=info['gps_stop'], mem_cache=self.mem_cache, max_block_size=calc_cache_max_block_size(chan, self.mem_cache))
                        status = 0
                except DAQDFrameErrorChannelNotFound:
                    print("Channel not found")
                    status = 2
                except DAQDFrameIOError:
                    print("Frame IO error")
                    status = 1
                    in_error = True
                header = {"i": index, "s": status, "d": len(data)}
                header_data = json.dumps(header).encode('utf8')
                header_data = struct.pack(">i", len(header_data)) + header_data
                self.write(header_data)
                self.write(data)
                index += 1

        header = {"start": 0, "stop": 0, "channels": 0}
        header_data = json.dumps(header).encode('utf8')
        header_data = struct.pack(">i", len(header_data)) + header_data
        self.write(header_data)


    def handle_get_online_data(self, line):
        """
        :param line: [stop] stride {chan list}
        :return:
        """
        if self.online is None:
            self.write(daqd.DAQD_NOT_FOUND)
            return

        cache = self.mem_cache

        start, stop, stride, channels = parse_get_online_data_line(line)
        self.stats.add_channel_list(channels)
        duration = stop

        try:
            online_plan, stride = self.online.get_plan(channels, stride)
        except IndexErrorWithName as err:
            raise DAQDException(daqd.DAQD_INVALID_CHANNEL_NAME,
                                "This channel was not found on the server {0}".format(", ".join(err.channels)))
        except IndexError:
            raise DAQDException(daqd.DAQD_INVALID_CHANNEL_NAME)

        self.write(daqd.DAQD_OK)

        self.write(binascii.hexlify(struct.pack("!L", self.writer_id)))
        self.write(struct.pack("!L", 0))

        self.writer_id += 1

        cur = start
        seq = 0

        prev_stride = 0

        while (stop != 0 and duration > 0) or (stop == 0):
            if stop > 0 and stride > duration:
                stride = duration

            cur = self.online.write_channel_data_block(online_plan, cur, stride, prev_stride, seq, cache, self)

            prev_stride = stride

            seq += 1

            if stop > 0:
                duration -= stride

    def handle_check_data(self, line):
        """
        Handle the check-data command
        :param line: start stop {channels}
        :return:
        """
        start, stop, stride, channels = parse_get_data_line(line)
        if len(channels) > self.__channel_cap:
            raise DAQDSyntaxError(
                "Please limit requests to {0} or less channels at a time.  This limit is to keep timeout errors down, "
                "we are working on raising it.".format(
                    self.__channel_cap))

        if start == 0:
            self.write(daqd.DAQD_OK)
            return
        self.stats.add_channel_list(channels)
        chan_info, fetch_plan = self.source.create_data_plan(
            start, stop, 0, channels)

        self.source.check_data(fetch_plan)

        self.write(daqd.DAQD_OK)

    def handle_list_epochs(self, line):
        if line != "":
            raise DAQDSyntaxError()
        epochs = []
        epoch_keys = sorted(self.epochs.keys())
        for epoch in epoch_keys:
            epochs.append("{0}={1}-{2}".format(epoch,
                                               self.epochs[epoch][0],
                                               self.epochs[epoch][1]
                                               ))
        out = " ".join(epochs).encode()
        self.write(daqd.DAQD_OK)
        self.write_int32(len(out))
        self.write(out)

    def handle_set_epoch(self, line):
        """

        :param line: start-stop; or name;
        """
        line = line.rstrip('; \t\n')
        line = line.strip()

        start = self.epoch_start
        stop = self.epoch_stop
        try:
            if line in self.epochs:
                start = self.epochs[line][0]
                stop = self.epochs[line][1]
            else:
                parts = line.split('-', 1)
                if len(parts) == 2:
                    start = int(parts[0])
                    stop = int(parts[1])
                else:
                    parts = line.split(":")
                    if len(parts) == 2:
                        start = int(parts[0])
                        stop = start + int(parts[1])
                    else:
                        raise Exception()
            if stop < start or start < 0:
                raise Exception()
        except:
            raise DAQDSyntaxError("Invalid epoch.")
        self.epoch_start = start
        self.epoch_stop = stop

        self.write(daqd.DAQD_OK)

    def handle_get_last_message(self, line):
        msg = self.__error_msg.encode()
        self.clear_error_message()
        self.write(daqd.DAQD_OK)
        self.write_int32(len(msg))
        self.write(msg)

    def clear_error_message(self):
        self.__error_msg = ""

    def set_error_message(self, msg):
        if msg is None or msg == "":
            return
        self.__error_msg += ":" + msg

    def parse_and_handle_commands(self):
        """
        The main command loop.
        :return:  Nothing
        """
        class QuitLoopException(Exception):
            pass

        def do_quit(line):
            raise QuitLoopException()

        cmd_log = logging.getLogger(__name__ + ".commands")

        cmd_map = {
            'authorize': self.handle_authorize,
            'server-protocol-version;': self.handle_proto_ver,
            'server-protocol-revision;': self.handle_proto_rev_default,
            'server-protocol-revision': self.handle_proto_rev_specific_version_requested,
            "count-channels": self.handle_count_channels,
            "get-channel-crc;": self.handle_get_channel_crc,
            "get-channels": self.handle_get_channels,
            "get-last-message;": self.handle_get_last_message,
            "get-source-data": self.handle_get_source_data,
            "get-source-list": self.handle_get_source_list,
            "get-data": self.handle_get_data,
            "__get-frame-data": self.handle_get_frame_data,
            "get-online-data": self.handle_get_online_data,
            "check-data": self.handle_check_data,
            "set-epoch": self.handle_set_epoch,
            "list-epochs;": self.handle_list_epochs,
            "quit;": do_quit,
        }
        try:
            while True:
                line = self.read_line().decode().strip()
                parts = line.split(None, 1)
                cmd = parts[0]
                if len(parts) == 1:
                    parts.append('')
                try:
                    if cmd != "get-last-message;":
                        self.clear_error_message()
                    if cmd in cmd_map:
                        self.stats.start_command(cmd.strip(';'))
                        self.bytes_written = 0
                        cmd_log.info("command: {0} {1}".format(cmd, parts[1]))
                        start_time = datetime.datetime.now()
                        cmd_map[cmd](parts[1])
                        end_time = datetime.datetime.now()
                        delta = end_time - start_time
                        cmd_log.info(
                            "completed: {0}: completed in {1}".
                                format(cmd, delta))
                        self.stats.add_bytes_sent(self.bytes_written)
                        self.stats.finish_command()
                    else:
                        cmd_log.info("Invalid command received")
                        raise DAQDException(
                            daqd.DAQD_COMMAND_SYNTAX, "Invalid command")
                except DAQDException as err:
                    cmd_log.info("completed: {0}: error {1}:{2}".format(
                        cmd, err.code, err.msg
                    ))
                    self.set_error_message(err.msg)
                    self.write(err.code)
                    self.stats.finish_command()
        except QuitLoopException:
            self.stats.finish_command()
            self.stats.close()


class NDS2Handler(socketserver.BaseRequestHandler):
    def setup(self):
        logging.info("Connection from {0}".format(self.client_address))
        self.nds2 = NDS2ServerLogic(self.request,
                                    frame_lookup_factory(
                                        self.server.frame_lookup_uri),
                                    chan_info_factory(
                                        self.server.chan_info_uri),
                                    memory_cache_factory(
                                        self.server.memory_cache_uri),
                                    stats_factory(
                                        self.server.stats_server_uri),
                                    online_factory(
                                        self.server.online_uri),
                                    reader_socket=self.server.reader_socket,
                                    concurrent=self.server.concurrent,
                                    remote_proxy=remote_proxy_factory(
                                        self.server.remote_proxy_uri),
                                    epochs=self.server.epochs,
                                    high_latency_path=self.server.high_latency_path
                                    )

    def handle(self):
        self.nds2.parse_and_handle_commands()

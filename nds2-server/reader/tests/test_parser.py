# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase


class TestParser(TestCase):
    def test_parser_good(self):
        from ..parser import Parser
        from ..parser import ReadArguments

        tests = (
            (
                ['read', 'frame-1000000000-128.gwf', '0', '1', 'X1:PROC-TEST_1%real_4%reduced%16'],
                'read',
                ReadArguments('frame-1000000000-128.gwf', 0, [
                    {'channel_type': 'reduced', 'channel_name': 'X1:PROC-TEST_1', 'rate': 16.0,
                     'full_name': 'X1:PROC-TEST_1,reduced,16.0', 'data_type': 'real_4'}]),
            ),
            (
                ['read', 'frames-1900000000-64.gwf', '6', '1', 'X1:PROC-TEST_2%real_4%reduced%16'],
                'read',
                ReadArguments('frames-1900000000-64.gwf', 6, [
                    {'channel_type': 'reduced', 'channel_name': 'X1:PROC-TEST_2', 'rate': 16.0,
                     'full_name': 'X1:PROC-TEST_2,reduced,16.0', 'data_type': 'real_4'}]),
            ),
            (
                ['quit', ],
                'quit',
                (),
            ),
        )
        parser = Parser()
        for test in tests:
            text = "\t".join(test[0])
            cmd, args = parser.parse(text)
            self.assertEqual(cmd, test[1])
            self.assertEqual(args, test[2])

    def test_parser_failure(self):
        from ..parser import Parser

        tests = (
            (
                'read', 'frame-1000000000-128.gwf', '0', '1000000006', '1',
            ),
            (
                'read', 'frames-1900000000-64.gwf', '0', '1900000006', '2', 'X1:PROC-TEST_2', 'X1:PROC-TEST_1',
            ),
            (
                'unknown', 'command',
            ),
            (
                'unknown_command',
            ),
        )
        parser = Parser()
        for test in tests:
            text = "\t".join(test)
            self.assertRaises(Exception, parser.parse, text)

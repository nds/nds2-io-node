# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import memcache


class NDS2MemCacheClient(memcache.Client):
    def __init__(self, servers, server_max_value_length=1024 * 1024):
        super(NDS2MemCacheClient, self).__init__(servers, server_max_value_length=server_max_value_length)
        self.__max_block_size = server_max_value_length

    def max_block_size(self):
        return self.__max_block_size

    def get(self, key):
        try:
            return super(NDS2MemCacheClient, self).get(key)
        except:
            return None

_aux_memory_cache_lookup = {}


def register_memory_cache_lookup(key, memory_cache):
    """
    Register a memory cache object on a specific uri.  Mostly for debugging.
    :param key: Key value
    :param memory_cache: Memory cache compatible object
    """
    global _aux_memory_cache_lookup
    _aux_memory_cache_lookup[key] = memory_cache


def memory_cache_factory(uri):
    """
    Given a uri return a memory cache object
    :param uri: The uri
    :return: A memory cache object
    """
    global _aux_memory_cache_lookup

    if uri.startswith("memcached://"):
        name = uri.split("://")[1]
        servers = name.split(",")
        return NDS2MemCacheClient(servers=servers)
    if uri in _aux_memory_cache_lookup:
        return _aux_memory_cache_lookup[uri]
    return NDS2MemCacheClient(servers=['127.0.0.1'])

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE


#   Channel list generated from: /online/LHO_Data
# H1:GDS-CALIB_F_CC online 0 real_4 16 LHO_Data
# H1:GDS-CALIB_F_CC_NOGATE online 0 real_4 16 LHO_Data
# H1:GDS-CALIB_KAPPA_C online 0 real_4 16 LHO_Data

#
# We have:
#  Name
#  channel type
#  gps start?
#  data type
#  sample rate
#  frame type

#  this can be broken into frame specific interfaces
#    frame type
#    parent directory
#
# and channel specific portions
#   Name
#   channel type
#   data type
#   sample rate
#   frame type
#
# We need a mapping so we know what is available and where
# it is available


# The interface that is needed for an online object is:

# * set_protocol_revision
# * get_plan(channels, stride) -> plan
# * write_channel_data_block(plan, cur, stride, prev_stride, seq, cache, writer)

import glob
import json
import logging
import os.path
import re
import struct
import time

try:
    import frameCPP
except ImportError:
    from LDAStools import frameCPP

import numpy as np
try:
    import daqd_stream
    has_daqd_stream = True
except ImportError:
    has_daqd_stream = False

from . import signal_units
from .data_buffer import DataBuffer
from .data_buffer import Closer as DataBufferCloser
from .cache_adaptor import CacheAdaptor
from .errors import DAQDFrameIOError
from .errors import DAQDGenericError
from .errors import DAQDSyntaxError
from .helpers import bytes_to_samples
from .helpers import calc_cache_max_block_size
from .helpers import dtype_to_bytes
from .helpers import get_frame_intersection_info
from .helpers import get_frame_name_info
from .helpers import parse_channel_name
from .helpers import samples_to_bytes
from .helpers import samples_to_seconds
from .helpers import seconds_to_samples
from .frame_io import cache_key
from .frame_io import read_from_frame


class IndexErrorWithName(IndexError):
    def __init__(self, channels):
        self.channels = []

    def __str__(self):
        return "Invalid channels {0}".format(",".join(self.channels))


def verify_mapping(channel_map):
    """
    Verify that channel_map has a valid channel_map structure
    :param channel_map:
    :return:
    Raises an exception on error
    """
    prefix_re = re.compile(r'^[A-Za-z\-\_0-9]+$')
    channel_re = re.compile(r'^[A-Za-z:\-\_0-9]+$')

    required_sub_keys = ("location", "prefix", "channels")
    required_channel_keys = ("sample_rate", "data_type")
    for frame_type in channel_map.keys():
        cur_ft = channel_map[frame_type]
        ft_keys = list(cur_ft.keys())
        for req_key in required_sub_keys:
            if req_key not in ft_keys:
                raise Exception("frame type missing required key {0}".format(req_key))
            if not os.path.exists(cur_ft["location"]):
                raise Exception("online frame location is invalid {0}".format(cur_ft["location"]))
            if not prefix_re.match(cur_ft['prefix']):
                raise Exception("online frame prefix is invalid {0}".format(cur_ft["prefix"]))
            channel_keys = list(cur_ft["channels"].keys())
            for channel_name in channel_keys:
                if not channel_re.match(channel_name):
                    raise Exception("Invalid channel for online channel mapping {0}".format(channel_name))
                cur_channel = cur_ft["channels"][channel_name]
                channel_keys = list(cur_channel.keys())
                for req_chan_key in required_channel_keys:
                    if req_chan_key not in channel_keys:
                        raise Exception("Online channel definition missing required field {0}".format(req_chan_key))


def load_mapping_from_file(fname):
    with open(fname, "rt") as f:
        dat = json.load(f)
        verify_mapping(dat)
        return dat

def _generate_channel_info_block(channel_info, stride, has_units=True):
    dtype_to_val = {
        'int_2': 1,
        'int_4': 2,
        'int_8': 3,
        'real_4': 4,
        'real_8': 5,
        'uint_4': 7,
    }
    ctype_to_val = {
        'unknown': 0,
        'online': 1,
        'raw': 2,
        'reduced': 3,
        's-trend': 4,
        'm-trend': 5,
        'test-pt': 6,
        'static': 7,
    }
    # reconfigure block
    #  has dummy values for gps, gps nano, and seq
    cur_offset = 0
    out = struct.pack("!LIII", 0xffffffff, 1, 2, 3)

    # to handle stride < 0 (16Hz)
    calc_stride = stride
    size_divider = 1
    if stride < 0:
        # calculate at 1s stride and divide into 16Hz groups after
        calc_stride = 1
        size_divider = 16
    for chinfo in channel_info:
        ch_span_size = int(seconds_to_samples(chinfo, calc_stride) *
                           dtype_to_bytes(chinfo))//size_divider
        out += struct.pack("!IIHHfff",
                           ch_span_size,
                           cur_offset,
                           ctype_to_val[chinfo["channel_type"]],
                           dtype_to_val[chinfo["data_type"]],
                           chinfo["rate"],
                           chinfo['units'].offset,
                           chinfo['units'].slope)

        cur_offset += ch_span_size
        if has_units:
            unit_str = chinfo['units'].units
            out += struct.pack("!I", len(unit_str)) + unit_str.encode()
            print("reconfig block has units")
        else:
            print("reconfig block does NOT have units")
    return struct.pack("!I", len(out)) + out


class OnlineFiles:
    def __init__(self, channel_map, sleep=time.sleep):
        """
        Initialize the online object with a channel mapping
        :param channel_map: a map of channel information of the form
        {
            "frame type": {
                "location": "path to frame files",
                "prefix": "prefix to the frame file name, which for online may be different than the frame type",
                "channels": {
                    "chan_name": {
                        "data_type": "data_type",
                        "sample_rate": float
                    }
                }
            }
        }
        :param sleep: The sleep function to use, defaults to the system sleep
        """
        self.__channel_mapping = channel_map
        self.__channel_cache = {}
        self.__sleep = sleep
        self.__get_data_has_units = False

    def set_protocol_revision(self, value):
        if value == 6:
            self.__get_data_has_units = True

    def _resolve_channel(self, channel_name):
        """
        Given a channel name, attempt to map it to the online channels
        :param channel_name: A channel name
        :return: frame_type, basename, 'online', rate, data_type if a match is found
        :note: raises an exception (IndexError) if a match is not found
        """
        basename, chtype, rate = parse_channel_name(channel_name)
        for frame_type in self.__channel_mapping:
            channels = self.__channel_mapping[frame_type]["channels"]
            if basename in channels:
                if chtype not in (None, 'online'):
                    raise IndexErrorWithName([basename, ])
                chtype = 'online'
                if rate is not None:
                    if rate != channels[basename]['sample_rate']:
                        continue
                else:
                    rate = channels[basename]['sample_rate']
                data_type = channels[basename]['data_type']
                self.__channel_cache[channel_name] = (
                    frame_type, basename, chtype, rate, data_type)
                return frame_type, basename, chtype, rate, data_type
        raise IndexErrorWithName([channel_name, ])

    def get_plan(self, channel_list, stride):
        """
        Given a list of channels return a high level plan object
        :param channel_list:
        :return:
        """
        if stride < 1:
            raise DAQDSyntaxError("frame based online data is not available at less than 1hz stride")
        return self._resolve_channels(channel_list), stride

    def _resolve_channels(self, channel_list):
        """
        Given a channel list resolve it to online frame types and
        directories
        :param channel_list: A list of channel names
        :return: ({
            "online frame type": {
                "location": "path to frame files",
                "prefix": "the prefix/real frame type",
                "channels": [
                    ("channel name", request index),
                ]
            }
        }, (
        # for each channel, in the order of channel_list
            {
                "channel_name": ...
                "rate": ... rate
                "data_type": ... real_4/real_8/...
                "channel_type": ... online
            }
        ))
        """
        results = {}
        channels = []
        for i, name in enumerate(channel_list):
            if name not in self.__channel_cache:
                self._resolve_channel(name)
            frame_type, basename, chtype, rate, data_type = self.__channel_cache[name]
            if not frame_type in results:
                ref = self.__channel_mapping[frame_type]
                frame = {
                    "location": ref["location"],
                    "prefix": ref["prefix"],
                    "channels": [],
                }
            else:
                frame = results[frame_type]
            frame["channels"].append((basename, i))
            results[frame_type] = frame
            channels.append({
                'channel_name': basename,
                'rate': rate,
                'data_type': data_type,
                'channel_type': chtype,
            })
        return results, tuple(channels)

    def _find_last_available_frame(self, plan_entry):
        """
        :param plan_entry: A online plan entry
        :return: The last available frame for the entry
        """
        entries = glob.glob(os.path.join(plan_entry['location'], plan_entry['prefix'] + "*.gwf"))
        entries.sort(reverse=True)
        return entries[0]

    def _get_latest_gps_time(self, plan_entry, gps_start):
        """
        Given a plan entry and a gps start time, return the
        max of the latest gps time available for the plan or gps_start
        :param plan_entry: A gps planning entry
        :param gps_start: A gps start time
        :return: The max of gps_start, latest available plan time
        """
        try:
            latest_frame = self._find_last_available_frame(plan_entry)
        except IndexError:
            return gps_start
        frame_info = get_frame_name_info(latest_frame)
        if frame_info[1] is None:
            return gps_start
        return max(gps_start, frame_info[1])

    def verify_channels(self, channel_list):
        """
        Verify that the given channels are online channels
        :param channel_list: a list of channel names
        :return: nothing
        Throws an exception if a channel is not a valid online channel
        """
        for name in channel_list:
            if name in self.__channel_cache:
                continue
            self._resolve_channel(name)

    def _get_frame_containing_gps(self, plan_entry):
        entries = glob.glob(os.path.join(plan_entry['location'], plan_entry['prefix'] + "*.gwf"))
        for entry in entries:
            finfo = get_frame_name_info(entry)
            if finfo[1] is None:
                continue
            if finfo[1] <= plan_entry['cur_gps'] and finfo[2] > plan_entry['cur_gps']:
                return entry
        return None

    def get_gps_start_for_plan(self, plan):
        """
        Given a online fetch plan, return the gps start time to use
        :param plan: the gps fetch plan
        :return: gps start time of the latest frame available.  Raises exception
        if none is found.
        """
        gps_start = 0
        current_wait = 0
        wait_step = 1
        longest_wait = 60
        while gps_start == 0 and current_wait < longest_wait:
            for entry in plan:
                gps_start = self._get_latest_gps_time(plan[entry], gps_start)
            if gps_start == 0:
                current_wait += wait_step
                self.__sleep(wait_step)
        if gps_start == 0:
            raise Exception("Waiting too long")
        return gps_start

    def get_segment(self, plan, channel_info, gps_start, stride, mem_cache, out_bufs):
        """
        Return one segment of online data
        :param plan: The online fetch plan
        :param channel_info: List of channel info structures
        :param gps_start: Start time, 0 for latest available
        :param stride: Length of the output buffer in seconds
        :param mem_cache: cache object
        :param out_bufs: output buffers, one per channel
        :note: Raises exceptions on errors
        :note: Modifies channel_info, updating the unit information
        """

        logger = logging.getLogger(__name__ + 'onlineio')
        if stride < 0:
            raise Exception()

        wait_step = 1
        longest_wait = 60

        if gps_start == 0:
            gps_start = self.get_gps_start_for_plan(plan)

        for entry in plan:
            plan[entry]['cur_gps'] = gps_start
            plan[entry]['wait'] = 0
        end = gps_start + stride

        cache = CacheAdaptor(mem_cache)

        for i in range(len(channel_info)):
            channel_info[i]["units"] = None

        done = False
        while not done:
            done = True
            request_wait = False
            for entry in plan:
                if plan[entry]['cur_gps'] >= end:
                    continue
                done = False
                cur_frame = self._get_frame_containing_gps(plan[entry])
                if cur_frame is None:
                    plan[entry]['wait'] += wait_step
                    request_wait = True
                    if plan[entry]['wait'] > longest_wait:
                        raise Exception('Waiting too long')
                    continue
                plan[entry]['wait'] = 0
                # we are now ready to read from [plan[entry]['cur_gps'], end)
                cur_gps = plan[entry]['cur_gps']
                target_frame = get_frame_intersection_info(cur_frame, cur_gps, end)
                for cur_ch in plan[entry]['channels']:
                    cur_channel_index = cur_ch[1]
                    chan = channel_info[cur_channel_index]
                    out_buf = out_bufs[cur_channel_index]

                    cur = cur_gps

                    max_block_size = calc_cache_max_block_size(chan, cache)

                    max_requested_samples = seconds_to_samples(chan, end - cur)
                    gps_offset = cur - target_frame['frame_start']
                    # the cache_ values are relative to the start of the frame
                    if gps_offset % max_block_size == 0:
                        cache_start = gps_offset
                        cache_offset_samples = 0
                    else:
                        cache_start = int(gps_offset / max_block_size) * max_block_size
                        cache_offset_samples = seconds_to_samples(
                            chan, gps_offset % max_block_size)
                    cache_offset_bytes = samples_to_bytes(chan, cache_offset_samples)
                    cache_end = cache_start + max_block_size
                    target_delta = target_frame['gps_stop'] - target_frame['gps_start']
                    if cache_end > target_delta:
                        cache_end = target_frame[
                                        'gps_stop'] - target_frame['gps_start']

                    key = cache_key(target_frame['frame_type'], target_frame[
                        'frame_start'], chan, cache_start)

                    data = cache.get(key)
                    if data is None:
                        logger.debug("cache miss @ {0}".format(key))
                        # FIXME: make a generic get frame method, lambda or something
                        fs = frameCPP.IFrameFStream(str(target_frame['filename']))
                        toc = fs.GetTOC()

                        cache.keep_local(key)
                        read_from_frame(fs, target_frame['frame_type'], chan, target_frame['frame_start'],
                                        cache_start, max_block_size, cache, toc)
                        data = cache.get(key)
                        cache.keep_local(None)
                        if data is None:
                            raise Exception("Unable to read data")
                    else:
                        logger.debug("cache hit @ {0}".format(key))
                    cur_segment_units, data = signal_units.extract_signal_units(data)
                    if chan['units'] is None:
                        chan['units'] = cur_segment_units
                    total_cache_samples = bytes_to_samples(chan, len(data))
                    remaining_cache_samples = \
                        total_cache_samples - cache_offset_samples
                    samples_to_write = min(
                        remaining_cache_samples, max_requested_samples)

                    stop_byte = cache_offset_bytes + \
                                samples_to_bytes(chan, samples_to_write)
                    out_buf.write(data[cache_offset_bytes:stop_byte])

                    cur += samples_to_seconds(chan, samples_to_write)

                plan[entry]['cur_gps'] = get_frame_name_info(cur_frame)[2]

            if request_wait:
                self.__sleep(wait_step)

                #         if it is not available
                #             wait up to a timeout value
                #         if it is not available
                #             raise a daqd_not_found error
                #         open the frame
                #         for each channel in frame type
                #             read in the smaller of remaining or frame size
                # return buffer, end
        for i in range(len(channel_info)):
            if channel_info[i]["units"] is None:
                channel_info[i]["units"] = signal_units.default()

    def write_channel_info_block(self, channel_info, stride, writer):
        writer.write(_generate_channel_info_block(channel_info, stride, has_units=self.__get_data_has_units))

    def write_channel_data_block(self, online_plan, start, stride, prev_stride, seq_num, cache, writter):
        """

        :param self:
        :param start:
        :param stride:
        :param prev_stride:
        :param seq_num:
        :param cache:
        :param writter: The object to write data to
        :return:
        """
        plan = online_plan[0]
        chan_info = online_plan[1]

        if start == 0:
            start = self.get_gps_start_for_plan(plan)

        for entry in plan:
            plan[entry]['cur_gps'] = start
            plan[entry]['wait'] = 0
        end = start + stride

        size = 0
        sizes = []
        for chan in chan_info:
            cur_size = int(seconds_to_samples(chan, stride) *
                           dtype_to_bytes(chan))
            size += cur_size
            sizes.append(cur_size)

        # write the block header
        # the 4 byte length at the start is the data size + 16 bytes of block
        # header
        header = struct.pack("!IIIII", size + 16, stride, start, 0, seq_num)

        try:
            with DataBufferCloser() as out_bufs:
                for i in range(len(chan_info)):
                    out_bufs.add(DataBuffer(sizes[i], scale_tip_point=1.0 / len(chan_info)))

                self.get_segment(plan, chan_info, start, stride, cache, out_bufs)

                if prev_stride != stride:
                    self.write_channel_info_block(chan_info, stride, writter)
                writter.write(header)
                for i in range(len(chan_info)):
                    out_bufs[i].flush_to(writter)

        except DAQDFrameIOError:
            # should this be a general catch everything???
            header = struct.pack("!IIIII", 16, stride, start, 0, seq_num)
            writter.write(header)
            raise
        return end

    def get_uri(self):
        return str(hash(self))


if has_daqd_stream:

    has_multi_source = True
    try:
        multi_source_test = daqd_stream.data_plan.multi_source
    except AttributeError:
        has_multi_source = False

    class daq_online_plan:
        def __init__(self, plan, buffer, headers):
            self.plan = plan
            self.buffer = buffer
            self.last_time_seen = daqd_stream.time_point()
            self.headers = headers

        def clear_headers(self):
            self.headers = None

    class DaqOnline:
        def __init__(self, buffer_name):
            self.__daq_reader = daqd_stream.client(buffer_name)
            self.__get_data_has_units = False
            self.__channel_map = {}
            for chan in self.__daq_reader.channels():
                self.__channel_map[chan.name] = chan
            self.__logger = logging.getLogger(__name__ + 'onlineio')

        def set_protocol_revision(self, version):
            if version == 6:
                self.__get_data_has_units = True

        def get_plan(self, channels, stride):
            global has_multi_source

            channel_list = []
            channel_info = []
            for chan in channels:
                basename, chtype, rate = parse_channel_name(chan)
                if chtype not in (None, 'online'):
                    raise IndexErrorWithName([basename, ])
                chtype = 'online'

                reference = self.__channel_map.get(basename, None)
                if reference is None:
                    raise IndexErrorWithName([basename, ])

                if rate is not None:
                    if rate != reference.datarate:
                        raise IndexErrorWithName([basename, ])

                channel_list.append(basename)
                channel_info.append(reference)
            if stride < 0:
                plan_type = daqd_stream.PLAN_TYPE.PLAN_16TH
                size_in_sec = 0
            else:
                plan_type = daqd_stream.PLAN_TYPE.PLAN_SEC
                size_in_sec = stride
            plan = self.__daq_reader.plan_request(plan_type, channel_list, size_in_sec)
            if has_multi_source:
                if stride < 0 and plan.multi_source():
                    plan_type = daqd_stream.PLAN_TYPE.PLAN_SEC
                    size_in_sec = 1
                    stride = 1
                    plan = self.__daq_reader.plan_request(plan_type, channel_list, size_in_sec)
            return daq_online_plan(plan,
                                   np.zeros(plan.required_size(), np.int8),
                                   _generate_channel_info_block(channel_info, stride, has_units=self.__get_data_has_units)), stride

        def write_channel_data_block(self, plan: daq_online_plan, cur, stride, prev_stride, seq_num, cache, writer):
            buffer = plan.buffer
            if plan.plan.type == daqd_stream.PLAN_TYPE.PLAN_16TH:
                initial = (plan.last_time_seen.seconds == 0)
                last_seen = daqd_stream.time_point()
                last_seen.seconds = plan.last_time_seen.seconds
                last_seen.nano = plan.last_time_seen.nano
                done = False
                while not done:
                    status = self.__daq_reader.get_16th_data(plan.plan, last_seen, buffer, daqd_stream.byte_order.BIG)
                    last_seen = status.gps
                    done = (initial and status.cycle == 0) or not initial
                    self.__logger.debug(
                        "done = {0} initial = {1} status.cycle = {2}".format(done, initial, status.cycle))
            else:
                status = self.__daq_reader.get_sec_data(plan.plan, plan.last_time_seen.seconds, stride, buffer, daqd_stream.byte_order.BIG)
            #self.__logger.debug("Requesting second data last_seen = {0}, got gps = {1}".format(plan.last_time_seen.seconds, status.gps.seconds))

            size = len(buffer)

            # write the block header
            # the 4 byte length at the start is the data size + 16 bytes of block
            # header
            secs_in_block = stride
            if stride < 0:
                secs_in_block = 1
            header = struct.pack("!IiIII", size + 16, secs_in_block, status.gps.seconds, status.gps.nano, seq_num)

            if plan.headers is not None:
                writer.write(plan.headers)
                plan.clear_headers()
            writer.write(header)
            writer.write(buffer)
            plan.last_time_seen = status.gps

            if plan.plan.type == daqd_stream.PLAN_TYPE.PLAN_SEC:
                plan.last_time_seen.seconds = status.gps.seconds + stride
                plan.last_time_seen.nano = 0

            if status.plan_status == daqd_stream.PLAN_STATUS.UPDATE_PLAN:
                self.__daq_reader.update_plan(plan.plan)


_aux_online = {}


def register_online(key, online):
    global _aux_online
    _aux_online[key] = online


def online_factory(uri):
    global _aux_online
    if uri is None or uri == "":
        return None
    if uri.startswith("framemap://"):
        name = uri.split("://")[1]
        return OnlineFiles(load_mapping_from_file(name))
    if uri.startswith("daq://"):
        name = uri.split("://")[1]
        return DaqOnline(name)
    if uri in _aux_online:
        return _aux_online[uri]
    return None

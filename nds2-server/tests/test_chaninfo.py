# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import socketserver
import struct
import threading
from unittest import TestCase

from .. import chan_info
from .. import sock_io
from .dummy_socket import DummySocket


class AuthorizeTestServer(socketserver.ForkingMixIn, socketserver.TCPServer):
    pass


class AuthorizeTestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        dat = self.request.recv(1024)
        if dat != b"authorize\n":
            raise Exception("Did not receive authorize, got '{0}'".format(dat))
        self.request.sendall(b'0000')


class AuthorizeTestFailureHandler(socketserver.BaseRequestHandler):
    def handle(self):
        self.request.sendall(b'0001')


class TestNDS2ChanInfo(TestCase):
    def run_server_once(self, server):
        # print("going to handle a server request")
        server.handle_request()

    def test_connect(self):
        server = AuthorizeTestServer(('localhost', 0), AuthorizeTestHandler)
        server.allow_reuse_address = True
        ip, port = server.server_address

        th = threading.Thread(target=self.run_server_once, args=(server,))
        th.start()

        info = chan_info.NDS2ChanInfo(ip, port)
        info.connect()
        # should be able to call this again and do nothing
        info.connect()

        th.join(5)

    def test_connect_fail(self):
        server = AuthorizeTestServer(('localhost', 0), AuthorizeTestFailureHandler)
        server.allow_reuse_address = True
        ip, port = server.server_address

        th = threading.Thread(target=self.run_server_once, args=(server,))
        th.start()

        info = chan_info.NDS2ChanInfo(ip, port)
        self.assertRaises(Exception, info.connect)

        th.join(5)

    def test_close(self):
        class Closeable:
            def __init__(self):
                self.closed = False

            def close(self):
                self.closed = True

        ch = chan_info.NDS2ChanInfo('localhost', 31200)
        ch.close()
        close_obj = Closeable()
        ch.s = close_obj
        ch.close()
        self.assertEqual(close_obj.closed, True)
        self.assertIsNone(ch.s)
        ch.close()

    def test_channel_avail_info(self):
        cinfo1 = {
            "channel_name": "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ",
            "channel_type": "raw",
            "data_type": "int_4",
            "full_name": "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ,raw,256",
            "rate": 256.0,
        }
        cinfo2 = {
            "channel_name": "H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ",
            "channel_type": "raw",
            "data_type": "int_4",
            "full_name": "H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ,raw,256",
            "rate": 256.0,
        }
        cinfo3 = {
            "channel_name": "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ.min",
            "channel_type": "m-trend",
            "data_type": "int_4",
            "full_name": "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ,m-trend,0.16667",
            "rate": 0.16667,
        }
        cinfo4 = {
            "channel_name": "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ.min",
            "channel_type": "s-trend",
            "data_type": "int_4",
            "full_name": "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ,s-trend,1",
            "rate": 1.0,
        }
        samples = (
            (
                ([cinfo1, cinfo2], 0, 1116733655, 1116733675),
                [cinfo1["channel_name"] + " {H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675} " +
                 cinfo2["channel_name"] + " {H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675}", ],
                {
                    cinfo1["full_name"]: [
                        {
                            "frame_type": "H-H1_C",
                            "start": 1116733655,
                            "stop": 1116733675,
                        },
                        {
                            "frame_type": "H-H1_R",
                            "start": 1116733655,
                            "stop": 1116733675,
                        },
                    ],
                    cinfo2["full_name"]: [
                        {
                            "frame_type": "H-H1_C",
                            "start": 1116733655,
                            "stop": 1116733675,
                        },
                        {
                            "frame_type": "H-H1_R",
                            "start": 1116733655,
                            "stop": 1116733675,
                        },
                    ],
                },
            ),
            (
                ([cinfo3, ], 0, 1057428000, 1057429000),
                [cinfo3["channel_name"] + " {H-H1_M:1057428000-1057429000}", ],
                {
                    cinfo3["full_name"]: [
                        {
                            "frame_type": "H-H1_M",
                            "start": 1057428000,
                            "stop": 1057429000,
                        },
                    ],
                }
            ),
            (
                ([cinfo1, cinfo4, cinfo3], 0, 1057428000, 1057429000),
                [cinfo3["channel_name"] + " {H-H1_M:1057428000-1057429000}",
                 cinfo4["channel_name"] + " {H-H1_T:1057428000-1057428500 H-H1_T:1057428600-1057429000}",
                 cinfo1["channel_name"] + " {H-H1_C:1057428000-1057429000}", ],
                {
                    cinfo3["full_name"]: [
                        {
                            "frame_type": "H-H1_M",
                            "start": 1057428000,
                            "stop": 1057429000,
                        },
                    ],
                    cinfo4["full_name"]: [
                        {
                            "frame_type": "H-H1_T",
                            "start": 1057428000,
                            "stop": 1057428500,
                        },
                        {
                            "frame_type": "H-H1_T",
                            "start": 1057428600,
                            "stop": 1057429000,
                        },
                    ],
                    cinfo1["full_name"]: [
                        {
                            "frame_type": "H-H1_C",
                            "start": 1057428000,
                            "stop": 1057429000,
                        },
                    ],
                }
            ),
        )

        for sample in samples:
            ch = chan_info.NDS2ChanInfo('localhost', 31200)
            input = b''
            input += b'0000'  # DAQD_OK for a set-epoch
            for resp in sample[1]:
                input += b'0000' + struct.pack("!I", len(resp)) + resp.encode()
            s = DummySocket(input)
            ch.s = sock_io.BufferedSocket(s)
            out = ch.channel_avail_info(*sample[0])
            exp = sample[2]
            self.assertEqual(len(exp), len(out))
            for entry_key in exp:
                self.assertTrue(entry_key in out)
                exp_entry = exp[entry_key]
                out_entry = out[entry_key]
                self.assertEqual(len(exp_entry), len(out_entry))
                for i in range(len(exp_entry)):
                    self.assertEqual(exp_entry[i]["frame_type"], out_entry[i]["frame_type"])
                    self.assertEqual(exp_entry[i]["start"], out_entry[i]["start"])
                    self.assertEqual(exp_entry[i]["stop"], out_entry[i]["stop"])
            look_for = "set-epoch {0}-{1}".format(sample[0][2], sample[0][3])
            self.assertTrue(s.out_buffer().decode().find(look_for) >= 0)

    def test_channel_list(self):
        samples = (
            (
                (["X1:PEM-1", "X1:PEM-2.min", ], 0, 1000000000),
                ("X1:PEM-1 raw 256 real_4", "X1:PEM-2.min s-trend 1 real_4", "X1:PEM-2.min m-trend 0.16667 real_4"),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,raw,256",
                        "rate": 256.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "s-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,s-trend,1",
                        "rate": 1.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "m-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,m-trend,0.16667",
                        "rate": 0.16667,
                    }
                ],
                "get-channels 0 unknown {X1:PEM-{1,2.min}};\n",
            ),
            (
                (["X1:PEM-1", "X1:PEM-2", ], 0, 1000000000, "test-pt"),
                ("X1:PEM-1 test-pt 2048 real_4", "X1:PEM-2 test-pt 2048 real_4"),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "test-pt",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,test-pt,2048",
                        "rate": 2048.0,
                    },
                    {
                        "channel_name": "X1:PEM-2",
                        "channel_type": "test-pt",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2,test-pt,2048",
                        "rate": 2048,
                    },
                ],
                "get-channels 0 test-pt {X1:PEM-{1,2}};\n",
            ), (
                (["X1:PEM-1", "Y1:PEM-2.min", ], 0, 1000000000),
                ("X1:PEM-1 raw 256 real_4",),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,raw,256",
                        "rate": 256.0,
                    },
                ],
                "get-channels 0 unknown {{X1:PEM-1,Y1:PEM-2.min}};\n",
            ), (
                ([], 0, 1000000000),
                ("X1:PEM-1 raw 256 real_4", "X1:PEM-2.min s-trend 1 real_4", "X1:PEM-2.min m-trend 0.16667 real_4"),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,raw,256",
                        "rate": 256.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "s-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,s-trend,1",
                        "rate": 1.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "m-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,m-trend,0.16667",
                        "rate": 0.16667,
                    }
                ],
                "get-channels 0 unknown;\n",
            ), (
                (["X1:PEM-1"], 0, 1000000000),
                ("X1:PEM-1 raw 256 real_4",),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,raw,256",
                        "rate": 256.0,
                    },
                ],
                "get-channels 0 unknown {X1:PEM-1};\n",
            ), (
                (["X1:PEM-1"], 0, 1000000000, 's-trend'),
                ("X1:PEM-1.n s-trend 1 real_4",),
                [
                    {
                        "channel_name": "X1:PEM-1.n",
                        "channel_type": "s-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1.n,s-trend,1",
                        "rate": 1.0,
                    },
                ],
                "get-channels 0 s-trend {X1:PEM-1};\n",
            ), (
                (["X1:PEM-1", "X1:PEM-1_PROCESSED"], 0, 1000000000, 'raw'),
                ("X1:PEM-1 raw 1024 real_4", "X1:PEM-1_PROCESSED raw 1024 real_4"),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,raw,1024",
                        "rate": 1024.0,
                    },
                    {
                        "channel_name": "X1:PEM-1_PROCESSED",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1_PROCESSED,raw,1024",
                        "rate": 1024.0,
                    },
                ],
                "get-channels 0 raw {X1:PEM-{1,1_PROCESSED}};\n",
            )
        )
        for sample in samples:
            ch = chan_info.NDS2ChanInfo('localhost', 31200)
            input = b"0000" + b"0000"
            input += struct.pack("!I", len(sample[1]))
            for listing in sample[1]:
                input += struct.pack("!I", len(listing) + 1)
                input += listing.encode() + b"\0"
            s = DummySocket(input)
            ch.s = sock_io.BufferedSocket(s)
            out = ch.channel_list(*sample[0])
            exp = sample[2]
            self.assertEqual(len(out), len(exp))
            for i in range(len(exp)):
                exp_entry = exp[i]
                out_entry = out[i]
                self.assertEqual(exp_entry["channel_name"], out_entry["channel_name"])
                self.assertEqual(exp_entry["channel_type"], out_entry["channel_type"])
                self.assertEqual(exp_entry["data_type"], out_entry["data_type"])
                self.assertEqual(exp_entry["full_name"], out_entry["full_name"])
                self.assertEqual(exp_entry["rate"], out_entry["rate"])
            self.assertTrue(sample[3].encode() in s.out_buffer())

    def test_channel_info_cache(self):
        samples = (
            (
                (["X1:PEM-1", "X1:PEM-2.min", ], 0, 1000000000),
                ("X1:PEM-1 raw 256 real_4", "X1:PEM-2.min s-trend 1 real_4", "X1:PEM-2.min m-trend 0.16667 real_4"),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,raw,256",
                        "rate": 256.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "s-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,s-trend,1",
                        "rate": 1.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "m-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,m-trend,0.16667",
                        "rate": 0.16667,
                    }
                ],
                "get-channels 0 unknown {X1:PEM-{1,2.min}};\n",
            ), (
                (["X1:PEM-1", "X1:PEM-2.min", ], 0, 1000000000),
                ("X1:PEM-1 raw 256 real_4", "X1:PEM-2.min s-trend 1 real_4", "X1:PEM-2.min m-trend 0.16667 real_4"),
                [
                    {
                        "channel_name": "X1:PEM-1",
                        "channel_type": "raw",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-1,raw,256",
                        "rate": 256.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "s-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,s-trend,1",
                        "rate": 1.0,
                    },
                    {
                        "channel_name": "X1:PEM-2.min",
                        "channel_type": "m-trend",
                        "data_type": "real_4",
                        "full_name": "X1:PEM-2.min,m-trend,0.16667",
                        "rate": 0.16667,
                    }
                ],
                "",
            ),
        )
        ch = chan_info.NDS2ChanInfo('localhost', 31200)
        input = b""
        sample = samples[0]
        input += b"0000" + b"0000"
        input += struct.pack("!I", len(sample[1]))
        for listing in sample[1]:
            input += struct.pack("!I", len(listing) + 1)
            input += listing.encode() + b"\0"

        s = DummySocket(input)
        ch.s = sock_io.BufferedSocket(s)
        exp_cmd_out = "set-epoch 0-1000000000;\n"
        for sample in samples:
            out = ch.channel_list(*sample[0])
            exp = sample[2]
            self.assertEqual(len(out), len(exp))
            for i in range(len(exp)):
                exp_entry = exp[i]
                out_entry = out[i]
                self.assertEqual(exp_entry["channel_name"], out_entry["channel_name"])
                self.assertEqual(exp_entry["channel_type"], out_entry["channel_type"])
                self.assertEqual(exp_entry["data_type"], out_entry["data_type"])
                self.assertEqual(exp_entry["full_name"], out_entry["full_name"])
                self.assertEqual(exp_entry["rate"], out_entry["rate"])
            exp_cmd_out += sample[3]

        self.assertEqual(s.out_buffer().decode(), exp_cmd_out)

    def test_channel_avail_info_passthrough(self):
        samples = (
            (
                (["H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ.min", ], 0, 1057428000, 1057429000),
                "some data that should be proxied back",
                """set-epoch 1057428000-1057429000;
get-source-data 0 {H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ.min};
""",
            ),
        )
        for sample in samples:
            ch = chan_info.NDS2ChanInfo('localhost', 31200)
            exp = b"0000" + struct.pack("!I", len(sample[1])) + sample[1].encode()
            input = b"0000" + exp
            s = DummySocket(input)
            sout = DummySocket("")
            out_buf = sock_io.BufferedSocket(sout)
            ch.s = sock_io.BufferedSocket(s)
            args = list(sample[0])
            args.append(out_buf)
            ch.channel_avail_info_passthrough(*args)
            self.assertEqual(exp, sout.out_buffer())
            self.assertEqual(sample[2].encode(), s.out_buffer())

    def test_channel_source_info_passthrough(self):
        samples = (
            (
                (["H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ.min", ], 0, 1057428000, 1057429000),
                "some data that should be proxied back",
                """set-epoch 1057428000-1057429000;
get-source-list 0 {H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ.min};
""",
            ),
        )
        for sample in samples:
            ch = chan_info.NDS2ChanInfo('localhost', 31200)
            exp = b"0000" + struct.pack("!I", len(sample[1])) + sample[1].encode()
            input = b"0000" + exp
            s = DummySocket(input)
            sout = DummySocket("")
            out_buf = sock_io.BufferedSocket(sout)
            ch.s = sock_io.BufferedSocket(s)
            args = list(sample[0])
            args.append(out_buf)
            ch.channel_source_info_passthrough(*args)
            self.assertEqual(exp, sout.out_buffer())
            self.assertEqual(sample[2].encode(), s.out_buffer())

    def test_channel_crc(self):
        samples = (
            b"1234",
            b"3456",
            b"ab",
        )
        for sample in samples:
            ch = chan_info.NDS2ChanInfo('localhost', 31200)
            input = b"0000" + sample
            s = DummySocket(input)
            ch.s = sock_io.BufferedSocket(s)
            if len(sample) == 4:
                self.assertEqual(sample, ch.channel_crc())
            else:
                self.assertRaises(Exception, ch.channel_crc)


    def test_channel_list_passthrough(self):
        samples = (
            (
                (0, "raw", "X1:PEM*", 0, 1000000000),
                ["X1:PEM-1 raw 256 real_4", "X1:PEM-2 raw 16 real_4"],
            ),
            (
                (0, "unkown", "*", 0, 1000000000),
                [
                    "X1:PEM-1 raw 256 real_4",
                    "X1:PEM-2 raw 16 real_4",
                    "X1:PEM-2.min s-trend 1 real_4",
                    "X1:PEM-2.min m-trend 0.16667 real_4",
                ],
            )
        )
        for sample in samples:
            ch = chan_info.NDS2ChanInfo('localhost', 31200)
            exp = b"0000" + struct.pack("!I", len(sample[1]))
            input = b"0000" + b"0000" + struct.pack("!I", len(sample[1]))
            for listing in sample[1]:
                tmp = struct.pack("!I", len(listing) + 1) + listing.encode() + b"\0"
                input += tmp
                exp += tmp
            s = DummySocket(input)
            sout = DummySocket("")
            out_buf = sock_io.BufferedSocket(sout)
            ch.s = sock_io.BufferedSocket(s)
            args = list(sample[0])
            args.append(out_buf)
            out = ch.channel_list_passthrough(*args)
            self.assertEqual(exp, sout.out_buffer())

    def test_channel_count_passthrough(self):
        samples = (
            (
                (0, "raw", "X1:PEM*", 0, 1000000000),
                5
            ),
        )
        for sample in samples:
            ch = chan_info.NDS2ChanInfo('localhost', 31200)
            exp = b"0000" + struct.pack("!I", sample[1])
            input = b"0000" + exp
            s = DummySocket(input)
            sout = DummySocket("")
            out_buf = sock_io.BufferedSocket(sout)
            ch.s = sock_io.BufferedSocket(s)
            args = list(sample[0])
            args.append(out_buf)
            out = ch.channel_count_passthrough(*args)
            self.assertEqual(exp, sout.out_buffer())

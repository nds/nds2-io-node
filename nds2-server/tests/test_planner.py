from unittest import TestCase

from .. import planner
from ..errors import DAQDException


class TestPlannerMatrixCell(TestCase):
    def test_matrix_cell(self):
        cell1 = planner.PlannerMatrixCell()
        self.assertEqual(cell1.start, 0)
        self.assertEqual(cell1.stop, 0)
        self.assertListEqual(cell1.type, [])
        cell1.add_frame_type(None)
        self.assertListEqual(cell1.type, [])
        cell1.start = 5
        cell1.stop = 10
        cell1.add_frame_type('FR1')
        self.assertListEqual(cell1.type, ['FR1', ])
        cell1.add_frame_type(None)
        self.assertListEqual(cell1.type, ['FR1', ])
        cell1.add_frame_type('FR1')
        self.assertListEqual(cell1.type, ['FR1', ])
        cell1.add_frame_type('FR2')
        self.assertListEqual(cell1.type, ['FR1', 'FR2'])

        cell2 = planner.PlannerMatrixCell(15, 20, ['FR3', ])
        self.assertEqual(cell2.start, 15)
        self.assertEqual(cell2.stop, 20)
        self.assertListEqual(cell2.type, ['FR3', ])

        cell3 = cell1.clone_dim()
        self.assertEqual(cell3.start, 5)
        self.assertEqual(cell3.stop, 10)
        self.assertListEqual(cell3.type, [])

        self.assertFalse(cell1 == cell2)
        self.assertFalse(cell1 == cell3)
        self.assertTrue(cell1 == planner.PlannerMatrixCell(5, 10, ['FR1', 'FR2']))


class TestPlannerMatrixRow(TestCase):
    def check_row_order(self, row):
        """
        :param row: Row to check
        :return: True if the column cells are in ascending order with no overlap,
        else False.
        """
        if len(row) <= 1:
            return
        for i in range(len(row) - 1):
            col0 = row[i]
            col1 = row[i + 1]
            self.assertTrue(col0.start < col0.stop)
            self.assertTrue(col1.start < col1.stop)
            self.assertTrue(col0.stop <= col1.start)

    def verify_row_contents(self, row, expected):
        """
        Verify that the row contains a given set of cells but do not check the ordering.
        :param row:  Row to verify
        :param expected: Tuple of segments (([Frame types], start, stop), ([...], start, stop), ...)
        :return: True if the given cell exists (ordering not checked) else False
        """
        self.assertEqual(len(row), len(expected))
        for exp in expected:
            match = False
            for cur in row:
                cur_types = set(cur.type)
                exp_types = set(exp[0])
                if cur.start == exp[1] and cur.stop == exp[2] and cur_types == exp_types:
                    match = True
                    break
            self.assertTrue(match)

    def verify_row_state(self, row, expected):
        """
        :param row: Row to verify
        :param expected: Tuple of segments (([Frame types], start, stop), ([...], start, stop), ...)
        :return: True if row has a state matching expected, else False
        """
        self.check_row_order(row)
        self.assertEqual(len(row), len(expected))
        for i in range(len(row)):
            cur = row[i]
            exp = expected[i]
            cur_types = set(cur.type)
            exp_types = set(exp[0])
            self.assertEqual(cur.start, exp[1])
            self.assertEqual(cur.stop, exp[2])
            self.assertEqual(cur_types, exp_types)

    def test_create_matrix_row(self):
        name = "row"
        r1 = planner.PlannerMatrixRow(name)
        self.assertEqual(r1.channel, name)
        self.assertListEqual(r1.columns, [])

        r2 = planner.PlannerMatrixRow(name, [0,5])
        self.assertEqual(r2.channel, name)
        self.assertEqual(len(r2.columns), 1)
        self.assertEqual(r2.columns[0].start, 0)
        self.assertEqual(r2.columns[0].stop, 5)
        self.assertListEqual(r2.columns[0].type, [])

        r3 = planner.PlannerMatrixRow(name, [0, 5, 6])
        self.assertEqual(r3.channel, name)
        self.assertEqual(len(r3.columns), 2)
        self.assertEqual(r3.columns[0].start, 0)
        self.assertEqual(r3.columns[0].stop, 5)
        self.assertEqual(r3.columns[1].start, 5)
        self.assertEqual(r3.columns[1].stop, 6)

    def test_add_frame_type(self):
        r1 = planner.PlannerMatrixRow("row", [0,4,6])
        r1.add_frame_type(0, 6, "FR1")
        for column in r1.columns:
            self.assertListEqual(column.type, ["FR1"])

        r2 = planner.PlannerMatrixRow("row", [0, 4, 6])
        r2.add_frame_type(4, 6, "FR1")
        self.assertListEqual(r2.columns[0].type, [])
        self.assertListEqual(r2.columns[1].type, ["FR1"])

    def test_matrix_row(self):
        row1 = planner.PlannerMatrixRow('chan1')
        self.assertEqual(row1.channel, 'chan1')
        self.assertEqual(len(row1), 0)

        # add segments to the same spot
        row2 = planner.PlannerMatrixRow('chan1', [0, 5])
        row2.add_frame_type(0, 5, "FR1")
        self.assertEqual(len(row2), 1)
        self.assertEqual(row2[0], planner.PlannerMatrixCell(0, 5, ['FR1', ]))
        self.check_row_order(row2)

    def test_matrix_row_order(self):
        row1 = planner.PlannerMatrixRow('chan1')
        row1.order_cells()
        self.assertEqual(len(row1), 0)

        row2 = planner.PlannerMatrixRow('chan2')
        row2.columns.append(planner.PlannerMatrixCell(0, 5, ('FR1',)))
        row2.order_cells()
        self.assertEqual(len(row2), 1)
        self.assertTrue(row2[0].start == 0 and row2[0].stop == 5)

        row3 = planner.PlannerMatrixRow('chan3')
        row3.columns.append(planner.PlannerMatrixCell(0, 5, ('FR1',)))
        row3.columns.append(planner.PlannerMatrixCell(5, 10, ('FR1',)))
        row3.order_cells()
        self.assertEqual(len(row3), 2)
        self.assertTrue(row3[0].start == 0 and row3[0].stop == 5)
        self.assertTrue(row3[1].start == 5 and row3[1].stop == 10)

        row4 = planner.PlannerMatrixRow('chan3')
        row4.columns.append(planner.PlannerMatrixCell(5, 10, ('FR1',)))
        row4.columns.append(planner.PlannerMatrixCell(0, 5, ('FR1',)))
        row4.order_cells()
        self.assertEqual(len(row4), 2)
        self.assertTrue(row4[0].start == 0 and row4[0].stop == 5)
        self.assertTrue(row4[1].start == 5 and row4[1].stop == 10)


    def test_matrix_row_is_contiguous(self):
        def test_contiguous(segments, expected):
            row = planner.PlannerMatrixRow("ch")
            for seg in segments:
                cell = planner.PlannerMatrixCell(seg[1], seg[2], seg[0])
                row.columns.append(cell)
            row.order_cells()
            self.assertEqual(row.is_contiguous(), expected)

        test_contiguous((), False)
        test_contiguous(((('FR1',), 0, 5),), True)
        test_contiguous((((), 0, 5),), False)
        test_contiguous((
             (('FR1',), -1, 0),
             (('FR1', 'FR2'), 0, 1),
             (('FR2',), 1, 5),
             ), True)
        test_contiguous((((), -2, 5), (('FR1',), -1, 2), (('FR2',), 1, 5),), False)
        test_contiguous(((('FR1',), 0, 5), (('FR1',), 6, 7),), False)
        test_contiguous(((('FR1',), 0, 5), (('FR2',), 6, 7),), False)
        test_contiguous(((('FR1',), 0, 5), (('FR3',), 5, 6), (('FR2',), 6, 7),), True)
        test_contiguous(((('FR1',), 0, 5), (('FR1',), 5, 6), (('FR2',), 6, 7),), True)

    def test_find_boundaries(self):
        avail = {
            'ch1': [
                {
                    "frame_type": "FR1", "start": 0, "stop": 5,
                },
                {
                    "frame_type": "FR1", "start": 6, "stop": 8,
                }
            ],
            "ch2": [
                {
                    "frame_type": "FR1", "start": 0, "stop": 8,
                },
                {
                    "frame_type": "FR2", "start": 2, "stop": 7,
                }
            ]
        }
        points = planner.PlannerMatrixRow.find_boundaries(avail)
        self.assertListEqual([0, 2, 5, 6, 7, 8], points)

        avail2 = {
            'ch1': [
                {
                    "frame_type": "FR1", "start": 0, "stop": 5,
                },
                {
                    "frame_type": "FR2", "start": 0, "stop": 5,
                }
            ],
        }
        points2 = planner.PlannerMatrixRow.find_boundaries(avail2)
        self.assertListEqual([0, 5], points2)


class TestPlannerMatrix(TestCase):
    def create_avail_list(self, segments):
        """
        Return an availability map from a list of [(channel, start, stop, frame_type), ...]
        :param segments:
        :return:
        """
        avail = {}
        for segment in segments:
            name = segment[0]
            seg = {
                "frame_type": segment[1],
                "start": segment[2],
                "stop": segment[3],
            }
            if name not in avail:
                avail[name] = []
            avail[name].append(seg)
        return avail

    def check_for_identical_row_structure(self, m):
        self.assertEqual(len(m.channels), len(m.rows))
        i = 0
        for row in m.rows:
            if i != 0:
                ref_row = m.rows[0]
                self.assertEqual(len(row), len(ref_row))
                for i in range(len(row)):
                    self.assertEqual(row[i].start, ref_row[i].start)
                    self.assertEqual(row[i].stop, ref_row[i].stop)
            i += 1
        return True

    def check_row(self, row, ref_data):
        """
        :param row: row to check
        :param ref_data: list of reference data [(start, stop, (frame types)), ...]
        :return:
        """
        self.assertEqual(len(row.columns), len(ref_data))
        for i in range(len(row.columns)):
            col = row.columns[i]
            ref = ref_data[i]
            self.assertEqual(col.start, ref[0])
            self.assertEqual(col.stop, ref[1])
            self.assertEqual(len(col.type), len(ref[2]))
            for type in ref[2]:
                self.assertTrue(type in col.type)


    def create_simple_1(self, avail={}):
        return planner.PlannerMatrix(('chan1',), avail)

    def create_simple_2(self, avail={}):
        return planner.PlannerMatrix(('chan1', 'chan2'), avail)

    def create_simple_3(self, avail={}):
        return planner.PlannerMatrix(('chan1', 'chan2', 'chan3'), avail)

    def test_create_planner_matrix(self):
        self.assertRaises(Exception, planner.PlannerMatrix, ())
        m1 = self.create_simple_1()
        self.assertTupleEqual(m1.channels, ('chan1',))
        m2 = self.create_simple_2()
        self.assertTupleEqual(m2.channels, ('chan1', 'chan2'))
        m3 = self.create_simple_3()
        self.assertTupleEqual(m3.channels, ('chan1', 'chan2', 'chan3'))

    def test_create_planner_matrix_with_avail(self):
        avail1 = {
            'ch1': [
                {
                    "frame_type": "FR1", "start": 0, "stop": 5,
                },
                {
                    "frame_type": "FR1", "start": 6, "stop": 8,
                }
            ],
            "ch2": [
                {
                    "frame_type": "FR1", "start": 0, "stop": 8,
                },
                {
                    "frame_type": "FR2", "start": 2, "stop": 7,
                }
            ]
        }
        avail2 = {
            'ch1': [
                {
                    "frame_type": "FR1", "start": 0, "stop": 5,
                },
                {
                    "frame_type": "FR2", "start": 0, "stop": 5,
                }
            ],
        }
        avail3 = {
            'ch1': [
                {
                    "frame_type": "FR1", "start": 0, "stop": 5,
                },
            ],
        }
        m1 = planner.PlannerMatrix(('ch1', 'ch2'), avail1)
        self.assertEqual(len(m1.rows), 2)
        self.check_for_identical_row_structure(m1)
        self.check_row(m1.rows[0],[
            (0, 2, ("FR1",)),
            (2, 5, ("FR1",)),
            (5, 6, ()),
            (6, 7, ("FR1",)),
            (7, 8, ("FR1",)),
        ])
        self.check_row(m1.rows[1],[
            (0, 2, ("FR1",)),
            (2, 5, ("FR1", "FR2")),
            (5, 6, ("FR1", "FR2")),
            (6, 7, ("FR1", "FR2")),
            (7, 8, ("FR1",)),
        ])
        m2 = planner.PlannerMatrix(('ch1',), avail2)
        self.assertEqual(len(m2.rows), 1)
        self.check_for_identical_row_structure(m2)
        self.check_row(m2.rows[0], [
            (0, 5, ("FR1", "FR2")),
        ])

        m3 = planner.PlannerMatrix(('ch1',), avail3)
        self.assertEqual(len(m3.rows), 1)
        self.check_for_identical_row_structure(m3)
        self.check_row(m3.rows[0], [
            (0, 5, ("FR1",)),
        ])

    def do_test_add_segments(self, channels,
                             segments, exp_structure,
                             test_frame_types, exp_contiguous,
                             exp_start=None, exp_stop=None):
        """

        :param channels: The list of channels to use
        :param segments:  Segments to add tuple/list of ('chan', 'frame_type', start, stop)
        :param exp_structure:  Expected end structure type tuple/list of [start, stop) pairs
        :param test_frame_types: Sampling of frame types, tupe/list of ('chan', column_index, ['frame types', ])
        :param exp_contiguous: True/False are all rows expected to be contiguous
        :param exp_start: Start time of all the rows (checked if not None)
        :param exp_stop: Stop time of all the rows (checked if not None)
        :return: Nothing
        """
        avail = self.create_avail_list(segments)
        m = planner.PlannerMatrix(channels, avail)
        self.check_for_identical_row_structure(m)
        self.assertEqual(m.get_column_count(), len(exp_structure))
        for i in range(len(exp_structure)):
            cur_exp = exp_structure[i]
            cur_cell = m.rows[0][i]
            self.assertEqual(cur_cell.start, cur_exp[0])
            self.assertEqual(cur_cell.stop, cur_exp[1])
        for cur_test in test_frame_types:
            chan = cur_test[0]
            index = cur_test[1]
            exp_types = set(cur_test[2])
            row = m.rows[m.channels.index(chan)]
            cur_types = set(row[index].type)
            self.assertEqual(exp_types, cur_types)
        self.assertEqual(m.is_contiguous(), exp_contiguous)
        if exp_start is not None:
            self.assertEqual(m.gps_start(), exp_start)
        if exp_stop is not None:
            self.assertEqual(m.gps_stop(), exp_stop)

    def do_test_frame_priorities(self, channels, segments, exp_frame_priorities):
        """
        :param channels: The list of channels to use
        :param segments:  Segments to add tuple/list of ('chan', 'frame_type', start, stop)
        :param exp_frame_priorities: A list/tuple of An ordered lists of priorities for the frame type in the segment
        :return: Nothing
        """
        avail = self.create_avail_list(segments)
        m = planner.PlannerMatrix(channels, avail)
        self.assertEqual(len(m.rows), len(exp_frame_priorities))
        for i in range(len(exp_frame_priorities)):
            exp_priority = exp_frame_priorities[i]
            act_priority = m.get_column_frame_priority(i)
            self.assertEqual(act_priority, exp_priority)

    def do_test_get_frame_types(self, channels, segments, exp_frame_types):
        """
        :param channels: The list of channels to use
        :param segments:  Segments to add tuple/list of ('chan', 'frame_type', start, stop)
        :param exp_frame_types: A list/tuple of ('chan', column_index, start, stop, [expected frame types])
        :return: Nothing
        """
        avail = self.create_avail_list(segments)
        m = planner.PlannerMatrix(channels, avail)
        m.finalize_matrix()

        for cur_expected in exp_frame_types:
            cur_cell = m.get_cell(cur_expected[0], cur_expected[1])
            cur_types = set(cur_cell.type)
            exp_types = set(cur_expected[4])
            self.assertEqual(cur_types, exp_types)
            self.assertEqual(cur_cell.start, cur_expected[2])
            self.assertEqual(cur_cell.stop, cur_expected[3])

    def test_matrix_add_left(self):
        self.do_test_add_segments(
            ('chan1',),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR1', -1, 0),
            ),
            (
                (-1, 0), (0, 5),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan1', 1, ['FR1', ]),
            ),
            True
        )
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', -10, -1),
            ),
            (
                (-10, -1), (-1, 0), (0, 5),
            ),
            (
                ('chan1', 2, ['FR1', ]),
                ('chan1', 1, []),
                ('chan2', 0, ['FR1', ]),
            ),
            False
        )

    def test_matrix_add_right(self):
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', 5, 6),
            ),
            (
                (0, 5), (5, 6),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 1, ['FR1', ]),
            ),
            False
        )
        self.do_test_add_segments(
            ('chan1',),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR1', 5, 6),
            ),
            (
                (0, 5), (5, 6),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan1', 1, ['FR1', ]),
            ),
            True
        )
        self.do_test_add_segments(
            ('chan1',),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR1', 6, 7),
            ),
            (
                (0, 5), (5, 6), (6, 7),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan1', 2, ['FR1', ]),
            ),
            False
        )

    def test_matrix_overlap_left(self):
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', -1, 5),
            ),
            (
                (-1, 0), (0, 5),
            ),
            (
                ('chan1', 0, []),
                ('chan2', 0, ['FR1', ]),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR1', ]),
            ),
            False
        )
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', -1, 5),
                ('chan1', 'FR1', -1, 0),
            ),
            (
                (-1, 0), (0, 5),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, ['FR1', ]),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR1', ]),
            ),
            True
        )

    def test_matrix_overlap_right(self):
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', 0, 6),
            ),
            (
                (0, 5), (5, 6),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, ['FR1', ]),
                ('chan1', 1, []),
                ('chan2', 1, ['FR1', ]),
            ),
            False
        )
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR2', 0, 6),
            ),
            (
                (0, 5), (5, 6),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, ['FR2', ]),
                ('chan1', 1, []),
                ('chan2', 1, ['FR2', ]),
            ),
            False
        )

    def test_matrix_overlap_both(self):
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', -1, 6),
            ),
            (
                (-1, 0), (0, 5), (5, 6),
            ),
            (
                ('chan1', 0, []),
                ('chan2', 0, ['FR1', ]),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR1', ]),
                ('chan1', 2, []),
                ('chan2', 2, ['FR1', ]),
            ),
            False
        )

    def test_matrix_overlap_internal_left(self):
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', 0, 3),
            ),
            (
                (0, 3), (3, 5),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, ['FR1', ]),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, []),
            ),
            False
        )

    def test_matrix_overlap_internal_right(self):
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR2', 3, 5),
            ),
            (
                (0, 3), (3, 5),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, []),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR2', ]),
            ),
            False
        )

    def test_matrix_overlap_internal_mid(self):
        self.do_test_add_segments(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR2', 1, 2),
            ),
            (
                (0, 1), (1, 2), (2, 5),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, []),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR2', ]),
                ('chan1', 2, ['FR1', ]),
                ('chan2', 2, []),
            ),
            False, 0, 5
        )

    def test_matrix_misc(self):
        self.do_test_add_segments(
            ('chan1', 'chan2', 'chan3'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR3', 5, 6),
                ('chan2', 'FR2', 0, 2),
                ('chan2', 'FR1', 2, 6),
                ('chan3', 'FR3', 0, 6),
            ),
            (
                (0, 2), (2, 5), (5, 6),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, ['FR2', ]),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR1', ]),
                ('chan1', 2, ['FR3', ]),
                ('chan2', 2, ['FR1', ]),
            ),
            True
        )
        self.do_test_add_segments(
            ('chan1', 'chan2', 'chan3'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR3', 5, 6),
                ('chan2', 'FR2', 0, 2),
                ('chan2', 'FR1', 2, 6),
                ('chan3', 'FR3', 0, 6),
                ('chan3', 'FR1', 0, 2),
            ),
            (
                (0, 2), (2, 5), (5, 6),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, ['FR2', ]),
                ('chan3', 0, ['FR1', 'FR3']),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR1', ]),
                ('chan3', 1, ['FR3', ]),
                ('chan1', 2, ['FR3', ]),
                ('chan3', 2, ['FR3', ]),
            ),
            True, 0, 6
        )
        self.do_test_add_segments(
            ('chan1', 'chan2', 'chan3', 'chan4'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR3', 5, 6),
                ('chan2', 'FR2', 0, 2),
                ('chan2', 'FR1', 2, 6),
                ('chan3', 'FR3', 0, 6),
                ('chan3', 'FR1', 0, 2),
                ('chan4', 'FR2', 0, 2),
                ('chan4', 'FR2', 6, 7),
            ),
            (
                (0, 2), (2, 5), (5, 6), (6, 7),
            ),
            (
                ('chan1', 0, ['FR1', ]),
                ('chan2', 0, ['FR2', ]),
                ('chan3', 0, ['FR1', 'FR3']),
                ('chan4', 0, ['FR2', ]),
                ('chan1', 1, ['FR1', ]),
                ('chan2', 1, ['FR1', ]),
                ('chan3', 1, ['FR3', ]),
                ('chan1', 2, ['FR3', ]),
                ('chan3', 2, ['FR3', ]),
                ('chan4', 3, ['FR2', ]),
            ),
            False, 0, 7
        )

    def test_gps_time(self):
        m1 = self.create_simple_2(
            self.create_avail_list([
                ('chan1', 'FR1', 0, 5),
                ('chan2', 'FR1', -1, 3),
            ])
        )
        self.assertEqual(m1.gps_start(), -1)
        self.assertEqual(m1.gps_stop(), 5)

    def test_gps_times_empty(self):
        m1 = self.create_simple_1()
        m1.finalize_matrix()
        self.assertEqual(m1.gps_start(), 0)
        self.assertEqual(m1.gps_stop(), 0)

    def test_empty_column_count(self):
        m1 = self.create_simple_1()
        m1.finalize_matrix()
        self.assertEqual(m1.get_column_count(), 0)

    def test_get_frame_priorities(self):
        self.do_test_frame_priorities(
            ('chan1',),
            (
                ('chan1', 'FR1', 0, 5),
            ),
            (
                ['FR1', ],
            ),
        )
        self.do_test_frame_priorities(
            ('chan1', 'chan2', 'chan3', 'chan4'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR2', 0, 2),
                ('chan1', 'FR3', 5, 6),
                ('chan2', 'FR2', 0, 2),
                ('chan2', 'FR1', 2, 6),
                ('chan3', 'FR3', 0, 6),
                ('chan3', 'FR1', 0, 2),
                ('chan4', 'FR2', 0, 2),
                ('chan4', 'FR2', 6, 7),
            ),
            (
                ['FR2', 'FR1', 'FR3'],
                ['FR1', 'FR3'],
                ['FR3', 'FR1'],
                ['FR2', ],
            )
        )

    def test_get_cell_frame_types(self):
        self.do_test_get_frame_types(
            ('chan1',),
            (
                ('chan1', 'FR1', 0, 5),
            ),
            (
                ('chan1', 0, 0, 5, ['FR1', ]),
            ),
        )
        self.do_test_get_frame_types(
            ('chan1', 'chan2'),
            (
                ('chan1', 'FR1', 0, 5),
                ('chan1', 'FR2', 0, 2),
                ('chan2', 'FR2', 2, 5),
            ),
            (
                ('chan1', 0, 0, 2, ['FR1', 'FR2']),
                ('chan1', 1, 2, 5, ['FR1', ]),
                ('chan2', 0, 0, 2, []),
                ('chan2', 1, 2, 5, ['FR2', ]),
            ),
        )


class TestPlanner(TestCase):
    def test_planner1(self):
        sched = planner.Planner()
        avail1 = {
            "chan1": [
                {
                    "frame_type": "FR1",
                    "start": 0,
                    "stop": 5,
                }
            ]
        }
        out1 = sched.plan(['chan1', ], avail1, 0, 5)
        self.assertEqual(len(out1.segments), 1)
        self.assertEqual(len(out1.segments[0].bundles), 1)
        self.assertEqual(out1.segments[0].bundles[0].channels, ["chan1", ])
        self.assertEqual(out1.segments[0].bundles[0].frame_type, "FR1")

    def test_planner1_bad_avail(self):
        sched = planner.Planner()
        avail1 = {
            "chan1": [
                {
                    "frame_type": "FR1",
                    "start": 10,
                    "stop": 5,
                }
            ]
        }
        self.assertRaises(DAQDException, sched.plan, ['chan1', ], avail1, 10, 15)

    def test_planner2(self):
        sched = planner.Planner()
        avail1 = {
            "chan1": [
                {
                    "frame_type": "FR1",
                    "start": 0,
                    "stop": 5,
                }
            ],
            "chan2": [
                {
                    "frame_type": "FR1",
                    "start": 0,
                    "stop": 5,
                },
                {
                    "frame_type": "FR2",
                    "start": 2,
                    "stop": 5,
                }
            ],
        }
        out1 = sched.plan(['chan1', 'chan2'], avail1, 0, 5)
        self.assertEqual(len(out1.segments), 2)
        self.assertEqual(len(out1.segments[0].bundles), 1)
        self.assertEqual(out1.segments[0].bundles[0].channels, ["chan1", "chan2"])
        self.assertEqual(out1.segments[0].bundles[0].frame_type, "FR1")
        self.assertEqual(len(out1.segments[0].bundles), 1)
        self.assertEqual(out1.segments[0].bundles[0].channels, ["chan1", "chan2"])
        self.assertEqual(out1.segments[0].bundles[0].frame_type, "FR1")

    def test_planner2_non_contig(self):
        sched = planner.Planner()
        avail1 = {
            "chan1": [
                {
                    "frame_type": "FR1",
                    "start": 0,
                    "stop": 5,
                }
            ],
            "chan2": [
                {
                    "frame_type": "FR1",
                    "start": 0,
                    "stop": 1,
                },
                {
                    "frame_type": "FR2",
                    "start": 2,
                    "stop": 5,
                }
            ],
        }
        self.assertRaises(DAQDException, sched.plan, ['chan1', 'chan2'], avail1, 0, 5)

    def test_large_input_simple_plan(self):
        sched = planner.Planner()
        chans = ["chan{0}".format(x) for x in range(1500)]
        avail = {}
        for chan in chans:
            avail[chan] = [
                {
                    "frame_type": "FR1",
                    "start": 0,
                    "stop": 5,
                }
            ]
        out = sched.plan(chans, avail, 0, 5)
        self.assertEqual(len(out.segments), 1)
        self.assertEqual(len(out.segments[0].bundles), 1)
        self.assertEqual(len(out.segments[0].bundles[0].channels), len(chans))
        self.assertEqual(out.segments[0].bundles[0].frame_type, "FR1")

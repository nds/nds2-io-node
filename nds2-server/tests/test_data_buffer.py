# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import struct
from unittest import TestCase

from ..data_buffer import Closer, DataBuffer


class DummyBuffer:
    def __init__(self):
        self.out = b""

    def write(self, data):
        self.out += data


class TestDataBuffer(TestCase):
    def test_data_buffer_ram(self):
        db1 = DataBuffer(5)
        self.assertEqual(db1.size(), 5)
        self.assertFalse(db1.is_mmapped())

        db1.write_int32(5)
        self.assertEqual(db1.used(), 4)
        db1.write(b'a')
        self.assertEqual(db1.used(), 5)

        self.assertRaises(Exception, db1.write, 'b')
        self.assertEqual(db1.used(), 5)

        db1_out = DummyBuffer()
        db1.flush_to(db1_out)

        self.assertEqual(db1_out.out, struct.pack("!I", 5) + b'a')
        self.assertRaises(Exception, db1.write, 'b')

        db1.close()

    def test_data_buffer_mmap(self):
        db1 = DataBuffer(20, tip_point=10)
        self.assertEqual(db1.size(), 20)
        self.assertTrue(db1.is_mmapped())

        db1.write_int32(5)
        self.assertEqual(db1.used(), 4)
        db1.write_int32(5)
        db1.write(b'000000000000')
        self.assertEqual(db1.used(), 20)

        self.assertRaises(Exception, db1.write, 'b')
        self.assertEqual(db1.used(), 20)

        db1_out = DummyBuffer()
        db1.flush_to(db1_out)

        self.assertEqual(db1_out.out, struct.pack("!I", 5) * 2 + b'0' * 12)
        self.assertRaises(Exception, db1.write, 'b')

        db1.close()

    def test_data_buffer_context(self):
        with DataBuffer(5, tip_point=10) as db:
            self.assertFalse(db.is_mmapped())
            self.assertEqual(db.size(), 5)

        with DataBuffer(20, tip_point=10) as db:
            self.assertTrue(db.is_mmapped())
            self.assertEqual(db.size(), 20)

        with DataBuffer(5, tip_point=10, scale_tip_point=0.1) as db:
            self.assertTrue(db.is_mmapped())
            self.assertEqual(db.size(), 5)

        with DataBuffer(20, tip_point=10, scale_tip_point=3.0) as db:
            self.assertFalse(db.is_mmapped())
            self.assertEqual(db.size(), 20)


class TestCloser(TestCase):
    def test_closer(self):
        class Closeable:
            def __init__(self, value=0):
                self.closed = False
                self.value = value

            def close(self):
                self.closed = True

        a = Closeable(1)
        b = Closeable(2)
        c = Closeable(3)
        with Closer() as lst:
            self.assertEqual(len(lst), 0)
            lst.add(a)
            self.assertEqual(len(lst), 1)
            lst.add(b)
            self.assertEqual(len(lst), 2)
            lst.add(c)
            self.assertEqual(len(lst), 3)
            self.assertEqual(1, lst[0].value)
            lst[0].value = 2
            self.assertEqual(2, lst[0].value)
        self.assertTrue(a.closed)
        self.assertEqual(2, a.value)
        self.assertTrue(b.closed)
        self.assertTrue(c.closed)

        with Closer() as lst:
            # test nothing to do here
            pass


from collections import namedtuple
from unittest import TestCase

from ..data_source import DataSource
from ..errors import DAQDDataOnTape

fake_stat = namedtuple("fake_stat", ["st_blocks",])

def stat_always_on_tape(fname):
    s = fake_stat(st_blocks=0)
    return s

class TestDataSource(TestCase):

    def test_check_for_latency_with_slow_path(self):
        slow = [
            "/slow/frame.gwf",
            "/slow/other.gwf",
        ]
        fast = [
            "/frame.gwf",
            "/other/frame.gwf",
        ]
        ds = DataSource(chan_info_obj=None,
                        frame_lookup_obj=None,
                        reader_socket=None,
                        remote_proxy=None,
                        high_latency_path="/slow/",)
        for name in slow:
            self.assertRaises(DAQDDataOnTape,
                              ds._check_for_latency,
                              *(name,),
                              **{"stat_func":stat_always_on_tape})
        for name in fast:
            ds._check_for_latency(name, stat_func=stat_always_on_tape)


    def test_check_for_latency_with_no_slow_path(self):
        slow = [
            "/slow/frame.gwf",
            "/slow/other.gwf",
            "/frame.gwf",
            "/other/frame.gwf",
        ]
        ds = DataSource(chan_info_obj=None,
                        frame_lookup_obj=None,
                        reader_socket=None,
                        remote_proxy=None,
                        high_latency_path="", )
        for name in slow:
            self.assertRaises(DAQDDataOnTape,
                              ds._check_for_latency,
                              *(name,),
                              **{"stat_func": stat_always_on_tape})

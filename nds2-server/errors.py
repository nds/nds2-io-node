# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from . import daqd


class DAQDException(Exception):
    def __init__(self, code, msg=None):
        super(DAQDException, self).__init__(msg)
        self.code = code
        self.msg = msg


class DAQDGenericError(DAQDException):
    def __init__(self, msg=None):
        super(DAQDGenericError, self).__init__(daqd.DAQD_ERROR, msg)


class DAQDSyntaxError(DAQDException):
    def __init__(self, msg=None):
        super(DAQDSyntaxError, self).__init__(daqd.DAQD_COMMAND_SYNTAX, msg)


class DAQDDataOnTape(DAQDException):
    def __init__(self, msg=None):
        super(DAQDDataOnTape, self).__init__(daqd.DAQD_DATA_ON_TAPE, msg)


class DAQDFrameIOError(DAQDException):
    def __init__(self, msg=None):
        super(DAQDFrameIOError, self).__init__(b"", msg)

class DAQDFrameErrorChannelNotFound(DAQDException):
    def __init__(self, msg=None):
        super(DAQDFrameErrorChannelNotFound, self).__init__(b"", msg)


class DAQDDataNotFound(DAQDException):
    def __init__(self, msg=None):
        super(DAQDDataNotFound, self).__init__(daqd.DAQD_NOT_FOUND, msg)

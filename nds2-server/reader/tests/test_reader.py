# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase


def run_reader_server(cmd_stream, server=None):
    """
    Run a frame reader server
    :param cmd_stream: A string of the input bytes.
    :param server: A sync server server used to synchronize the system
    :return: The output of the server for that command stream
    """
    from ...memory_cache import register_memory_cache_lookup
    from ..frame_reader import SyncServerMixin
    from ..frame_reader import FrameReaderHandler
    from ...tests.dummy_cache import DummyCache
    from ...tests.dummy_socket import DummySocket

    cmd_stream = cmd_stream + b"quit\n"

    sock_obj = DummySocket(cmd_stream)
    server_obj = server
    if server_obj is None:
        cache = DummyCache()
        register_memory_cache_lookup(cache.get_uri(), cache)
        server_obj = SyncServerMixin()
        server_obj.setup_sync_params(concurrent_jobs=4, cache_uri=cache.get_uri())
    FrameReaderHandler(sock_obj, (), server_obj)
    return sock_obj.out_buffer()


def run_reader_command(command, args, server=None):
    """
    Run a frame reader object with the given command.
    :param command: String version of the command to run
    :param args: the appropriate argument structure
    :return: The bytes returned by the reader command
    """
    from ...memory_cache import register_memory_cache_lookup
    from ..frame_reader import FrameReaderLogic
    from ..frame_reader import SyncServerMixin
    from ...tests.dummy_socket import DummySocket
    from ...tests.dummy_cache import DummyCache
    from ...sock_io import BufferedSocket

    server_obj = server
    if server_obj is None:
        cache = DummyCache()
        register_memory_cache_lookup(cache.get_uri(), cache)
        server_obj = SyncServerMixin()
        server_obj.setup_sync_params(concurrent_jobs=4, cache_uri=cache.get_uri())

    sock_obj = DummySocket("")
    output = BufferedSocket(sock_obj)
    reader_obj = FrameReaderLogic(output, server_obj)
    if command == "read":
        reader_obj.do_read(args)
    return sock_obj.out_buffer()


class TestReader(TestCase):
    def test_reader(self):
        import struct
        import os.path
        from ...tests import create_samples
        from ...tests.tempdir import TempDir
        from ..parser import Parser
        from ...signal_units import extract_signal_units

        with TempDir() as tmp:
            parser = Parser()

            create_samples.create_aggrigate_frame1(tmp.get())
            frame_path = os.path.join(tmp.get(), "X-X1_TEST-1000000000-128.gwf")

            cmd_parts = ['read', frame_path, '0', '1', 'X1:PROC-TEST_1%real_4%reduced%16']

            cmd = "\t".join(cmd_parts) + '\n'
            out_text = run_reader_server(cmd.encode())

            cmd_name, args = parser.parse("\t".join(cmd_parts))
            out_cmd = run_reader_command(cmd_name, args)

            self.assertEqual(out_text, out_cmd)
            result_len = struct.unpack("!I", out_text[0:4])[0]
            self.assertNotEqual(result_len, 0)
            units, dat = extract_signal_units(out_text[4:])
            cur = 0
            while len(dat) > 0:
                self.assertEqual(float(cur), struct.unpack("!f", dat[0:4])[0])
                dat = dat[4:]
                cur += 1

            fail_cmd_parts = ['read', frame_path, '0', '1', 'X1:PROC-TEST_NON_EXISTANT%real_4%reduced%16']
            fail_cmd = "\t".join(fail_cmd_parts)
            fail_text = run_reader_server((fail_cmd + "\n").encode())
            self.assertEqual(len(fail_text), 4)
            self.assertEqual(struct.unpack("!I", fail_text)[0], 0)

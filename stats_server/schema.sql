CREATE TABLE "stats" (
    "id" INTEGER PRIMARY KEY,
    "stat" TEXT NOT NULL UNIQUE,
    "value" INTEGER
);
;
CREATE TABLE "recent_commands" (
    "id" INTEGER PRIMARY KEY,
    "server" TEXT NOT NULL,
    "command" TEXT NOT NULL,
    "timestamp" INTEGER
);

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import logging
import struct

logger = logging.getLogger(__name__)


class BufferedSocket:
    def __init__(self, s):
        self.__buffer = b''
        self.__s = s
        self.__quiet = False

    def quiet(self):
        self.__quiet = True

    def close(self):
        self.__buffer = b''
        self.__s.close()

    def __read(self):
        buf = self.__s.recv(1024)
        if len(buf) == 0:
            raise Exception()
        self.__buffer += buf

    def read(self, count=0):
        if count <= 0:
            result = self.__buffer
            self.__buffer = b''
            return result
        result = b''
        while count > 0:
            segment = count
            buf_len = len(self.__buffer)
            if buf_len < count:
                segment = buf_len
            result += self.__buffer[:segment]
            self.__buffer = self.__buffer[segment:]
            count -= segment
            if count > 0:
                self.__read()
        return result

    def read_line(self):
        idx = self.__buffer.find(b'\n')
        while idx < 0:
            self.__read()
            idx = self.__buffer.find(b'\n')
        line = self.__buffer[:idx]
        self.__buffer = self.__buffer[idx + 1:]
        return line

    def write(self, data):
        if not self.__quiet:
            if len(data) < 4:
                logger.debug("sending data '{0}'".format(data))
            else:
                logger.debug("sending data '{0}...'".format(data[:4]))
        self.__s.sendall(data)

    def write_int32(self, value):
        data = struct.pack("!I", value)
        self.write(data)

    def read_int32(self):
        while len(self.__buffer) < 4:
            self.__read()
        val = struct.unpack_from("!I", self.__buffer)[0]
        self.__buffer = self.__buffer[4:]
        return val

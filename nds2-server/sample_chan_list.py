# Copyright 2019 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import numpy

from .simple_chaninfo import class_types
from .simple_chaninfo import data_types
from .simple_chaninfo import signal_units
from .simple_chaninfo import simple_chan_buffer
from .simple_chaninfo import simple_chan_info


class GPS_Channel_Online(simple_chan_info):
    def __init__(self, name, offset):
        super(GPS_Channel_Online, self).__init__(name,
                                                 data_types.to_data_type("int_4"),
                                                 class_types.to_class_type("online"),
                                                 16.0,
                                                 signal_units(
                                                     offset=0.0,
                                                     slope=1.0,
                                                     units="counts",
                                                 ))
        self.__offset = offset

    def rough_availability(self):
        return [(0, 1999999999), ]

    def detailed_availability(self, start, stop):
        return []

    def get_data(self, start, stop):
        data = []
        for cur in range(start, stop):
            data.extend([cur + self.__offset] * 16)
        return simple_chan_buffer(
            units=self.info(),
            data=numpy.array(data, dtype=numpy.int32)
        )


class GPS_Channel(simple_chan_info):
    def __init__(self, name, offset):
        super(GPS_Channel, self).__init__(name,
                                          data_types.to_data_type("int_4"),
                                          class_types.to_class_type("raw"),
                                          16.0,
                                          signal_units(
                                              offset=0.0,
                                              slope=1.0,
                                              units="counts",
                                          ))
        self.__offset = offset

    def rough_availability(self):
        return [(1000000000, 1999999999), ]

    def detailed_availability(self, start, stop):
        return [(start, stop)]

    def get_data(self, start, stop):
        data = []
        for cur in range(start, stop):
            data.extend([cur + self.__offset] * 16)
        return simple_chan_buffer(
            units=self.info(),
            data=numpy.array(data, dtype=numpy.int32)
        )


class GPS_Channel_STrend(simple_chan_info):
    def __init__(self, name, offset):
        super(GPS_Channel_STrend, self).__init__(name,
                                                 data_types.to_data_type("real_8"),
                                                 class_types.to_class_type("s-trend"),
                                                 1.,
                                                 signal_units(
                                                     offset=0.0,
                                                     slope=1.0,
                                                     units="counts",
                                                 ))
        self.__offset = offset

    def rough_availability(self):
        return [(1000000000, 1999999999), ]

    def detailed_availability(self, start, stop):
        return [(start, stop)]

    def get_data(self, start, stop):
        data = []
        for cur in range(start, stop):
            data.append(cur + self.__offset)
        return simple_chan_buffer(
            units=self.info(),
            data=numpy.array(data, dtype=numpy.double)
        )


def channel_list():
    channels = []
    for i in range(25):
        channels.append(GPS_Channel("GPS_OFF{0}".format(i), i))
        channels.append(GPS_Channel_Online("GPS_OFF{0}".format(i), i))
        channels.append(GPS_Channel_STrend("GPS_OFF{0}.min".format(i), i))
        channels.append(GPS_Channel_STrend("GPS_OFF{0}.max".format(i), i))
        channels.append(GPS_Channel_STrend("GPS_OFF{0}.mean".format(i), i))
    return channels

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import os
import socket
import threading
import time

has_requests = True
try:
    import requests
except ImportError:
    has_requests = False

has_statsd = True
try:
    import statsd
except ImportError:
    has_statsd = False


class StatsIgnoreHandler:
    """
    A statistics object that just ignores everything.
    """

    def start_command(self, text):
        pass

    def add_channel_list(self, channels):
        pass

    def add_bytes_sent(self, byte_count):
        pass

    def finish_command(self):
        pass

    def close(self):
        pass


class SimpleBucketStats(object):
    """
    Take stats and put them in simple buckets.
    You must override send_stats to do anything.
    """

    def __init__(self):
        self.commfand = ""
        self.stats = {}
        self.timestamp = 0

    def send_stats(self, stats):
        pass

    def start_command(self, text):
        self.command = text.split()[0]
        self.stats = {}
        self.timestamp = time.time()

    def add_channel_list(self, channels):
        if self.command == "":
            return

        care_about = (
            "check-data"
            "get-data",
            "get-source-data",
            "get-source-list",
        )

        if self.command in care_about:
            channel_count = len(channels)
            key = "cmd.{0}.channel_count".format(self.command)
            self.stats[key] = channel_count
            bucket = "large"
            if channel_count < 1:
                return
            if channel_count == 1:
                bucket = "1"
            elif channel_count == 2:
                bucket = "2"
            elif channel_count <= 5:
                bucket = "5"
            elif channel_count <= 10:
                bucket = "10"
            bucket_key = "cmd.{0}.channel_count_bucket{1}".format(
                self.command,
                bucket,
            )
            self.stats[bucket_key] = 1

    def add_bytes_sent(self, byte_count):
        if self.command == "":
            return

        care_about = (
            "get-data",
            "get-channels",
            "get-source-data",
            "get-source-list",
        )

        if self.command in care_about:
            mb_count = byte_count // 1024 * 1024
            key = "cmd.{0}.mb_sent".format(self.command)
            self.stats[key] = mb_count
            bucket = "large"
            if byte_count <= 64 * 1024:
                bucket = "lte_64k"
            elif byte_count <= 128 * 1024:
                bucket = "lte_128k"
            elif byte_count <= 256 * 1024:
                bucket = "lte_256k"
            elif byte_count <= 512 * 1024:
                bucket = "lte_512k"
            elif byte_count <= 1024 * 1024:
                bucket = "lte_1m"
            elif byte_count <= 5 * 1024 * 1024:
                bucket = "lte_5m"
            elif byte_count <= 10 * 1024 * 1024:
                bucket = "lte_10m"
            elif byte_count <= 25 * 1024 * 1024:
                bucket = "lte_25m"
            elif byte_count <= 50 * 1024 * 1024:
                bucket = "lte_50m"
            elif byte_count <= 100 * 1024 * 1024:
                bucket = "lte_100m"
            elif byte_count <= 250 * 1024 * 1024:
                bucket = "lte_250m"
            elif byte_count <= 500 * 1024 * 1024:
                bucket = "lte_500m"
            elif byte_count <= 1024 * 1024 * 1024:
                bucket = "lte_1g"
            bucket_key = "cmd.{0}.bytes_sent.{1}".format(
                self.command,
                bucket
            )
            self.stats[bucket_key] = 1

    def calc_command_time(self):
        delta = time.time() - self.timestamp
        total_time_key = "cmd.{0}.runtime_s".format(self.command)
        self.stats[total_time_key] = int(delta)
        bucket = "large"
        if delta <= 0.3:
            bucket = "lte_0.3s"
        elif delta <= 0.5:
            bucket = "lte_0.5s"
        elif delta <= 1.0:
            bucket = "lte_1s"
        elif delta <= 5.0:
            bucket = "lte_5s"
        elif delta <= 10.0:
            bucket = "lte_10s"
        elif delta <= 30.0:
            bucket = "lte_30s"
        elif delta <= 60.0:
            bucket = "lte_1m"
        elif delta <= 300.0:
            bucket = "lte_5m"
        elif delta <= 900.0:
            bucket = "lte_15m"
        elif delta <= 1800.0:
            bucket = "lte_30m"
        elif delta <= 3600.0:
            bucket = "lte_1h"
        bucket_key = "cmd.{0}.runtime.bucket.{1}".format(
            self.command,
            bucket
        )
        self.stats[bucket_key] = 1

    def finish_command(self):
        if self.command == "":
            return
        self.calc_command_time()
        self.send_stats(self.stats)
        self.command = ""

if has_requests:
    class SimpleHttpStats(SimpleBucketStats):
        """
        Send stats to a simple http server
        """

        def __init__(self, hostname, post_func=requests.post):
            super(SimpleHttpStats, self).__init__()
            self.hostname = "http://" + hostname
            self.pid = os.getpid()
            self.post_func = post_func
            self.send_stats({"connects": 1})

        def close(self):
            self.send_stats({"disconnects": 1})
            self.hostname = None

        def send_stats(self, stats):
            if self.hostname is None:
                return
            data = []
            for item in stats:
                data.append({'key': item, 'value': stats[item]})
            self.post_func(self.hostname + "/ws/stat/",
                           json=data, timeout=1.0)

        def start_command(self, text):
            super(SimpleHttpStats, self).start_command(text)
            data = {
                'command': text,
                'pid': self.pid
            }
            self.post_func(self.hostname + "/ws/running_command/",
                           json=data, timeout=1.0)


    class ThreadedHttpStats:
        def __init__(self, hostname):
            self.stat_queue = []
            self.quit = False
            self.lock = threading.Lock()
            self.signal = threading.Condition(self.lock)
            self.stats = SimpleHttpStats(hostname, self.delayed_post)
            self.background_thread = threading.Thread(target=self.background_loop)
            self.background_thread.daemon = False
            self.background_thread.start()

        def close(self):
            self.stats.close()
            with self.signal:
                self.quit = True

        def start_command(self, text):
            self.stats.start_command(text)

        def add_channel_list(self, channels):
            self.stats.add_channel_list(channels)

        def add_bytes_sent(self, byte_count):
            self.stats.add_bytes_sent(byte_count)

        def finish_command(self):
            self.stats.finish_command()

        def delayed_post(self, url, json, timeout):
            with self.signal:
                self.stat_queue.append({'url': url, 'json': json, 'timeout': timeout})
                self.signal.notify()

        def background_loop(self):

            def get_job():
                with self.signal:
                    while True:
                        if len(self.stat_queue) > 0:
                            return self.stat_queue.pop()
                        if self.quit:
                            return None
                        self.signal.wait()
                return None

            while True:
                job = get_job()
                if job is None:
                    break
                try:
                    requests.post(job['url'], json=job['json'], timeout=job['timeout'])
                except Exception as e:
                    pass
else:
    class SimpleHttpStats(object):
        def __init__(self, hostname):
            raise RuntimeError("The http based statistics objects requires the requests python module be vailable.")


    class ThreadedHttpStats(object):
        def __init__(self, hostname):
            raise RuntimeError("The threaded http based statistics objects requires the requests python module be vailable.")

if has_statsd:
    class StatsdStats(SimpleBucketStats):
        def __init__(self, hostname):
            super(StatsdStats, self).__init__()
            port = 8125
            if ':' in hostname:
                hostname, port = hostname.split(':', 1)
                port = int(port)
            self.statsd = statsd.StatsClient(hostname, port, 'nds2')

        def send_stats(self, stats):
            with self.statsd.pipeline() as pipe:
                for key in stats:
                    pipe.incr(key, stats[key])

        def close(self):
            pass
else:
    class StatsdStats(object):
        def __init__(self, hostname):
            raise RuntimeError("The statsd based statistics objects requires the statsd python module be available.")


class SimpleStats(SimpleBucketStats):
    __max_chunk__ = 8*1024

    def __init__(self, hostname):
        super(SimpleStats, self).__init__()
        port = 8125
        if ':' in hostname:
            hostname, port = hostname.split(':', 1)
            port = int(port)
        self.socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.__dest = (hostname, port)

    def send_stats(self, stats):
        buffer = b""
        for key in stats.keys():
            key_len = len(key)
            if key_len > SimpleStats.__max_chunk__ or key_len == 0:
                continue
            new_data = "{0}\n".format(key).encode(encoding="utf8")
            if len(buffer) + len(new_data) > SimpleStats.__max_chunk__:
                self.socket.sendto(buffer, self.__dest)
                buffer = b""
            else:
                buffer += new_data
        if len(buffer) != 0:
            self.socket.sendto(buffer, self.__dest)

    def close(self):
        self.socket.close()


def stats_factory(uri):
    parts = uri.split("://")
    if parts[0] == "simple-http":
        return SimpleHttpStats(parts[1])
    elif parts[0] == "threaded-http":
        return ThreadedHttpStats(parts[1])
    elif parts[0] == "statsd":
        return StatsdStats(parts[1])
    elif parts[0] == "simple":
        return SimpleStats(parts[1])
    return StatsIgnoreHandler()

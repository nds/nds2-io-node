# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

# symbolic constants for daqd return codes
DAQD_OK = b"0000"
DAQD_ERROR = b"0001"
DAQD_INVALID_CHANNEL_NAME = b"0004"
DAQD_NOT_FOUND = b"000d"
DAQD_COMMAND_SYNTAX = b"0019"
DAQD_DATA_ON_TAPE = b"001a"

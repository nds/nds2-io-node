# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import os.path
import shutil
import tempfile


class TempDir:
    def __init__(self, dir=None):
        self.__tmpdir = tempfile.mkdtemp(dir=dir)

    def close(self):
        if os.path.exists(self.__tmpdir):
            shutil.rmtree(self.__tmpdir, True)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        try:
            self.close()
        except:
            pass
        return False

    def get(self):
        return self.__tmpdir

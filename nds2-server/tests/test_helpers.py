# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase


class TestDtypeToBytes(TestCase):
    def test_dtype_to_bytes(self):
        from ..helpers import dtype_to_bytes
        _dtype_to_bytes = {
            'int_2': 2,
            'int_4': 4,
            'int_8': 8,
            'real_4': 4,
            'real_8': 8,
            'uint_4': 4,
        }
        for key in _dtype_to_bytes:
            ch = {"data_type": key}
            self.assertEqual(dtype_to_bytes(ch), _dtype_to_bytes[key])
        self.assertRaises(Exception, dtype_to_bytes, {"data_type": "unknown"})


class TestSampleByteConversions(TestCase):
    def test_seconds_to_samples(self):
        pass


class TestIntersects(TestCase):
    def test_intersects(self):
        from ..helpers import intersects
        succeed = (
            (0.0, 1.0, 0.0, 1.0),
            (-1.0, 0.0, -0.5, 0.0),
            (0.0, 1.0, 0.0, 0.5),
            (0.0, 1.0, 0.5, 0.75),
            (0.0, 1.0, -0.5, 0.5),
        )
        fail = (
            (0.0, 1.0, 1.0, 1.5),
            (-5.0, 1.0, 2.0, 3.0),
        )
        for entry in succeed:
            self.assertTrue(intersects(entry[0],
                                       entry[1],
                                       entry[2],
                                       entry[3]))
            self.assertTrue(intersects(entry[2],
                                       entry[3],
                                       entry[0],
                                       entry[1]))
        for entry in fail:
            self.assertFalse(intersects(entry[0],
                                        entry[1],
                                        entry[2],
                                        entry[3]))
            self.assertFalse(intersects(entry[2],
                                        entry[3],
                                        entry[0],
                                        entry[1]))


class TestIntersection(TestCase):
    def test_intersection(self):
        from ..helpers import intersection
        samples = (
            ((0.0, 1.0, 0.0, 1.0), (0.0, 1.0)),
            ((-1.0, 0.0, -0.5, 0.0), (-0.5, 0.0)),
            ((0.0, 1.0, 0.0, 0.5), (0.0, 0.5)),
            ((0.0, 1.0, 0.5, 0.75), (0.5, 0.75)),
            ((0.0, 1.0, -0.5, 0.5), (0.0, 0.5)),
            ((0.0, 1.0, 1.0, 1.5), (None, None)),
            ((-5.0, 1.0, 2.0, 3.0), (None, None)),
        )
        for entry in samples:
            test_input = entry[0]
            expected = entry[1]
            self.assertEqual(intersection(*test_input), expected)
            test_input = (entry[0][2], entry[0][3], entry[0][0], entry[0][1])
            self.assertEqual(intersection(*test_input), expected)


class TestRangeContains(TestCase):
    def test_range_contains(self):
        from ..helpers import range_contains
        samples = (
            ((0.0, 1.0, 0.5), True),
            ((0.0, 1.0, 0.0), True),
            ((0.0, 1.0, 0.99999), True),
            ((0.0, 1.0, 1.0), False),
            ((0.0, 1.0, -0.00001), False),
            ((0.0, 1.0, 10.0), False),
            ((0.0, 1.0, -10.0), False),
        )
        for entry in samples:
            test_input = entry[0]
            expected = entry[1]
            self.assertEqual(range_contains(*test_input), expected)


class TestGetFrameTime(TestCase):
    def test_get_frame_times(self):
        from ..helpers import get_frame_name_info
        samples = (
            ("someplace/H-H1_HOFT_C00-1155473408-4096.gwf", "H-H1_HOFT_C00", 1155473408, 1155473408 + 4096),
            ("H-H1_HOFT_C00-1155473408-4096.gwf", "H-H1_HOFT_C00", 1155473408, 1155473408 + 4096),
            ("H-H1_llhoft-1159553692-4.gwf", "H-H1_llhoft", 1159553692, 1159553692 + 4),
            ("H-H1_R-1159499968-64.gwf", "H-H1_R", 1159499968, 1159499968 + 64),
            ("someplace/H-H1_R-1159500032-64.gwf", "H-H1_R", 1159500032, 1159500032 + 64),
            ("", None, None, None),
            ("H-H1_R-1159499968_64.gwf", None, None, None),
            ("H-H1_R-a-1159499968-64.gwf", None, None, None),
        )
        for sample in samples:
            frame_type, start, stop = get_frame_name_info(sample[0])
            self.assertEqual(frame_type, sample[1])
            self.assertEqual(start, sample[2])
            self.assertEqual(stop, sample[3])


class TestFixFrameNames(TestCase):
    def test_fix_frame_names(self):
        from ..helpers import fix_frame_names
        samples = (
            (
                ("",),
                (),
            ),
            (
                ("fname.gwf",),
                ("fname.gwf",),
            ),
            (
                ("fname", "fname..",),
                ("fname.gwf", "fname.gwf",),
            ),
        )
        for sample in samples:
            out = fix_frame_names(sample[0])
            self.assertListEqual(out, list(sample[1]))


class TestOptimizeFrames(TestCase):
    def test_optimize_frame_selection(self):
        from ..helpers import optimize_frame_selection
        samples = (
                ("", "",),
                ("/archive/frames/1", "/ceph/frames/1",),
                ("/archive/frames/2", "/ceph/frames/2",),
                ("/archive/frames/old", "/archive/frames/old",),
                ("/other/1", "/other/1",),
        )

        def mock_exists(fname):
            return not fname.endswith('old')

        for sample in samples:
                out = optimize_frame_selection(sample[0], mock_exists)
                self.assertEqual(out, sample[1])


class TestParseChannelName(TestCase):
    def test_parse_channel_name(self):
        from ..helpers import parse_channel_name
        self.assertTupleEqual(('X1:TEST_CHAN', None, None), parse_channel_name('X1:TEST_CHAN'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, 16.0), parse_channel_name('X1:TEST_CHAN,16'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', None), parse_channel_name('X1:TEST_CHAN,online'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, 16.0), parse_channel_name('X1:TEST_CHAN%16'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', None), parse_channel_name('X1:TEST_CHAN%online'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', 16.0), parse_channel_name('X1:TEST_CHAN,16,online'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', 16.0), parse_channel_name('X1:TEST_CHAN%16%online'))
        self.assertRaises(Exception, parse_channel_name, 'X1:TEST,16,16')


class TestParseFullChannelName(TestCase):
    def test_parse_full_channel_name(self):
        from ..helpers import parse_full_channel_name
        self.assertTupleEqual(('X1:TEST_CHAN', None, None, None), parse_full_channel_name('X1:TEST_CHAN'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, None, 16.0), parse_full_channel_name('X1:TEST_CHAN,16'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', None, None), parse_full_channel_name('X1:TEST_CHAN,online'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, None, 16.0), parse_full_channel_name('X1:TEST_CHAN%16'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', None, None), parse_full_channel_name('X1:TEST_CHAN%online'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', None, 16.0), parse_full_channel_name('X1:TEST_CHAN,16,online'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', None, 16.0), parse_full_channel_name('X1:TEST_CHAN%16%online'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, 'int_4', None), parse_full_channel_name('X1:TEST_CHAN,int_4'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, 'int_8', 16.0), parse_full_channel_name('X1:TEST_CHAN,int_8,16'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', 'real_4', None),
                              parse_full_channel_name('X1:TEST_CHAN,online,real_4'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, 'real_8', None), parse_full_channel_name('X1:TEST_CHAN%real_8'))
        self.assertTupleEqual(('X1:TEST_CHAN', None, 'int_2', 16.0), parse_full_channel_name('X1:TEST_CHAN%16,int_2'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', 'int_2', None),
                              parse_full_channel_name('X1:TEST_CHAN%int_2%online'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', 'int_4', 16.0),
                              parse_full_channel_name('X1:TEST_CHAN%int_4,16,online'))
        self.assertTupleEqual(('X1:TEST_CHAN', 'online', 'real_8', 16.0),
                              parse_full_channel_name('X1:TEST_CHAN%16%online,real_8'))
        self.assertRaises(Exception, parse_full_channel_name, 'X1:TEST,16,16')
        self.assertRaises(Exception, parse_full_channel_name, 'X1:TEST,int_4,int_2')
        self.assertRaises(Exception, parse_full_channel_name, 'X1:TEST,int_4,int_4')

class TestParseGetDataLine(TestCase):
    def test_parse_get_data_line(self):
        from ..helpers import parse_get_data_line
        from ..errors import DAQDSyntaxError
        self.assertTupleEqual((0, 1000000, 0, ['ABC']), parse_get_data_line("0 1000000 {ABC};"))
        self.assertTupleEqual((0, 1000000, 1000000, ['ABC']), parse_get_data_line("0 1000000 1000000 {ABC};"))
        self.assertTupleEqual((0, 1000000, 1000, ['ABC']), parse_get_data_line("0 1000000 1000 {ABC};"))
        self.assertTupleEqual((0, 1000000, 1000, ['ABC', 'DEF', 'GHI']),
                              parse_get_data_line("0 1000000 1000 {ABC DEF GHI};"))
        self.assertTupleEqual((600, 1000000, 1000, ['ABC', 'DEF', 'GHI']),
                              parse_get_data_line("600 1000000 1000 {ABC DEF GHI};"))
        self.assertRaises(DAQDSyntaxError, parse_get_data_line, ('0 0 0 {'))
        self.assertRaises(DAQDSyntaxError, parse_get_data_line, ('a 0 0 {ABC};'))
        self.assertRaises(DAQDSyntaxError, parse_get_data_line, ('0 b 0 {ABC};'))
        self.assertRaises(DAQDSyntaxError, parse_get_data_line, ('0 1 c {ABC};'))
        self.assertRaises(DAQDSyntaxError, parse_get_data_line, ('a b {ABC};'))


class TestParseGetOnlineDataLine(TestCase):
    def test_parse_get_data_line(self):
        from ..helpers import parse_get_online_data_line
        from ..errors import DAQDSyntaxError
        self.assertTupleEqual((0, 0, 1000000, ['ABC']), parse_get_online_data_line("0 1000000 {ABC};"))
        self.assertTupleEqual((0, 1000000, 1000, ['ABC']), parse_get_online_data_line("1000000 1000 {ABC};"))
        self.assertTupleEqual((0, 1000000, 1000, ['ABC', 'DEF', 'GHI']),
                              parse_get_online_data_line("1000000 1000 {ABC DEF GHI};"))
        self.assertRaises(DAQDSyntaxError, parse_get_online_data_line, ('0 0 {'))
        self.assertRaises(DAQDSyntaxError, parse_get_online_data_line, ('0 0 0 {'))
        self.assertRaises(DAQDSyntaxError, parse_get_online_data_line, ('a b {ABC};'))
        self.assertRaises(DAQDSyntaxError, parse_get_online_data_line, ('a {ABC};'))


class TestCalcCacheMaxBlockSize(TestCase):
    def test_calc_cache_max_block_size(self):
        from ..helpers import calc_cache_max_block_size
        channels = [
            {
                'data_type': 'real_4',
                'rate': 16.0,
                'expected_1m': (1024 * 1024) // (16 * 4),
            },
            {
                'data_type': 'real_8',
                'rate': 16.0,
                'expected_1m': (1024 * 1024) // (16 * 8),
            },
            {
                'data_type': 'real_4',
                'rate': 16 * 1024.0,
                'expected_1m': (1024 * 1024) // (16 * 1024 * 4),
            },
            {
                'data_type': 'real_8',
                'rate': 16 * 1024.0,
                'expected_1m': (1024 * 1024) // (16 * 1024 * 8),
            },
            {
                'data_type': 'int_2',
                'rate': 256.0,
                'expected_1m': (1024 * 1024) // (256 * 2),
            },
        ]

        class SizeOnlyCache:
            def max_block_size(self):
                return 1024 * 1024

        cache = SizeOnlyCache()
        for ch in channels:
            result = calc_cache_max_block_size(ch, cache)
            self.assertEqual(result, ch['expected_1m'])

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE


class CacheAdaptor:
    """
    A simple wrapper around a cache that can hold
    onto a specific key if it is set.
    """

    def __init__(self, cache):
        self.__cache = cache
        self.__local_key = None
        self.__local_value = None

    def set(self, key, value):
        """
        Store key=value into the cache, keeping a local copy
        if key is the local key value to keep
        :param key: key
        :param value: Value to set
        :return: what the underlying cache returns
        """
        if key == self.__local_key:
            self.__local_value = value
        return self.__cache.set(key, value)

    def get(self, key):
        """
        Retreive the value stored with key in the cache.
        If key is the local key, and the local copy is not None
        return the local key, else return the value in the underlying
        cache
        :param key: key to search on
        :return: The local copy if key is the local key or the value
        retreived from the underlying cache
        """
        if key == self.__local_key:
            if self.__local_value is not None:
                return self.__local_value
        return self.__cache.get(key)

    def keep_local(self, key):
        """
        Set the local key value
        :param key: key to keep locally, set to None for no local storage
        :return: nothing
        """
        if self.__local_key != key:
            self.__local_value = None
        self.__local_key = key

    def keep_local_key(self):
        """
        :return: The local key to keep, may be None
        """
        return self.__local_key

    def max_block_size(self):
        """
        :return: The max block size that the underlying cache can do
        """
        return self.__cache.max_block_size()

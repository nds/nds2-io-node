#  Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import contextlib
import logging
import time

import socketserver
import threading

try:
    import frameCPP
except ImportError:
    from LDAStools import frameCPP

from ..frame_io import cache_key
from ..frame_io import read_from_frame
from ..helpers import calc_cache_max_block_size
from ..helpers import get_frame_name_info
from ..memory_cache import memory_cache_factory
from ..reader.parser import Parser
from ..reader.cache_entry import CacheEntry


class FrameReaderLogic(object):

    def __init__(self, output, sync_server):
        self.output = output
        self.server = sync_server
        self.logger = logging.getLogger(__name__+'io')

    def do_read(self, args):
        # note this leaves the data with a signal_units blob encoded in front
        # along w/ the data.  The client is expected to split the two apart.
        from ..cache_adaptor import CacheAdaptor

        data = None
        try:
            _cache = self.server.get_cache()
            cache = CacheAdaptor(_cache)

            finfo = get_frame_name_info(args.filename)
            self.logger.info('Serving read request for {0}'.format(finfo[0]))

            with self.server.run_job():
                with self.server.use_frame(args.filename) as (fs, toc):
                    cache_stride = calc_cache_max_block_size(args.channels[0], cache)

                    key = cache_key(finfo[0], finfo[1], args.channels[0], args.frame_offset)
                    cache.keep_local(key)

                    read_from_frame(fs, finfo[0], args.channels[0], finfo[1], args.frame_offset, cache_stride, cache,
                                    toc)
                    # note this leaves the data with a signal_units blob encoded in front
                    # along w/ the data.  The client is expected to split the two apart.
                    data = cache.get(key)
        except:
            self.output.write_int32(0)
            return

        if data is None:
            self.output.write_int32(0)
        else:
            self.output.write_int32(len(data))
            self.output.write(data)


class FrameReaderHandler(socketserver.BaseRequestHandler):
    def setup(self):
        from ..sock_io import BufferedSocket
        self.logger = logging.getLogger(__name__+'conn')
        self.logger.info("Connection from {0}".format(self.client_address))
        self.io = BufferedSocket(self.request)
        self.io.quiet()
        self.logic = FrameReaderLogic(self.io, self.server)
        self.command_parser = Parser()

    def handle(self):
        while True:
            try:
                text = self.io.read_line()
            except:
                break
            cmd, args = self.command_parser.parse(text.decode())
            if cmd == 'read':
                self.logic.do_read(args)
            elif cmd == 'quit':
                break


class SyncServerMixin(object):
    def setup_sync_params(self, concurrent_jobs, cache_uri, frame_cache_max_size=None):
        self.__concurrent_jobs = concurrent_jobs
        self.__cache_uri = cache_uri
        self.__lock = threading.Lock()
        self.__job_condition = threading.Condition(self.__lock)
        self.__jobs_in_process = 0
        self.__jobs_waiting = 0
        max_frames = concurrent_jobs
        if not frame_cache_max_size is None:
            max_frames = frame_cache_max_size
        if max_frames < concurrent_jobs:
            raise RuntimeError("The frame cache size must equal at least the number of concurrent jobs allowed")
        self.__frame_cache_max_size = max_frames
        self.__frame_cache = {}

    def get_sync_param(self, key):
        with self.__lock:
            if key == "concurrent_jobs":
                return self.__concurrent_jobs
            elif key == "cache_uri":
                return self.__cache_uri
            elif key == "jobs_in_process":
                return self.__jobs_in_process
            elif key == "jobs_waiting":
                return self.__jobs_waiting
            elif key == "frame_cache_max_size":
                return self.__frame_cache_max_size
            elif key == "frame_cache_size":
                return len(self.__frame_cache)

    def start_job(self):
        with self.__job_condition:
            self.__jobs_waiting += 1
            while self.__jobs_in_process >= self.__concurrent_jobs:
                self.__job_condition.wait()
            self.__jobs_waiting -= 1
            self.__jobs_in_process += 1

    def stop_job(self):
        with self.__job_condition:
            if self.__jobs_in_process > 0:
                self.__jobs_in_process -= 1
            self.__job_condition.notify()

    def get_cache(self):
        """
        :return: Return a cache object
        """
        return memory_cache_factory(self.__cache_uri)

    @contextlib.contextmanager
    def run_job(self):
        """
        :return: Run a job
        """
        self.start_job()
        yield self
        self.stop_job()

    def frame_cache_eject_a_frame(self):
        """
        Remove the oldest frame from the cache
        :return: 
        """
        if len(self.__frame_cache) == 0:
            return
        oldest = None
        oldest_time = time.time() + 1.0
        for key in self.__frame_cache:
            cur_access_time = self.__frame_cache[key].access_time()
            if cur_access_time < oldest_time and self.__frame_cache[key].use_count() == 0:
                oldest = key
                oldest_time = cur_access_time
        del self.__frame_cache[oldest]

    def acquire_frame(self, path):
        """
        Retrieve a frame and toc from the file specified at path, use the cache if possible
        :param path: path to the frame
        :return: frame, toc objects
        """
        with self.__lock:
            if not path in self.__frame_cache:
                while len(self.__frame_cache) >= self.__frame_cache_max_size:
                    self.frame_cache_eject_a_frame()
                fs = frameCPP.IFrameFStream(path)
                toc = fs.GetTOC()
                self.__frame_cache[path] = CacheEntry(frame=fs, toc=toc)
            return self.__frame_cache[path].get()

    def release_frame(self, path):
        """
        Decrement the use count of the given frame
        :param path: 
        :return: 
        """
        with self.__lock:
            if path in self.__frame_cache:
                self.__frame_cache[path].release()

    @contextlib.contextmanager
    def use_frame(self, path):
        """
        :return: Run a job 
        """
        frame, toc = self.acquire_frame(path)
        yield frame, toc
        self.release_frame(path)

    def query_frame_cache(self, path):
        """
        Diagnostic function, return the number of references to the given frame.
        :param path: path to the frame
        :return: -1 if not in the case, else the number of active references to the frame
        """
        with self.__lock:
            if path in self.__frame_cache:
                return self.__frame_cache[path].use_count()
            return -1


class FrameReaderServer(socketserver.ThreadingMixIn, socketserver.UnixStreamServer, SyncServerMixin):
    pass


if __name__ == "__main__":
    import argparse
    import sys

    parser = argparse.ArgumentParser(
        description="""Framio reader node.  This centralises reading on a individual nds2 server node.""")
    default = "memcached://127.0.0.1"
    parser.add_argument('-m', '--memcache', default=default,
                        help="Memcache servers to connect to.  "
                             "Formatted as memcached://host[,host,host,"
                             "...].  Default of [{0}]".format(
                            default))
    default = 16
    parser.add_argument('-r', '--readers', default=default,
                        help="Number of simultanious readers allowed.  "
                             "Default of [{0}]".format(default),
                        type=int)
    parser.add_argument('-f', '--max-frames', default=default,
                        help="Number of frames that can be cached at a time.  "
                             "Must be greater or equal to the number of readers.  "
                             "Default of [{0}]".format(default),
                        type=int)
    default = 'frame_io_socket'
    parser.add_argument('-s', '--socket', default=default,
                        help="Path to the unix socket to listen on.  Default of [{0}]".format(default))
    args = parser.parse_args()
    if args.max_frames < args.readers or args.readers < 1:
        parser.print_usage()
        sys.exit(1)

    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)

    server = FrameReaderServer(args.socket, FrameReaderHandler)
    server.setup_sync_params(concurrent_jobs=args.readers,
                             cache_uri=args.memcache,
                             frame_cache_max_size=args.max_frames)
    server.allow_reuse_address = True

    logger.info("Frame IO Reader starting at {0}".format(args.socket))
    logger.info("Memory cache server: {0}".format(args.memcache))
    logger.info("Reader count: {0}".format(args.readers))
    logger.info("Frame cache size: {0}".format(args.max_frames))

    server.serve_forever()

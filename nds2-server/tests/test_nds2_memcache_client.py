# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase


class TestNDS2MemCacheClient(TestCase):
    def test_nds2_mem_cache_client(self):
        from ..memory_cache import NDS2MemCacheClient
        c0 = NDS2MemCacheClient(servers=['somwhere.localdomain', ])
        self.assertEqual(c0.max_block_size(), 1024 * 1024)
        c1 = NDS2MemCacheClient(servers=['somewhere.localdomain', ], server_max_value_length=512 * 1024)
        self.assertEqual(c1.max_block_size(), 512 * 1024)
        self.assertIsNotNone(c1.set)
        self.assertIsNotNone(c1.get)

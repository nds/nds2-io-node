#!/usr/bin/env python

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from __future__ import print_function

import argparse
import random
import re
import struct

try:
    from LDASTools import frameCPP
except:
    import frameCPP

from flask import Flask
from flask import json
from flask import jsonify
from flask import request
from flask import Response

app = Flask(__name__)

# global channel lists
full_channel_list = []
name_lookup = {}


class channel(object):
    """Represents a channel"""

    def __init__(self, name, rate, chtype, dtype, timespans):
        self.name = name
        self.rate = rate
        self.chtype = chtype
        self.dtype = dtype
        self.timespans = timespans

    def full_name(self):
        return "{0},{1},{2},{3}".format(
            self.name,
            self.chtype,
            self.dtype,
            self.rate,
        )

    def __str__(self):
        spans = ""
        for span in self.timespans:
            spans += " {0}-{1}".format(span[0], span[1])
        return self.full_name() + spans


def add_channel(ch):
    """Add a channel to the global channel list"""
    global full_channel_list
    global name_lookup

    if ch.name not in name_lookup:
        name_lookup[ch.name] = []
    lookups = name_lookup[ch.name]

    index = len(full_channel_list)

    full_channel_list.append(ch)
    lookups.append(index)

    name_lookup[ch.name] = lookups


def setup_channels_from_frame(fname):
    def translate_ref_type(ref_type):
        if ref_type == frameCPP.FrVect.FR_VECT_C:
            return "byte_1"
        elif ref_type == frameCPP.FrVect.FR_VECT_2S:
            return "int_2"
        elif ref_type == frameCPP.FrVect.FR_VECT_8R:
            return "real_8"
        elif ref_type == frameCPP.FrVect.FR_VECT_4R:
            return "real_4"
        elif ref_type == frameCPP.FrVect.FR_VECT_4S:
            return "int_4"
        elif ref_type == frameCPP.FrVect.FR_VECT_8S:
            return "int_8"
        elif ref_type == frameCPP.FrVect.FR_VECT_8C:
            return "complex_8"
        elif ref_type == frameCPP.FrVect.FR_VECT_16C:
            return "complex_16"
        # elif ref_type == frameCPP.FrVect.FR_VECT_STRING:
        #     return ""
        # elif ref_type == frameCPP.FrVect.FR_VECT_2U:
        #     return ""
        elif ref_type == frameCPP.FrVect.FR_VECT_4U:
            return "uint_4"
        # elif ref_type == frameCPP.FrVect.FR_VECT_8U:
        #     return ""
        # elif ref_type == frameCPP.FrVect.FR_VECT_1U:
        #    return ""
        return "unknown"

    fs = frameCPP.IFrameFStream(fname)
    toc = fs.GetTOC()
    frame_count = fs.GetNumberOfFrames()
    fr_start = fs.ReadFrameN(0)
    fr_end = fs.ReadFrameN(frame_count - 1)
    earliest = int(fr_start.GetGTime()[0])
    latest = int(fr_end.GetGTime()[0] + fr_end.GetDt())

    print ("Reading channels from frame")
    i = 0
    for entry in toc.GetADC().keys():
        ch_data = fs.ReadFrAdcData(0, entry)
        rate = ch_data.GetSampleRate()
        ref_type = ch_data.RefData()[0].GetType()
        ch_type = translate_ref_type(ref_type)
        add_channel(channel(entry, rate, 'raw', ch_type,
                            [(earliest, latest)]))
        if i % 1000 == 0:
            print("{0} {1} raw {2}".format(entry, rate, ch_type))
        i += 1


def setup_channels(count):
    """Create count base channels.  99% of the channels will have
    a full completment of minute and second trends"""

    # we need repeatable channels for testing
    random.seed(0)
    # some flavor
    IFOs = ['H0', 'H1', 'H2', 'L1', 'L1', 'L2', 'G0', 'G1']
    subs = ['CDS', 'GDS', 'DAQ', 'SYS', 'SUS', 'SEI', 'VAC']
    rates = [16, 256, 1024, 2048, 4096, 8 * 1024, 16 * 1024]

    earliest = 1000000000
    boundry = 1200000000
    latest = 1440000000
    forever = 1999999999

    early_delta = boundry - earliest
    latest_delta = latest - boundry

    add_channel(channel("X0:TEST-CHAN", '4096', 'raw',
                        'real_4', [(earliest, forever)]))

    for i in xrange(count - 1):
        ifo = random.choice(IFOs)
        sub = random.choice(subs)
        rate = random.choice(rates)
        name = "{0}:{1}-CHANNEL_{2}".format(ifo, sub, i)

        ts_prob = random.random()
        if ts_prob <= 0.5:
            timespans = [(earliest, forever)]
        elif ts_prob < 0.65:
            timespans = [
                (earliest, boundry + int(random.random() * latest_delta))]
        elif ts_prob < 0.8:
            timespans = [
                (earliest + int(random.random() * early_delta), latest)]
        else:
            timespans = [(earliest + int(random.random() * early_delta),
                          boundry + int(random.random() * latest_delta))]

        # base channel
        add_channel(channel(name, rate, 'raw', 'real_4', timespans))

        # 99% of the channels will have trends
        if random.random() < 0.99:
            add_channel(channel(name + '.n', '1',
                                's-trend', 'int_4', timespans))
            add_channel(channel(name + '.min', '1',
                                's-trend', 'real_8', timespans))
            add_channel(channel(name + '.max', '1',
                                's-trend', 'real_8', timespans))
            add_channel(channel(name + '.mean', '1',
                                's-trend', 'real_8', timespans))
            add_channel(channel(name + '.rms', '1',
                                's-trend', 'real_8', timespans))

            add_channel(channel(name + '.n', str(1.0 / 60.0),
                                'm-trend', 'int_4', timespans))
            add_channel(channel(name + '.min', str(1.0 / 60.0),
                                'm-trend', 'real_8', timespans))
            add_channel(channel(name + '.max', str(1.0 / 60.0),
                                'm-trend', 'real_8', timespans))
            add_channel(channel(name + '.mean', str(1.0 / 60.0),
                                'm-trend', 'real_8', timespans))
            add_channel(channel(name + '.rms', str(1.0 / 60.0),
                                'm-trend', 'real_8', timespans))
    random.seed()


def is_float(val):
    try:
        float(val)
    except:
        return False
    return True


def chan_name_to_idx(name):
    global name_lookup
    global full_channel_list

    name = name.replace('%', ',')
    parts = name.split(',')

    if len(parts) > 3:
        raise Exception("Invalid channel name")

    base = parts[0]
    parts = parts[1:]
    rate = None
    chtype = None
    for part in parts:
        if is_float(part):
            if rate is None:
                rate = part
            else:
                raise Exception("Invalid channel name")
        else:
            if chtype is not None:
                raise Exception("Invalid channel name")
            chtype = part

    print("Looking for '{0}',{1} @ {2}".format(base, chtype, rate))

    if base not in name_lookup:
        raise Exception("Unknown channel name '{0}'".format(base))

    candidates = name_lookup[base]
    if rate is None and chtype is None:
        if len(candidates) == 1:
            return candidates[0]
        raise Exception("Ambigous channel requested")
    for idx in candidates:
        chan = full_channel_list[idx]
        print("Evaluating against params {0} @ {1}".format(
            chan.chtype, chan.rate))
        if rate is not None and rate != chan.rate:
            continue
        if chtype is not None and chtype != chan.chtype:
            continue
        return idx
    raise Exception("Unknown channel name")


@app.route('/count', methods=['POST', 'GET'])
def count():
    global name_lookup
    if request.method == 'GET':
        return "{0} channels".format(len(full_channel_list))

    cLen = int(request.headers['Content-Length'])
    cType = request.headers['Content-Type']

    if cLen > 100 * 1024 or cType != 'application/json':
        raise Exception("Invalid input")

    params = request.json
    print(str(params))
    pattern = params['pattern']
    pattern = pattern.replace(r'*', r'.*')
    pattern = pattern.replace(r'?', r'.')
    print("Compiling '{0}'".format(pattern))
    chan_pattern = re.compile(pattern)

    count = 0
    for name in name_lookup:
        if chan_pattern.match(name):
            count += len(name_lookup[name])
    print("Sending a count of %d" % count)
    return jsonify(count=count)


@app.route('/list', methods=['POST', 'GET'])
def list():
    global full_channel_list
    global name_lookup

    def compile_pattern(pattern):
        pattern = pattern.replace(r'*', r'.*')
        pattern = pattern.replace(r'?', r'.')
        print("Compiling '{0}'".format(pattern))
        return re.compile(pattern)

    def chan_to_json(chan):
        ch = {
            'name': chan.name,
            'rate': float(chan.rate),
            'channel_type': chan.chtype,
            'data_type': chan.dtype,
        }
        return json.dumps(ch)

    if request.method == 'GET':
        pattern = request.args.get('pattern', '*')
        channel_type = request.args.get('type', 'unknown')

        def list_gen(pattern, channel_type):
            global name_lookup
            global full_channel_list

            first = True
            yield "{\"channels\": [\n"
            if pattern.find('.') >= 0 or pattern.find('*') >= 0:
                channel_pattern = compile_pattern(pattern)
                for chan in full_channel_list:
                    if channel_pattern.match(chan.name):
                        if chan.chtype == channel_type or channel_type == "unknown":
                            data = chan_to_json(chan)
                            if not first:
                                data = ",\n" + data
                            first = False
                            yield data
            else:
                # single channel
                if pattern in name_lookup:
                    for idx in name_lookup[pattern]:
                        chan = full_channel_list[idx]
                        if chan.chtype == channel_type or channel_type == "unknown":
                            data = chan_to_json(chan)
                            if not first:
                                data = ",\n" + data
                            first = False
                            yield data
            yield "]\n}"
        return Response(list_gen(pattern, channel_type))

    cLen = int(request.headers['Content-Length'])
    cType = request.headers['Content-Type']

    if cLen > 100 * 1024 or cType != 'application/json':
        raise Exception("Invalid input")

    params = request.json
    print(str(params))
    chan_pattern = compile_pattern(params['pattern'])

    if params['channel_type'] == 'unknown':
        def _counter(name):
            global name_lookup
            return len(name_lookup[name])

        def _filter_idx(name):
            global name_lookup
            return name_lookup[name]
    else:
        def _counter(name):
            global name_lookup
            global full_channel_list
            cnt = 0
            for idx in name_lookup[name]:
                if full_channel_list[idx].chtype == params['channel_type']:
                    cnt += 1
            return cnt

        def _filter_idx(name):
            global name_lookup
            global full_channel_list
            results = []
            for idx in name_lookup[name]:
                if full_channel_list[idx].chtype == params['channel_type']:
                    results.append(idx)
            return results

    count = 0
    for name in name_lookup:
        if chan_pattern.match(name):
            count += _counter(name)

    def binary_list_gen():
        yield struct.pack("!I", count)
        for name in name_lookup:
            if chan_pattern.match(name):
                for index in _filter_idx(name):
                    chan = full_channel_list[index]
                    data = "%s %s %s %s\0" % (
                        chan.name, chan.chtype, str(chan.rate), chan.dtype)
                    buf = struct.pack("!I", len(data)) + data
                    yield buf
    return Response(binary_list_gen())


@app.route('/find', methods=['POST', 'GET'])
def find():
    global full_channel_list
    global name_lookup

    if request.method == 'GET':
        params = {
            "channels": [request.args.get('channel', '')]
        }
    else:
        cLen = int(request.headers['Content-Length'])
        cType = request.headers['Content-Type']

        if cLen > 100 * 1024 or cType != 'application/json':
            raise Exception("Invalid input")

        params = request.json

    avail = {}
    for name in params["channels"]:
        # idx = chan_name_to_idx(name)
        avail[name] = [{
            "frame_type": "a_frame",
            "start": 0,
            "stop": 1999999999,
        }]
    return json.dumps(avail)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Test Channel Information server")
    parser.add_argument("--frame", default="",
                        help="Frame to pull channel list from")
    args = parser.parse_args()
    if args.frame == "":
        setup_channels(10 * 1000)
    else:
        setup_channels_from_frame(args.frame)
    app.run(debug=True)

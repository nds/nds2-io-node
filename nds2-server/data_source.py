# Copyright 2020 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import os
import time
import logging
import collections

from . import daqd
from . import signal_units
from .concurrent import concurrent_factory
from .errors import DAQDException
from .errors import DAQDDataOnTape
from .errors import DAQDSyntaxError
from .frame_io import cache_key
from .frame_reader import frame_reader_factory
from .remote_proxy import remote_proxy_factory
from .helpers import get_frame_name_info
from .helpers import parse_channel_name
from .helpers import seconds_to_samples
from .helpers import samples_to_bytes
from .helpers import samples_to_seconds
from .helpers import bytes_to_samples
from .helpers import fix_frame_names
from .helpers import get_frame_intersection_info
from .planner import Planner
from .location_cache import LocationCache

DataPlanCacheEntry = collections.namedtuple(
    'DataPlanCacheEntry',
    ['time', 'start', 'stop', 'stride', 'channels', 'plan'],
)


class DataSource:
    """
    A channel info/data provider

    """

    def __init__(self, chan_info_obj, frame_lookup_obj, reader_socket, remote_proxy=None, high_latency_path="", concurrent=None):
        """Create a NDS2 based channel information provider.

        :param host: hostname to connect to
        :param port: port number to connect to
        """
        self.chan_info = chan_info_obj
        self.disk_cache = frame_lookup_obj
        self.frame_reader = frame_reader_factory(reader_socket)
        if remote_proxy is None:
            self.remote_proxy = remote_proxy_factory("")
        else:
            self.remote_proxy = remote_proxy
        self.location_cache = LocationCache()
        self.__data_plan_cache = None
        self.__high_latency_path = high_latency_path
        if concurrent is None:
            concurrent = concurrent_factory(None)
        self.concurrent = concurrent

    def _get_channel_info(self, channels, gps_start, gps_stop):
        """Retreive information about the given list of channels.

        NOTE this should not be used for online data, as it will
        ignore online channels.

        :param channels: A list of channel names as strings
        :param gps_start: Optional start time if not specified defaults to
        the current epoch
        :param gps_stop: Optional stop time, if not specified defaults to the
        current epoch
        :note: if either gps_start or gps_stop are None the default is used
        for both.
        """
        # if gps_start is None or gps_stop is None:
        #     gps_start = self.epoch_start
        #     gps_stop = self.epoch_stop
        results = [None for i in range(len(channels))]
        lookup_map = {}
        for i, chan in enumerate(channels):
            name, chtype, rate = parse_channel_name(chan)
            if chtype is None:
                chtype = 'unknown'
            if chtype == 'online':
                raise DAQDSyntaxError("Cannot process an online channel via an offline request")
            if chtype not in lookup_map:
                lookup_map[chtype] = []
            lookup_map[chtype].append({
                'index': i,
                'name': name,
                'rate': rate,
            })
        for chtype in lookup_map:
            chlist = [ch['name'] for ch in lookup_map[chtype]]
            data = self.chan_info.channel_list(chlist,
                                               gps_start,
                                               gps_stop,
                                               chtype)
            # spread the data out to a cache
            # mark duplicates
            dups = {}
            cache = {}
            for entry in data:
                if entry['channel_type'] == 'online':
                    continue
                ch_name = entry['channel_name']
                if ch_name not in cache:
                    cache[ch_name] = entry
                else:
                    choices = dups.get(ch_name, [])
                    if len(choices) == 0:
                        choices = [cache[ch_name]["full_name"]]
                    choices.append(entry['full_name'])
                    dups[ch_name] = choices
                    cache[ch_name] = None

            for ch_entry in lookup_map[chtype]:
                if ch_entry['name'] in cache:
                    if cache[ch_entry['name']] is not None:
                        results[ch_entry['index']] = cache[ch_entry['name']]
                    else:
                        ch_name = ch_entry['name']
                        raise DAQDException(daqd.DAQD_INVALID_CHANNEL_NAME,
                                            "Ambiguous channel '{0}' could be one of {1}".format(ch_name,
                                                                                                 dups[ch_name]))
                else:
                    raise DAQDException(daqd.DAQD_INVALID_CHANNEL_NAME,
                                        "This channel was not found on the server '{0}'".format(ch_entry['name']))
        return results

    def _get_channel_avail_info(self, channels, epoch_start, epoch_stop):
        # start_time = self.epoch_start
        # if start is not None:
        #     start_time = start
        # stop_time = self.epoch_stop
        # if stop is not None:
        #     stop_time = stop
        return self.chan_info.channel_avail_info(
            channels,
            0,
            epoch_start,
            epoch_stop,
        )

    ######### channel info

    def channel_count_passthrough(self, gps, channel_type, pattern, epoch_start, epoch_stop, dest):
        self.chan_info.channel_count_passthrough(
            gps,
            channel_type,
            pattern,
            epoch_start,
            epoch_stop,
            dest,
        )

    def channel_list_passthrough(self, gps, channel_type, pattern, epoch_start, epoch_stop, dest):
        self.chan_info.channel_list_passthrough(
            gps,
            channel_type,
            pattern,
            epoch_start,
            epoch_stop,
            dest,
        )

    def channel_avail_info_passthrough(self, channels, gps, epoch_start, epoch_stop, dest):
        self.chan_info.channel_avail_info_passthrough(
            channels,
            gps,
            epoch_start,
            epoch_stop,
            dest,
        )

    def channel_source_info_passthrough(self, channels, gps, epoch_start, epoch_stop, dest):
        self.chan_info.channel_source_info_passthrough(
            channels,
            gps,
            epoch_start,
            epoch_stop,
            dest,
        )

    def channel_crc(self):
        return self.chan_info.channel_crc()

    ########## data retrieval

    def create_data_plan(self, start, stop, stride, channels, get_time=time.time):
        """Create a get-data/check-data data plan

        May throw DAQD exception if there is a gap or a time alignment problem

        :param start: gps start (inclusive)
        :param stop:  gps stop (exclusive)
        :param channels: list of channel names
        :return: a tuple of (channel_info, fetch_plan)
        """
        cache = self.__data_plan_cache
        if cache is not None:
            delta = get_time() - cache.time
            if delta < 60.0 and cache.start == start and cache.stop == stop and cache.stride == stride and \
                    cache.channels == channels:
                return cache.plan
        chan_info = self._get_channel_info(channels, start, stop)
        channels_full_name = [x["full_name"] for x in chan_info]

        has_mtrend = False
        for chinfo in chan_info:
            if chinfo["channel_type"] == "m-trend":
                has_mtrend = True
        if has_mtrend:
            if start % 60 != 0 or stop % 60 != 0 or stride % 60 != 0:
                raise DAQDException(daqd.DAQD_NOT_FOUND,
                                    "Unaligned m-trend access, when requesting minute trends, please ensure that all "
                                    "gps times are divisible by 60")

        # we should also get availability information
        chan_avail = self._get_channel_avail_info(chan_info, epoch_start=start, epoch_stop=stop)
        planner = Planner()
        cache = DataPlanCacheEntry(time=get_time(),
                                   start=start,
                                   stop=stop,
                                   stride=stride,
                                   channels=channels,
                                   plan=(chan_info, planner.plan(channels_full_name, chan_avail, start, stop)))
        self.__data_plan_cache = cache
        return cache.plan

    def get_frame_list(self, frame_type, gps_start, gps_stop):
        """Get a list of frame files for the given frame type over [gps_start, gps_stop)
        :return: a list of frame paths
        """
        frames = []
        cur = gps_start
        while cur < gps_stop:
            fname = self.location_cache.get(frame_type, cur)
            if fname is None:
                filenames = self.disk_cache.query(frame_type, cur, gps_stop)
                if len(filenames) == 0:
                    break
                for filename in filenames:
                    cur_type, cur_start, cur_stop = get_frame_name_info(filename)
                    if cur_type is None:
                        continue
                    self.location_cache.add(cur_type, cur_start, cur_stop, filename)
                    frames.append(filename)
                break
            else:
                cur_type, cur_start, cur_stop = get_frame_name_info(fname)
                frames.append(fname)
                cur = cur_stop
        return frames

    def _check_for_latency(self, fname, stat_func=os.stat):
        """Given a filename, check to see if it is high-latency.  May short circuit if it is on
        a known 'fast' path."""
        if fname.startswith(self.__high_latency_path):
            finfo = stat_func(fname)
            if finfo.st_blocks == 0:
                raise DAQDDataOnTape()

    def _is_frame_high_latency(self, frame_info, stat_func=os.stat):
        try:
            self._check_for_latency(frame_info['filename'], stat_func=stat_func)
            return False
        except DAQDDataOnTape:
            return True

    def _check_frame_existence(self, frame_type, gps_start, gps_stop):
        """Verify that the frames of a given type can be accessed over [gps_start, gps_stop)

        exception on missing/on tape frames

        :param frame_type: Frame type to check
        :param gps_start: Start time inclusive
        :param gps_stop: Stop time exclusive
        :return: Nothing on success, will raise an error as an
        """
        fname = ""
        try:
            cur = gps_start
            while cur < gps_stop:
                fname = self.location_cache.get(frame_type, cur)
                if fname is None:
                    filenames = self.disk_cache.query(
                        frame_type, cur, gps_stop)
                    if len(filenames) == 0:
                        msg = "could not find a frame for {0}:{1}-{2}".format(
                            frame_type, cur, gps_stop)
                        raise DAQDException(daqd.DAQD_NOT_FOUND, msg)
                    for filename in filenames:
                        cur_type, cur_start, cur_stop = get_frame_name_info(
                            filename)
                        if cur_type is None:
                            continue
                        self.location_cache.add(
                            cur_type, cur_start, cur_stop, filename)
                    continue
                cur_type, cur_start, cur_stop = get_frame_name_info(fname)
                self._check_for_latency(fname)
                cur = cur_stop
        except OSError:
            msg = "Could not check frame file {0}.".format(fname)
            raise DAQDException(daqd.DAQD_NOT_FOUND, msg)

    def check_data(self, fetch_plan):
        for plan_segment in fetch_plan.segments:
            gps_start = plan_segment.gps_start
            gps_stop = plan_segment.gps_stop
            for plan_bundle in plan_segment.bundles:
                frame_type = plan_bundle.frame_type
                if frame_type.startswith("("):
                    continue
                self._check_frame_existence(frame_type, gps_start, gps_stop)

    def _find_frame_containing(self, chan, fetch_plan, gps_start, gps_stop):
        frame_type = fetch_plan[0].get_frame_type(chan['full_name'])
        if frame_type == "":
            raise DAQDException(daqd.DAQD_NOT_FOUND)
        cached_fname = self.location_cache.get(frame_type, gps_start)
        if cached_fname is None:
            frames = fix_frame_names(self.disk_cache.query(
                frame_type, gps_start, gps_stop))
        else:
            frames = [cached_fname, ]
        results = []
        for fname in frames:

            entry = get_frame_intersection_info(fname, gps_start, gps_stop)
            if entry is None:
                continue

            if cached_fname is None:
                self.location_cache.add(
                    entry['frame_type'], entry['frame_start'],
                    entry['frame_stop'], fname)
            results.append(entry)
        return results

    def _read_frame_with_limits(self, target_frame: str, chan, key, cache_start, max_block_size, mem_cache):
        """Read data from a frame, while staying within any prescribed concurrency limits"""
        def do_read():
            return self.frame_reader.read_from_frame(target_frame, chan, key, cache_start, max_block_size, mem_cache)

        if self._is_frame_high_latency(target_frame):
            self.concurrent.get_data_waiting()
            with self.concurrent.get_data_semaphore:
                self.concurrent.get_data_running()
                try:
                    return do_read()
                finally:
                    self.concurrent.get_data_done()
        else:
            return do_read()

    def _fetch_frame_data(self, chan, target_frame, start, stop, mem_cache, max_block_size):
        logger = logging.getLogger(__name__ + "frameio")
        max_requested_samples = seconds_to_samples(chan, stop - start)
        gps_offset = start - target_frame['frame_start']
        # the cache_ values are relative to the start of the frame
        if gps_offset % max_block_size == 0:
            cache_start = gps_offset
            cache_offset_samples = 0
        else:
            cache_start = int(gps_offset / max_block_size) * max_block_size
            cache_offset_samples = seconds_to_samples(
                chan, gps_offset % max_block_size)
        cache_offset_bytes = samples_to_bytes(chan, cache_offset_samples)
        cache_end = cache_start + max_block_size
        target_delta = target_frame['gps_stop'] - target_frame['gps_start']
        if cache_end > target_delta:
            cache_end = target_frame[
                            'gps_stop'] - target_frame['gps_start']

        key = cache_key(target_frame['frame_type'], target_frame[
            'frame_start'], chan, cache_start)

        data = mem_cache.get(key)
        if data is None:
            logger.debug("cache miss @ {0}".format(key))
            data = self._read_frame_with_limits(target_frame, chan, key, cache_start, max_block_size, mem_cache)
            if data is None:
                raise DAQDException(daqd.DAQD_ERROR, "Unable to read data")
        else:
            logger.debug("cache hit @ {0}".format(key))
        units, data = signal_units.extract_signal_units(data)
        if units is None:
            units = signal_units.default()
        total_cache_samples = bytes_to_samples(chan, len(data))
        remaining_cache_samples = \
            total_cache_samples - cache_offset_samples
        samples_to_write = min(
            remaining_cache_samples, max_requested_samples)

        stop_byte = cache_offset_bytes + samples_to_bytes(chan, samples_to_write)

        data = data[cache_offset_bytes:stop_byte]

        return data, samples_to_write, units

    def fetch_frame_data(self, chan, fname, start, stop, mem_cache, max_block_size):
        target_frame = get_frame_intersection_info(fname, start, stop)
        return self._fetch_frame_data(chan, target_frame, start, stop, mem_cache, max_block_size)

    def fetch_data(self, chan, fetch_plan, start, stop, mem_cache, max_block_size):
        if fetch_plan[0].bundles[0].frame_type.startswith("("):
            return self.remote_proxy.remote_data(fetch_plan[0].bundles[0].frame_type, chan, start, stop)
        target_frames = self._find_frame_containing(chan, fetch_plan, start, stop)
        target_frame = target_frames[0]
        return self._fetch_frame_data(chan, target_frame, start, stop, mem_cache, max_block_size)

    def clear_cache(self):
        self.frame_reader.clear_cache()

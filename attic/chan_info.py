#!/usr/bin/env python

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from __future__ import print_function

import json
import requests

import daqd


class HTTPChanInfo:
    def __init__(self, server_url):
        self.server_url = server_url

    def connect(self):
        pass

    def close(self):
        pass

    def channel_avail_info(self, channels, gps, epoch_start, epoch_stop):
        params = {
            "channels": channels,
            "epoch": [epoch_start, epoch_stop],
            "gps": gps,
        }
        payload = json.dumps(params)
        r = requests.post(self.server_url + "/find",
                          headers={'content-type': 'application/json'},
                          data=payload)
        if not r.status_code == requests.codes.ok:
            raise Exception("unable to retrieve channel info")
        return r.json()

    def channel_list(self, pattern, epoch_start, epoch_stop, channel_type="unknown"):
        """
        Return channel information in a structured manner
        :param pattern: Channel pattern to match
        :param epoch_start: global time window start
        :param epoch_stop: global time window stop
        :param channel_type: Type of the channel
        :return: A list of structures
        """
        params = {
            "pattern": pattern,
            "type": channel_type,
        }
        r = requests.get(self.server_url + "/list", params=params)
        if not r.status_code == requests.codes.ok:
            raise Exception("Unable to find channel {0}".format(pattern))
        data = r.json()
        return data["channels"]

    def channel_list_passthrough(self, gps, channel_type, pattern,
                                 epoch_start, epoch_stop, dest):
        payload = json.dumps({
            "epoch": [epoch_start, epoch_stop],
            "gps": gps,
            "channel_type": channel_type,
            "pattern": pattern,
        })
        r = requests.post(self.server_url + "/list",
                          headers={'content-type': 'application/json'},
                          data=payload)
        if r.status_code == requests.codes.ok:
            dest.write(daqd.DAQD_OK)
            for chunk in r.iter_content(4 * 1024):
                dest.write(chunk)
        else:
            dest.write(daqd.DAQD_ERROR)
            return

    def channel_count_passthrough(self, gps, channel_type, pattern,
                                  epoch_start, epoch_stop, dest):
        payload = json.dumps({
            "epoch": [epoch_start, epoch_stop],
            "gps": gps,
            "channel_type": channel_type,
            "pattern": pattern,
        })
        r = requests.post(self.server_url + "/count",
                          headers={'content-type': 'application/json'},
                          data=payload)
        if r.status_code == requests.codes.ok:
            dest.write(daqd.DAQD_OK)
            resp = r.json()
            print(str(resp))
            dest.write_int32(resp["count"])
        else:
            dest.write(daqd.DAQD_ERROR)
            return

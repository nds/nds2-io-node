# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE


class DummyCacheBase(object):
    def __init__(self, max_block_size=1024 * 1024):
        self.__max_block_size = max_block_size

    def max_block_size(self):
        return self.__max_block_size


class DummyCache(DummyCacheBase):
    def __init__(self, max_block_size=1024 * 1024):
        super(DummyCache, self).__init__(max_block_size=max_block_size)
        self.__cache = {}

    def get_uri(self):
        """Get uri returns a pseudo random string that represents this object.
        It is provided so that a specific memory cache handler can be provided
        for testing."""
        return str(hash(self))

    def get(self, key):
        if key in self.__cache:
            return self.__cache[key]
        return None

    def set(self, key, value):
        self.__cache[key] = value


class DummyCacheTracking(DummyCacheBase):
    def __init__(self, max_block_size=1024 * 1024):
        super(DummyCacheTracking, self).__init__(max_block_size=max_block_size)
        self.__cache = {}
        self.gets = []
        self.sets = []

    def get_uri(self):
        """Get uri returns a pseudo random string that represents this object.
        It is provided so that a specific memory cache handler can be provided
        for testing."""
        return str(hash(self))

    def get(self, key):
        self.gets.append(key)
        if key in self.__cache:
            return self.__cache[key]
        return None

    def set(self, key, value):
        self.sets.append(key)
        self.__cache[key] = value


class DummyCacheLikeaSeive(DummyCacheBase):
    """A dummy mem cache that always looses what you give it.
    This cache can be configured with an optional data validation check."""

    def __init__(self, max_block_size=1024 * 1024, data_validator=None):
        super(DummyCacheLikeaSeive, self).__init__(max_block_size=max_block_size)
        self.__cache = {}
        self.__validator = data_validator

    def get_uri(self):
        """Get uri returns a pseudo random string that represents this object.
        It is provided so that a specific memory cache handler can be provided
        for testing."""
        return str(hash(self))

    def get(self, key):
        return None

    def set(self, key, value):
        if self.__validator is not None:
            self.__validator(key, value)

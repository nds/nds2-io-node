# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from .helpers import range_contains


class CacheEntry():
    def __init__(self, gps_start, gps_stop, filename):
        self.gps_start = gps_start
        self.gps_stop = gps_stop
        self.filename = filename

    def __hash__(self):
        return hash("{0}:{1}".format(self.gps_start, self.gps_stop))


class LocationCache():
    """
    A cache for frame location lookups
    """

    def __init__(self):
        self.__cache = {}

    def get(self, frame_type, gps_start):
        """
        Return a frame filename from the cache if possible
        :param frame_type: The frame type
        :param gps_start: Time that must be in the cached frame file
        :return: None if no match found, else the filename
        """
        if frame_type in self.__cache:
            for entry in self.__cache[frame_type]:
                if range_contains(entry.gps_start, entry.gps_stop, gps_start):
                    return entry.filename
        return None

    def add(self, frame_type, frame_start, frame_stop, filename):
        """
        Add an entry to the cache
        :param frame_type: The frame type
        :param frame_start: The start of the frame (inclusive)
        :param frame_stop: The stop time of the frame (exclusive)
        :param filename: The filename/url to the frame
        :return: nothing
        """
        if frame_type not in self.__cache:
            self.__cache[frame_type] = set()
        entry = CacheEntry(frame_start, frame_stop, filename)
        self.__cache[frame_type].add(entry)

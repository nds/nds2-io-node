# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import json
import socket
import struct
import urllib.request


class dc_socket(socket.socket):
    def __init__(self, address_family, sock_type):
        super(dc_socket, self).__init__(address_family, sock_type)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False


class Nds3FrameServer():
    def __init__(self, hostname, port):
        self.__hostname = hostname
        self.__port = port

    def query(self, frame_type, gps_start, gps_stop):
        url = "http://{0}:{1}/frames/paths/{2}/{3}/{4}/".format(self.__hostname, self.__port, frame_type, gps_start, gps_stop)
        with urllib.request.urlopen(url) as resp:
            if resp.getcode() != 200:
                return []
            if resp.info().get_content_type() != "application/json":
                return []
            paths = []
            for line in resp.read().decode('utf8').split('\n'):
                line = line.strip()
                if line == "":
                    continue
                path = json.loads(line)
                paths.append(path)
            return paths


class DiskCacheClient():
    def __init__(self, hostname, port):
        self.__hostname = hostname
        self.__port = port

    def __read(self, s, count):
        results = b""
        while count > 0:
            buf = s.recv(count)
            if len(buf) == 0:
                raise Exception("Unexpected end of socket data")
            count -= len(buf)
            results += buf
        return results

    def query(self, frame_type, gps_start, gps_stop):
        query_str = "filenames --extension .gwf " + \
                    "--ifo-type-list {0} --start-time {1} --end-time {2}"
        query_str = query_str.format(frame_type, gps_start, gps_stop)
        cmd = "{0}{1}".format(len(query_str), query_str)

        with dc_socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.__hostname, self.__port))
            s.send(cmd.encode("utf-8"))
            buf = self.__read(s, 1)
            have_data = struct.unpack("<?", buf)[0]
            if have_data:
                data_len = struct.unpack("<I", self.__read(s, 4))[0]
                data = ""
                while data_len > 0:
                    buf = s.recv(data_len).decode('utf-8')
                    if len(buf) == 0:
                        raise Exception("Lost connection to disk cache server")
                    data += buf
                    data_len -= len(buf)
                return data.split()


_aux_frame_lookup = {}


def register_frame_lookup(key, frame_lookup):
    """
    Register a frame lookup object on a specific uri.
    Typically used for debugging.
    :param key: uri to key on
    :param frame_lookup: object to return
    """
    global _aux_frame_lookup
    _aux_frame_lookup[key] = frame_lookup


def frame_lookup_factory(uri):
    """
    Given a uri return a frame lookup server
    :param uri: The uri
    :return: A frame lookup server
    """
    if uri.startswith("dcache://"):
        name = uri.split("://")[1]
        port = 20222
        if name.find(":") >= 0:
            name, port = name.split(":", 1)
            port = int(port)
        return DiskCacheClient(name, port)
    elif uri.startswith("nds3://"):
        name = uri.split("://")[1]
        port = 11301
        if name.find(":") >= 0:
            name, port = name.split(":", 1)
            port = int(port)
        return Nds3FrameServer(name, port)
    if uri in _aux_frame_lookup:
        return _aux_frame_lookup[uri]
    return DiskCacheClient('localhost', 20222)

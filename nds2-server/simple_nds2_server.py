# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import socketserver
import argparse
import binascii
import datetime
import fnmatch
import importlib
import logging
import logging.handlers
import struct
import time

from . import daqd

from .data_buffer import DataBuffer
from .errors import DAQDException
from .errors import DAQDFrameIOError
from .errors import DAQDGenericError
from .errors import DAQDSyntaxError
from .helpers import intersects
from .helpers import seconds_to_samples
from .helpers import parse_get_data_line
from .helpers import parse_get_online_data_line
from .simple_chaninfo import class_types
from .simple_chaninfo import data_types
from .stats import stats_factory


class simple_chan_list(object):
    def __init__(self):
        self.__chans = {}
        self.__keys = []

    def add_channel(self, simple_chan):
        name = simple_chan.name()
        block = self.__chans.get(name, [])
        index = simple_chan_list.__find_individual_chan(block, simple_chan)
        if index is not None:
            raise IndexError("Duplicate channel found")
        block.append(simple_chan)
        self.__chans[name] = block

    def finalize(self):
        for k in self.__chans:
            block = self.__chans[k]
            block.sort(key=simple_chan_list.sort_function)
            self.__chans[k] = block
        self.__keys = list(self.__chans.keys())
        self.__keys.sort()

    def visit_rough_cut(self, gps_start, gps_stop, f):
        for k in self.__keys:
            for entry in self.__chans[k]:
                avail = entry.rough_availability()
                match = False
                for avail_entry in avail:
                    if intersects(gps_start, gps_stop, avail_entry[0], avail_entry[1]):
                        match = True
                        break
                if match:
                    f(entry)

    def match_channel(self, input):
        def to_float(val):
            try:
                return float(val)
            except ValueError:
                return None

        parts = input.replace("%", ",").split(",")
        base_name = parts[0]
        if base_name not in self.__chans:
            return None
        rate = 0.0
        class_type = None
        for attribute in parts[1:]:
            val = to_float(attribute)
            if val is not None:
                rate = val
            else:
                class_type = class_types.to_class_type(attribute)
        best_match = self.__chans[base_name][0]
        best_score = 0

        if rate == 0.0:
            rate_score = lambda x: True
        else:
            rate_score = lambda chan: chan.sample_rate() == rate

        if class_type is None:
            class_score = lambda x: True
        else:
            class_score = lambda chan: chan.class_type() == class_type

        if rate == 0.0 and class_type is None:
            return best_match
        for chan in self.__chans[base_name]:
            score = 0
            if rate_score(chan):
                score += 1
            else:
                continue
            if class_score(chan):
                score += 1
            else:
                continue
            if score > best_score:
                best_score = score
                best_match = chan
        if best_score > 0:
            return best_match
        return None

    @staticmethod
    def __find_individual_chan(block, simple_chan):
        i = 0
        for entry in block:
            if entry.data_type() == simple_chan.data_type() and entry.class_type() == simple_chan.class_type() and entry.sample_rate() == simple_chan.sample_rate():
                return i
            i += 1
        return None

    @staticmethod
    def sort_function(simple_chan):
        return "{0} {1:0>10} {2} {3}".format(simple_chan.name(),
                                             simple_chan.sample_rate(),
                                             simple_chan.data_type(),
                                             simple_chan.class_type())


# class ForkingServer(socketserver.ForkingMixIn, socketserver.TCPServer):
class ForkingServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    stats_server_uri = ""
    request_queue_size = 1024


class NDS2ServerLogic:
    """
    The main NDS2 server logic.
    Split out from the handler to allow better testing.
    """

    INFINITE = 1999999999

    def __init__(self, request, stats_obj, chan_list):
        """
        Create a NDS2 server object
        :param request: The socket like object to send/receive data with
        :param stats_obj: A statistics object to report usage with
        :param chan_list: A simple_chan_list object
        """
        self.__buffer = ""
        self.epoch_start = 0
        self.epoch_stop = self.INFINITE
        self.writer_id = 1
        self.epochs = {
            "ALL": (0, 1999999999),
            "ER2": (1025636416, 1028563232),
            "ER3": (1042934416, 1045353616),
            "ER4": (1057881616, 1061856016),
            "ER5": (1073606416, 1078790416),
            "ER6": (1102089216, 1102863616),
            "ER7": (1116700672, 1116800000),
            "ER8": (1123856384, 1123900032),
            "ER9": (1151848448, 1152169216),
            "ER10": (1163173888, 1164554240),
            "O1": (1126621184, 1137254400),
            "O2": (1164554240, 1188081664),
            "NONE": (0, 0),
            "S5": (815153408, 880920032),
            "S6": (930960015, 971654415),
            "S6a": (930960015, 935798415),
            "S6b": (937785615, 947203215),
            "S6c": (947635215, 961545615),
            "S6d": (961545615, 971654415),
        }
        self.stats = stats_obj
        self.request = request
        self.chan_list = chan_list
        self.__io_logger = logging.getLogger(__name__ + '.clientio')
        self.bytes_written = 0
        self.__error_msg = ""
        self.__channel_cap = 500
        self.__use_units = False
        self.__authorized = True

    def _gpstime(self):
        return int(time.time() - 315964800 + 38 - 19)

    def __read(self):
        """
        Read from the connection into the internal buffer.
        """
        buf = self.request.recv(1024)
        if len(buf) == 0:
            raise Exception()
        self.__io_logger.debug("read '{0}'".format(buf))
        self.__buffer += buf
        self.__io_logger.debug("buffer = '{0}'".format(self.__buffer))

    def read_line(self):
        """Return the next text line (delimited by \n) from the buffer.
        Reads from the connection if more data is needed."""
        idx = self.__buffer.find('\n')
        while idx < 0:
            self.__read()
            idx = self.__buffer.find('\n')
        line = self.__buffer[:idx]
        self.__buffer = self.__buffer[idx + 1:]
        return line

    def write(self, data):
        """
        Write the given data to the connection
        :param data: Data to write
        """
        self.request.sendall(data)
        self.bytes_written += len(data)

    def SYNTAX_ERROR(self):
        """Raise a syntax error exception"""
        raise DAQDSyntaxError()

    def _get_channel_avail_info(self, channels, gps=0, start=None, stop=None):
        # FIXME:
        start_time = self.epoch_start
        if start is not None:
            start_time = start
        stop_time = self.epoch_stop
        if stop is not None:
            stop_time = stop
        return self.chan_info.channel_avail_info(channels,
                                                 gps,
                                                 start_time,
                                                 stop_time)

    def _select_channels_from_list(self, names):
        """
        Given a list of channel names (with optional rate, type attributes)
        return a list of channels that match it
        :param names: Channel names
        :return: List of channels from chan_list
        :notes: raises an exception if a channel cannot be matched
        """
        selections = []
        for entry_name in names:
            chan = self.chan_list.match_channel(entry_name)
            if chan is None:
                raise RuntimeError("Invalid channel name {0}".format(entry_name))
            selections.append(chan)
        return selections

    def _data_available(self, start, stop, simple_channels):
        for entry in simple_channels:
            avail = entry.detailed_availability(start, stop)
            if len(avail) != 1:
                self.set_error_message("A gap was found in {0}".format(entry.name()))
                return False
            if avail[0][0] != start or avail[0][1] != stop:
                self.set_error_message("A gap was found in {0}".format(entry.name()))
                return False
        return True

    def write_int32(self, value):
        """
        Write a 32bit integer to the connection in network byte order
        :param value: Integer value to write
        :return:
        """
        data = struct.pack("!I", value)
        self.write(data)

    def handle_authorize(self, line):
        """Handle the authorize command
        :param line: the remainder of the command line, should be empty"""
        self.__authorized = True
        self.write(daqd.DAQD_OK)

    def handle_proto_ver(self, line):
        """
        Handle the protocol version command
        :param line:
        :return:
        """
        self.write(daqd.DAQD_OK)
        self.write_int32(1)

    def handle_proto_rev_default(self, line):
        """
        Handle the protocol revision command
        :param line:
        :return:
        """
        self.write(daqd.DAQD_OK)
        self.write_int32(5)

    def handle_proto_rev_specific_version_requested(self, line):
        """
        Handle the protocol revision command where a specific version
        has been requested.
        :param line:
        :return:
        """
        if line == "6;":
            self.write(daqd.DAQD_OK)
            self.write_int32(6)
            self.__use_units = True
        else:
            self.SYNTAX_ERROR()

    def _parse_channel_filter(self, line):
        """
        Parse a nds2 channel filter
        :param line: a nds2 channel filter
        :return: a dictionary {
            "gps": gps time int value
            "channel_type": channel_type (raw|unknown|m-trend|s-trend)
            "pattern":  a pattern to match
        }
        """
        if line[-1] != ';':
            self.SYNTAX_ERROR()
        line = line[:-1]
        match = "*"
        idx = line.find('{')
        if idx > 0:
            if line[-1] != '}':
                self.SYNTAX_ERROR()
            match = line[idx + 1:-1]
            line = line[:idx]

        parts = line.split()
        if len(parts) not in [1, 2]:
            self.SYNTAX_ERROR()

        gps = int(parts[0])
        chantype = 'unknown'
        if len(parts) > 1:
            chantype = parts[1]

        if chantype not in ['raw', 'reduced', 'unknown', 'm-trend', 's-trend', 'online', 'test-pt', 'static']:
            self.SYNTAX_ERROR()

        params = {
            "epoch": [self.epoch_start, self.epoch_stop],
            "gps": gps,
            "channel_type": chantype,
            "pattern": match,
        }
        return params

    def handle_count_channels(self, line):
        """
        Handle the count channels command
        :param line:
        :return:
        """
        class Counter(object):
            def __init__(self, pattern, chan_class):
                self.__pattern = pattern
                self.__count = 0
                self.__target_type = class_types.to_class_type(chan_class)
                if chan_class == class_types.name(class_types.UNKNOWN):
                    self.__type_filter = lambda x: True
                else:
                    self.__type_filter = lambda chan: chan.class_type() == self.__target_type

            def __call__(self, chan):
                if fnmatch.fnmatch(chan.name(), self.__pattern) and self.__type_filter(chan):
                    self.__count += 1

            def count(self):
                return self.__count

        # FIXME:
        payload = self._parse_channel_filter(line)
        self.write(daqd.DAQD_OK)
        c = Counter(payload["pattern"], payload['channel_type'])
        self.chan_list.visit_rough_cut(self.epoch_start, self.epoch_stop, c)
        self.write_int32(c.count())

        #self.chan_info.channel_count_passthrough(payload['gps'],
        #                                         payload['channel_type'],
        #                                         payload['pattern'],
        #                                         self.epoch_start,
        #                                         self.epoch_stop,
        #                                         self)

    def handle_get_channel_crc(self, line):
        """
        Handle the get-channel-crc command
        :param line:
        :return:
        """
        # FIXME:
        try:
            chan_crc = self.chan_info.channel_crc()
        except:
            raise DAQDGenericError("Invalid CRC")
        self.write(daqd.DAQD_OK)
        self.write(chan_crc)

    def handle_get_channels(self, line):
        """
        Handle the get channels command
        :param line:
        :return:
        """
        class Selecter(object):
            def __init__(self, pattern, chan_class):
                self.__pattern = pattern
                self.__count = 0
                self.__channels = []
                self.__target_type = class_types.to_class_type(chan_class)
                if chan_class == class_types.name(class_types.UNKNOWN):
                    self.__type_filter = lambda x: True
                else:
                    self.__type_filter = lambda chan: chan.class_type() == self.__target_type

            def __call__(self, chan):
                if fnmatch.fnmatch(chan.name(), self.__pattern) and self.__type_filter(chan):
                    self.__count += 1
                    self.__channels.append(chan)

            def count(self):
                return self.__count

            def channels(self):
                return self.__channels

        def chan_to_string(chan):
            s = "{0} {1} {2} {3}\0".format(chan.name(),
                               class_types.name(chan.class_type()),
                               str(chan.sample_rate()),
                               data_types.name(chan.data_type()))
            s = s.encode("utf-8")
            return struct.pack("!I", len(s)) + s

        # FIXME:
        payload = self._parse_channel_filter(line)
        self.write(daqd.DAQD_OK)
        s = Selecter(payload["pattern"], payload["channel_type"])
        self.chan_list.visit_rough_cut(self.epoch_start, self.epoch_stop, s)
        self.write_int32(s.count())
        for chan in s.channels():
            self.write(chan_to_string(chan))
        #self.chan_info.channel_list_passthrough(payload['gps'],
        #                                        payload['channel_type'],
        #                                        payload['pattern'],
        #                                        self.epoch_start,
        #                                        self.epoch_stop,
        #                                        self)

    def handle_get_source_data(self, line):
        """
        Handle the get source data command
        :param line:
        :return:
        """
        idx = line.find('{')
        if idx < 0 or line[-1] != ';' or line[-2] != '}':
            self.SYNTAX_ERROR()
        parts = line[:idx - 1].strip().split()
        channels = line[idx + 1:-2].strip().split()

        self.stats.add_channel_list(channels)

        selections = self._select_channels_from_list(channels)
        self.write(daqd.DAQD_OK)
        out = []
        for entry in selections:
            avail_list = entry.detailed_availability(self.epoch_start, self.epoch_stop)
            avail_out = []
            for avail in avail_list:
                avail_out.append("sim_data:{0}-{1}".format(avail[0], avail[1]))
            out.append("{0} {1}{2}{3}".format(entry.name(), "{", " ".join(avail_out), "}"))
        out = " ".join(out)
        out = struct.pack("!i", len(out)) + out
        self.write(out)
        # self.chan_info.channel_avail_info_passthrough(channels,
        #                                               int(parts[0]),
        #                                               self.epoch_start,
        #                                               self.epoch_stop,
        #                                               self)

    def handle_get_source_list(self, line):
        """
        Handle the get source data command
        :param line:
        :return:
        """
        # FIXME:
        idx = line.find('{')
        if idx < 0 or line[-1] != ';' or line[-2] != '}':
            self.SYNTAX_ERROR()
        parts = line[:idx - 1].strip().split()
        channels = line[idx + 1:-2].strip().split()

        self.stats.add_channel_list(channels)

        self.chan_info.channel_source_info_passthrough(channels,
                                                       int(parts[0]),
                                                       self.epoch_start,
                                                       self.epoch_stop,
                                                       self)

    def write_channel_info_block(self, simple_channels, stride):
        dtype_to_val = {
            'int_2': 1,
            'int_4': 2,
            'int_8': 3,
            'real_4': 4,
            'real_8': 5,
            'complex_8': 6,
            'uint_4': 7,
        }
        ctype_to_val = {
            'unknown': 0,
            'online': 1,
            'raw': 2,
            'reduced': 3,
            's-trend': 4,
            'm-trend': 5,
            'test-pt': 6,
            'static': 7,
        }
        # reconfigure block
        #  has dummy values for gps, gps nano, and seq
        cur_offset = 0
        out = struct.pack("!LIII", 0xffffffff, 1, 2, 3)
        for chan in simple_channels:
            ch_span_size = int(seconds_to_samples(chan, stride) *
                               data_types.size(chan.data_type()))
            out += struct.pack("!IIHHfff",
                               ch_span_size,
                               cur_offset,
                               ctype_to_val[class_types.name(chan.class_type())],
                               dtype_to_val[data_types.name(chan.data_type())],
                               chan.sample_rate(),
                               chan.info().offset,
                               chan.info().slope)
            if self.__use_units:
                unit_str = chan.info().units
                out += struct.pack("!I", len(unit_str)) + unit_str
            cur_offset += ch_span_size
        out = struct.pack("!I", len(out)) + out
        self.write(out)

    def write_channel_data_block(self, simple_channels, start, stride, prev_stride, seq_num):
        size = 0
        for chan in simple_channels:
            size += int(seconds_to_samples(chan, stride) *
                        data_types.size(chan.data_type()))

        # write the block header
        # the 4 byte length at the start is the data size + 16 bytes of block
        # header
        header = struct.pack("!IIIII", size + 16, stride, start, 0, seq_num)

        try:
            with DataBuffer(size) as seg_buf:
                for chan in simple_channels:
                    chan_buffer = chan.get_data(start, start + stride)
                    chan_buffer.data.byteswap(True)
                    data = chan_buffer.data
                    data = data.tobytes()
                    seg_buf.write(data)
                if prev_stride != stride:
                    self.write_channel_info_block(simple_channels, stride)
                self.write(header)
                seg_buf.flush_to(self)
        except DAQDFrameIOError:
            # should this be a general catch everything???
            header = struct.pack("!IIIII", 16, stride, start, 0, seq_num)
            self.write(header)
            raise

    def handle_get_data(self, line):
        """
        :param line: start stop [stride] {chan list}
        :return:
        """
        start, stop, stride, channels = parse_get_data_line(line)
        if len(channels) > self.__channel_cap:
            raise DAQDSyntaxError(
                "Please limit requests to {0} or less channels at a time.  This limit is to keep timeout errors down, "
                "we are working on raising it.".format(
                    self.__channel_cap))

        if start == 0:
            self.handle_get_online_data("{0} {1} {{ {2} }};".format(stop, stride, " ".join(channels)))
            return
        self.do_get_data(start, stop, stride, channels, online=False)

    def handle_get_online_data(self, line):
        """
       :param line: [stop] stride {chan list}
       :return:
       """
        start, stop, stride, channels = parse_get_online_data_line(line)
        self.stats.add_channel_list(channels)
        duration = stop
        self.do_get_data(start, stop, stride, channels, online=True)

    def do_get_data(self, start, stop, stride, channels, online):

        self.stats.add_channel_list(channels)

        selections = self._select_channels_from_list(channels)
        if not online:
            if not self._data_available(start, stop, selections):
                self.write(daqd.DAQD_NOT_FOUND)
                return

        if not online:
            has_mtrend = False
            for entry in selections:
                if entry.class_type() == class_types.MTREND:
                    has_mtrend = True
                elif entry.class_type() == class_types.ONLINE or entry.class_type() == class_types.TESTPT:
                    self.write(daqd.DAQD_NOT_FOUND)
                    self.set_error_message("Requesting historical data from an online channel")
                    return
                elif entry.class_type() == class_types.UNKNOWN or entry.data_type() == data_types.UNKNOWN:
                    self.write(daqd.DAQD_NOT_FOUND)
                    self.set_error_message("Requesting data from a channel with unknown attributes")
            if has_mtrend:
                if start % 60 != 0 or stop % 60 != 0 or stride % 60 != 0:
                    self.write(daqd.DAQD_NOT_FOUND)
                    self.set_error_message("Requesting m-trend data not aligned to 60s gps boundaries")
                    return
            gps_offset = 0
        else:
            allowed = (class_types.ONLINE, class_types.TESTPT)
            for entry in selections:
                if entry.class_type() not in allowed:
                    self.write(daqd.DAQD_NOT_FOUND)
                    self.set_error_message("Requesting non online channel {0} in an online data request".format(entry.name()))
            gps_offset = self._gpstime()

        if stride == 0:
            if online:
                stride = 1
        if stride > (stop - start):
            stride = stop - start

        self.write(daqd.DAQD_OK)

        self.write(binascii.hexlify(struct.pack("!L", self.writer_id)))
        offline_val = 1
        if online:
            offline_val = 0
        self.write(struct.pack("!L", offline_val))

        self.writer_id += 1

        cur = start
        seq = 0

        prev_stride = 0

        while cur < stop:
            if cur + stride > stop:
                stride = stop - cur

            if online:
                end_time = cur + gps_offset + stride
                while self._gpstime() < end_time:
                    time.sleep(0.2)

            self.write_channel_data_block(selections, cur + gps_offset, stride, prev_stride, seq)

            prev_stride = stride
            seq += 1
            cur += stride

    def handle_check_data(self, line):
        """
        Handle the check-data command
        :param line: start stop {channels}
        :return:
        """
        start, stop, stride, channels = parse_get_data_line(line)
        if len(channels) > self.__channel_cap:
            raise DAQDSyntaxError(
                "Please limit requests to {0} or less channels at a time.  This limit is to keep timeout errors down, "
                "we are working on raising it.".format(
                    self.__channel_cap))

        if start == 0:
            self.write(daqd.DAQD_OK)
            return
        self.stats.add_channel_list(channels)
        selections = self._select_channels_from_list(channels)
        if not self._data_available(start, stop, selections):
            self.write(daqd.DAQD_NOT_FOUND)
            return
        for entry in selections:
            if entry.on_tape(start, stop):
                self.set_error_message("{0} is on tape".format(entry.name()))
                self.write(daqd.DAQD_DATA_ON_TAPE)
                return
        self.write(daqd.DAQD_OK)

    def handle_list_epochs(self, line):
        if line != "":
            raise DAQDSyntaxError()
        epochs = []
        epoch_keys = sorted(self.epochs.keys())
        for epoch in epoch_keys:
            epochs.append("{0}={1}-{2}".format(epoch,
                                               self.epochs[epoch][0],
                                               self.epochs[epoch][1]
                                               ))
        out = " ".join(epochs)
        self.write(daqd.DAQD_OK)
        self.write_int32(len(out))
        self.write(out)

    def handle_set_epoch(self, line):
        """

        :param line: start-stop; or name;
        """
        line = line.rstrip('; \t\n')
        line = line.strip()

        start = self.epoch_start
        stop = self.epoch_stop
        try:
            if line in self.epochs:
                start = self.epochs[line][0]
                stop = self.epochs[line][1]
            else:
                parts = line.split('-', 1)
                if len(parts) == 2:
                    start = int(parts[0])
                    stop = int(parts[1])
                else:
                    parts = line.split(":")
                    if len(parts) == 2:
                        start = int(parts[0])
                        stop = start + int(parts[1])
                    else:
                        raise Exception()
            if stop < start or start < 0:
                raise Exception()
        except:
            raise DAQDSyntaxError("Invalid epoch.")
        self.epoch_start = start
        self.epoch_stop = stop

        self.write(daqd.DAQD_OK)

    def handle_get_last_message(self, line):
        msg = self.__error_msg
        self.clear_error_message()
        self.write(daqd.DAQD_OK)
        self.write_int32(len(msg))
        self.write(msg)

    def clear_error_message(self):
        self.__error_msg = ""

    def set_error_message(self, msg):
        if msg is None or msg == "":
            return
        self.__error_msg += ":" + msg

    def parse_and_handle_commands(self):
        """
        The main command loop.
        :return:  Nothing
        """

        class QuitLoopException(Exception):
            pass

        def do_quit(line):
            raise QuitLoopException()

        cmd_log = logging.getLogger(__name__ + ".commands")

        cmd_map = {
            'authorize': self.handle_authorize,
            'server-protocol-version;': self.handle_proto_ver,
            'server-protocol-revision;': self.handle_proto_rev_default,
            'server-protocol-revision': self.handle_proto_rev_specific_version_requested,
            "count-channels": self.handle_count_channels,
            "get-channel-crc;": self.handle_get_channel_crc,
            "get-channels": self.handle_get_channels,
            "get-last-message;": self.handle_get_last_message,
            "get-source-data": self.handle_get_source_data,
            "get-source-list": self.handle_get_source_list,
            "get-data": self.handle_get_data,
            "get-online-data": self.handle_get_online_data,
            "check-data": self.handle_check_data,
            "set-epoch": self.handle_set_epoch,
            "list-epochs;": self.handle_list_epochs,
            "quit;": do_quit,
        }
        try:
            while True:
                line = self.read_line().strip()
                parts = line.split(None, 1)
                cmd = parts[0]
                if len(parts) == 1:
                    parts.append('')
                try:
                    if cmd != "get-last-message;":
                        self.clear_error_message()
                    if cmd in cmd_map:
                        self.stats.start_command(cmd.strip(';'))
                        self.bytes_written = 0
                        cmd_log.info("command: {0} {1}".format(cmd, parts[1]))
                        start_time = datetime.datetime.now()
                        cmd_map[cmd](parts[1])
                        end_time = datetime.datetime.now()
                        delta = end_time - start_time
                        cmd_log.info(
                            "completed: {0}: completed in {1}".
                                format(cmd, delta))
                        self.stats.add_bytes_sent(self.bytes_written)
                        self.stats.finish_command()
                    else:
                        cmd_log.info("Invalid command received")
                        raise DAQDException(
                            daqd.DAQD_COMMAND_SYNTAX, "Invalid command")
                except DAQDException as err:
                    cmd_log.info("completed: {0}: error {1}:{2}".
                        format(
                        cmd, err.code, err.message
                    ))
                    self.set_error_message(err.message)
                    self.write(err.code)
                    self.stats.finish_command()
        except QuitLoopException:
            self.stats.finish_command()
            self.stats.close()


class NDS2Handler(socketserver.BaseRequestHandler):
    def setup(self):
        logging.info("Connection from {0}".format(self.client_address))
        self.nds2 = NDS2ServerLogic(self.request,
                                    stats_factory(
                                        self.server.stats_server_uri),
                                    self.server.chan_list
                                    )

    def handle(self):
        self.nds2.parse_and_handle_commands()


def create_arg_parser():
    parser = argparse.ArgumentParser(description="""
NDS2 Server, distributed architecture experiment.
This is a server process that speaks the NDS2 protocol.
It makes use of a distributed set of services
to help serve data to the clients.""")
    parser.add_argument('-l', '--listen', default="localhost",
                        help="Interface to listen on [localhost]")
    parser.add_argument('-p', '--port', default=31200,
                        type=int, help="Port to listen on [31200]")
    parser.add_argument("-s", "--stats", default="",
                        help="Statistics server.  "
                             "Default is ['']")
    parser.add_argument("--syslog", default="",
                        help="Specify a syslog server to connect to.  "
                             "host:port or path to socket"
                             "default is no connection")
    parser.add_argument("-m", "--module", default="sample_chan_list",
                        help="Python module to import the channel list from")
    default = "local3"
    parser.add_argument("--syslog-facility", default=default,
                        help="facility to use when using the syslog"
                             "default is [{0}]".format(default))
    return parser


# def do_monitor_process(concurrent_obj):
#     from multiprocessing import Process
#
#     def monitor_loop(concurrent_obj):
#         import time
#         last = (0, 0)
#         while True:
#             cur = concurrent_obj.get_data_counts()
#             if cur != last:
#                 print cur
#             last = cur
#             time.sleep(0.25)
#
#     p = Process(target=monitor_loop, args=(concurrent_obj,))
#     p.start()

def main():
    parser = create_arg_parser()
    args = parser.parse_args()
    print(args)

    req_host = args.listen
    req_port = args.port

    if args.syslog != '':
        sys_target = args.syslog
        if ":" in args.syslog:
            sys_target = args.syslog.spserverlit(":", 1)
        sys_handler = logging.handlers.SysLogHandler(address=sys_target,
                                                     facility=args.syslog_facility)
        sys_handler.setLevel(logging.DEBUG)
        fmt = "nds2simple[%(process)d]:" + logging.BASIC_FORMAT
        sys_handler.setFormatter(logging.Formatter(fmt))
        logging.getLogger().addHandler(sys_handler)
        logging.getLogger().setLevel(logging.DEBUG)

    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)

    channel_list_object = simple_chan_list()

    chan_mod = importlib.import_module(args.module)
    for chan in chan_mod.channel_list():
        logger.info("Loading channel {0}".format(chan.name()))
        channel_list_object.add_channel(chan)
    channel_list_object.finalize()

    server = ForkingServer((req_host, req_port), NDS2Handler)
    ip, port = server.server_address
    server.chan_list = channel_list_object

    server.stats_server_uri = args.stats

    logger.info("NDS2 server starting at {0}:{1}".format(ip, port))
    logger.info("Stats server: {0}".format(server.stats_server_uri))

    server.allow_reuse_address = True
    server.serve_forever()


if __name__ == '__main__':
    # import cProfile

    # cProfile.run("main()")
    main()

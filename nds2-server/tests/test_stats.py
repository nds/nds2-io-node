# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase

from ..stats import stats_factory


class TestDefaultStats(TestCase):
    def test_default(self):
        stats = stats_factory("")
        self.assertIsNotNone(stats)

        stats.start_command("some text here")
        stats.add_channel_list([])
        stats.add_channel_list(['abc', 'def'])
        stats.add_bytes_sent(0)
        stats.add_bytes_sent(50)
        stats.finish_command()
        stats.close()

        # need to work out how to test this, it requires a web server
        # to be listening
        #
        # class TestSimpleHttpStats(TestCase):
        #     def test_simplehttp(self):
        #         from stats import stats_factory
        #         from stats import SimpleHttpStats
        #
        #         stats = stats_factory("simple-http://localhost:5000")
        #         self.assertIsNotNone(stats)
        #         self.assertIsInstance(stats, SimpleHttpStats)
        #         stats.close()

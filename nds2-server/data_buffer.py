# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import mmap
import struct


class DataBuffer:
    """
    A write buffer class that is backed by a mmapped file if
    it is too large
    """
    TipPoint = 1024 * 1024 * 10

    def __init__(self, size, tip_point=TipPoint, scale_tip_point=1.0):
        """
        Create a buffer of a given size
        :param size: size in bytes
        :param tip_point: The threshold where it switches from a ram
        backed buffer to a mmapped backed buffer.
        :param scale_tip_point: A scale value to modify the tip_point,
        useful to do general scaling of the tip_point without caring
        exactly what it is.
        """
        self.__size = size
        self.__is_mmapped = False
        self.__buffer = b""
        self.__mbuf = None
        if size > int(tip_point * scale_tip_point):
            self.__is_mmapped = True
            self.__mbuf = mmap.mmap(-1, size, access=mmap.ACCESS_WRITE)

    def size(self):
        """
        :return:  Return the size of the buffer
        """
        return self.__size

    def is_mmapped(self):
        """
        :return: True if this is a mmapped file
        """
        return self.__is_mmapped

    def write_int32(self, val):
        self.write(struct.pack("!I", val))

    def write(self, data):
        need = len(data)
        avail = self.__size - self.used()
        if need > avail:
            raise RuntimeError("Cannot exceed the size of internal buffers")
        if self.__is_mmapped:
            self.__mbuf.write(data)
        else:
            self.__buffer += data

    def used(self):
        """
        :return: The number of bytes written to the buffer
        """
        if self.__is_mmapped:
            return self.__mbuf.tell()
        return len(self.__buffer)

    def flush_to(self, out):
        """
        Flush buffer to out
        :param out: object to write data to
        """
        if self.__is_mmapped:
            count = self.__mbuf.tell()
            chunk = 4096
            cur = 0
            while cur < count:
                delta = count - cur
                if delta < chunk:
                    chunk = delta
                out.write(self.__mbuf[cur:cur + chunk])
                cur += chunk
        else:
            out.write(self.__buffer)
        self.close()

    def close(self):
        if self.__mbuf is not None:
            self.__mbuf.close()
        self.__mbuf = None
        self.__buffer = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False


class Closer:
    def __init__(self):
        self.__objs = []

    def add(self, obj):
        self.__objs.append(obj)

    def __len__(self):
        return len(self.__objs)

    def __getitem__(self, item):
        return self.__objs[item]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, ext_tb):
        for i in range(len(self.__objs)):
            try:
                self.__objs[i].close()
            except:
                pass
        return False

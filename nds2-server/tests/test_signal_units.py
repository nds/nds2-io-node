# Copyright 2018 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import struct
from unittest import TestCase

from ..signal_units import default
from ..signal_units import encode
from ..signal_units import signal_units
from ..signal_units import extract_signal_units
from ..signal_units import decode_signal_unit_length


class TestSignalUnits(TestCase):
    def su_equal(self, su1, su2):
        self.assertEqual(su1.units, su2.units)
        self.assertAlmostEqual(su1.slope, su2.slope)
        self.assertAlmostEqual(su1.offset, su2.offset)

    def test_encode(self):
        su = signal_units(0.0, 1.0, "some units")
        fmt_str = "!b{0}sff".format(len(su.units))
        expected = struct.pack(fmt_str, len(su.units), su.units.encode(), 0.0, 1.0)
        actual = encode(su)
        self.assertEqual(actual, expected)

    def test_encode_long_units(self):
        su = signal_units(0.5, 0.8, "1234567890123456789012345678901234567890abc")
        expected = struct.pack("!b40sff", 40, b"1234567890123456789012345678901234567890", 0.5, 0.8)
        actual = encode(su)
        self.assertEqual(actual, expected)

    def test_encode_empty_units(self):
        su = signal_units(0.5, 0.8, "")
        expected = struct.pack("!b0sff", 0, b"", 0.5, 0.8)
        actual = encode(su)
        self.assertEqual(actual, expected)
        self.assertEqual(len(actual), 4 + 4 + 1)

    def test_encoded_length(self):
        s1 = encode(signal_units(0.0, 1.0, "some units"))
        s1_buf = s1 + b"some other stuff here, lets just take up space"
        self.assertEqual(decode_signal_unit_length(s1_buf), len(s1))

        s2 = encode(signal_units(0.2, 1.5, ""))
        s2_buf = s2 + b"some other stuff here, lets just take up space la de da"
        self.assertEqual(decode_signal_unit_length(s2_buf), len(s2))

    def test_extract_signal_units(self):
        s1_in = signal_units(0.0, 1.0, "some units")
        s1 = encode(s1_in)
        s1_buf = s1 + b"some other stuff here, lets just take up space"
        s1_ex, data = extract_signal_units(s1_buf)
        self.su_equal(s1_in, s1_ex)
        self.assertEqual(data, b"some other stuff here, lets just take up space")

        s2_in = signal_units(0.2, 1.5, "")
        s2 = encode(s2_in)
        s2_buf = s2 + b"some other stuff here, lets just take up space la de da"
        s2_ex, data = extract_signal_units(s2_buf)
        self.su_equal(s2_in, s2_ex)
        self.assertEqual(data, b"some other stuff here, lets just take up space la de da")

    def test_default_signal_units(self):
        self.su_equal(signal_units(0.0, 1.0, "counts"), default())

# Copyright 2020 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from binascii import hexlify
import urllib.request
import json
import struct

from .errors import DAQDFrameIOError
from .helpers import dtype_to_bytes
from .signal_units import default


_aux_remote_proxy_lookup = {}

class Nds3RemoteProxy(object):
    def __init__(self, remote):
        self.__remote = remote

    def remote_data(self, frame_type, chan_info, start, stop):
        url = "http://{0}/proxy/data/{1}/{2}/{3}/".format(self.__remote, frame_type, start, stop)
        full_name = chan_info['full_name'] + "," + chan_info["data_type"]
        body = json.dumps(full_name)
        req = urllib.request.Request(url=url, data=body.encode('utf8'), headers={'content-type':"application/json"},method="POST")
        cur = start
        data = b''
        samples = ""
        with urllib.request.urlopen(req) as resp:
            if resp.getcode() != 200:
                print("response code of {1}".format(resp.getcode()))
                print(hexlify(resp.read()))
                raise DAQDFrameIOError("Error reading remote data")
            # read the header
            size = struct.unpack(">I", resp.read(4))[0]
            block_header = json.loads(resp.read(size).decode('utf8'))
            while block_header['start'] != 0 and block_header['stop'] != 0:
                if block_header["start"] != cur or block_header['channels'] != 1:
                    raise DAQDFrameIOError("Data missing")
                size = struct.unpack(">I", resp.read(4))[0]
                chan_header = json.loads(resp.read(size).decode('utf8'))
                if chan_header["s"] != 0:
                    raise DAQDFrameIOError("Data missing on channel")
                data += resp.read(chan_header["d"])
                cur = block_header["stop"]

                size = struct.unpack(">I", resp.read(4))[0]
                block_header = json.loads(resp.read(size).decode('utf8'))
            if cur != stop:
                DAQDFrameIOError("Remote data did not end at the expected time")
            samples = len(data)//dtype_to_bytes(chan_info)
            print("Returning {0} bytes data {1} samples from {2}", len(data), samples, chan_info)
            return (data, samples, default())
            #print(hexlify(resp.read()))
            #raise DAQDFrameIOError("not done yet")


class null_remote_proxy(object):
    def remote_data(self, frame_type, chan, start, stop):
        raise DAQDFrameIOError("Remote frames are not available")

def register_remote_proxy_lookup(key, remote_proxy):
    """
    Register a remote_proxy object on a specific uri.  Mostly for debugging
    :param key:  Key value
    :param remote_proxy: the remote proxy object
    :return:
    """
    global _aux_remote_proxy_lookup
    _aux_remote_proxy_lookup[key] = remote_proxy

def remote_proxy_factory(uri):
    global _aux_remote_proxy_lookup

    if uri == "":
        return null_remote_proxy()
    if uri.startswith("nds3://"):
        return Nds3RemoteProxy(uri.split("://", 1)[1])
    if uri in _aux_remote_proxy_lookup:
        return _aux_remote_proxy_lookup[uri]
    return null_remote_proxy(uri)
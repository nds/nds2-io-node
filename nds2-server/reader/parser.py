# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from collections import namedtuple

from ..helpers import parse_full_channel_name


ReadArguments = namedtuple('ReadArguments', ['filename', 'frame_offset', 'channels'])


class Parser:
    def __init__(self):
        pass

    def parse(self, text):
        parts = text.split('\t')
        if parts[0] == 'read':
            channel_count = int(parts[3])
            if channel_count != 1:
                raise Exception("Multiple channels are not supported yet")
            chan_info = parse_full_channel_name(parts[4])
            for part in chan_info:
                if part is None:
                    raise Exception("Channel not fully qualified")
            channel = {
                'channel_name': chan_info[0],
                'channel_type': chan_info[1],
                'data_type': chan_info[2],
                'full_name': "{0},{1},{2}".format(chan_info[0], chan_info[1], chan_info[3]),
                'rate': chan_info[3],
            }
            return parts[0], ReadArguments(parts[1], int(parts[2]), [channel])
        if parts[0] == 'quit':
            return 'quit', ()
        raise Exception('Unkown command {0}'.format(parts[0]))

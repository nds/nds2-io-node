import argparse
import json
import logging
import sys
import urllib.request

import netifaces

from .concurrent import concurrent_factory
from .nds2_server import ForkingServer
from .nds2_server import NDS2Handler

from .epochs import default_epoch
from .epochs import parse_epochs_from_file
from .epochs import cleanup_json_epochs

def create_arg_parser():
    parser = argparse.ArgumentParser(description="""
NDS2 Server, distributed architecture experiment.
This is a server process that speaks the NDS2 protocol.
It makes use of a distributed set of services
to help serve data to the clients.""")
    parser.add_argument('-l', '--listen', default="localhost",
                        help="Interface to listen on [localhost]")
    parser.add_argument('-p', '--port', default=31200,
                        type=int, help="Port to listen on [31200]")
    default = "memcached://127.0.0.1"
    parser.add_argument('-m', '--memcache', default=default,
                        help="Memcache servers to connect to.  "
                             "Formatted as memcached://host[,host,host,"
                             "...].  Default of [{0}]".format(
                            default))
    default = "dcache://localhost:20222"
    parser.add_argument("-f", "--framelookup", default=default,
                        help="Frame lookup server.  "
                             "Default is diskcacheAPI [{0}]".format(default))
    default = "nds2://localhost:31201"
    parser.add_argument("-i", "--channelinfo", default=default,
                        help="Channel information server.  "
                             "Default is [{0}]".format(default))
    parser.add_argument("-s", "--stats", default="",
                        help="Statistics server.  "
                             "Default is ['']")
    parser.add_argument("--syslog", default="",
                        help="Specify a syslog server to connect to.  "
                             "host:port or path to socket"
                             "default is no connection")
    default = "local3"
    parser.add_argument("--syslog-facility", default=default,
                        help="facility to use when using the syslog"
                             "default is [{0}]".format(default))
    parser.add_argument("-o", "--online", default="",
                        help="Online channel access method.  "
                             "Default is ['']")
    default = ""
    parser.add_argument("-r", "--reader", default=default,
                        help="Path to the unix socket that the io reader listens on."
                             "Default is none".format(default))
    default = "0"
    parser.add_argument("-c", "--concurrent", default=default,
                        help="Number of concurrent reads that are allowed in the system when "
                             "doing local frameio (< 1 is unbounded).  Default is {0}".format(default))
    parser.add_argument("-P", "--remoteproxy", default="",
                        help="Remote data proxy server.  Default is []")

    parser.add_argument("-C", "--remote-config", default="",
                        help="host:port:config_name used to look up configuration")
    parser.add_argument("--epoch-file", default="",
                        help="file containing the name epochs")
    parser.add_argument("--high-latency-path", default="",
                        help="Root path that may be on tape, defaults to all paths")
    return parser


def parse_args():
    parser = create_arg_parser()
    args = parser.parse_args()
    high_latency_path = args.high_latency_path
    if args.remote_config != "":
        remote = args.remote_config.split(":")
        if len(remote) != 3:
            parser.print_help()
            sys.exit(1)
        args = configure_from_remote(host=remote[0], port=remote[1], config_name=remote[2])
        args.high_latency_path = high_latency_path
    elif args.epoch_file != "":
        setattr(args, "epochs", parse_epochs_from_file(args.epoch_file))
    else:
        setattr(args, "epochs", default_epoch())
    return args


def string_match_length(s1, s2):
    match_len = 0
    for entry in zip(s1, s2):
        if entry[0] == entry[1]:
            match_len += 1
        else:
            break
    return match_len


def configure_from_remote(host, port, config_name="default"):
    url = "http://{0}:{1}/config/".format(host, port)
    with urllib.request.urlopen(url) as resp:
        if resp.getcode() != 200:
            print("Unable to retrieve configuration information from remote source")
            sys.exit(1)
        data = json.load(resp)

        cfg = data["io_node_cfg"][config_name]
        args = argparse.Namespace()

        setattr(args, "channelinfo", cfg["metadata_server"])
        setattr(args, "concurrent", cfg["concurrency"])
        setattr(args, "framelookup", cfg["frame_lookup"])
        setattr(args, "memcache", cfg["memcache"])
        setattr(args, "online", cfg["online"])
        setattr(args, "port", cfg["port"])
        setattr(args, "reader", "")
        setattr(args, "remoteproxy", cfg["remote_data_proxy"])
        setattr(args, "stats", cfg["stats"])
        setattr(args, "syslog", "")
        setattr(args, "syslog_facility", "")
        setattr(args, "epochs", cleanup_json_epochs(cfg["epochs"]))

        match_address = cfg["network"]

        listen_addr = None
        match_len = 0
        for iface in netifaces.interfaces():
            try:
                cur_addr = netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr']
                cur_match_len = string_match_length(match_address, cur_addr)
                if cur_match_len > match_len:
                    match_len = cur_match_len
                    listen_addr = cur_addr
                # print(netifaces.ifaddresses(iface)[netifaces.AF_INET])
            except KeyError:
                pass
        if listen_addr is None:
            print("Unable to determine interface to listen on")
            sys.exit(1)
        setattr(args, "listen", listen_addr)
        return args


# def do_monitor_process(concurrent_obj):
#     from multiprocessing import Process
#
#     def monitor_loop(concurrent_obj):
#         import time
#         last = (0, 0)
#         while True:
#             cur = concurrent_obj.get_data_counts()
#             if cur != last:
#                 print cur
#             last = cur
#             time.sleep(0.25)
#
#     p = Process(target=monitor_loop, args=(concurrent_obj,))
#     p.start()

def main():
    args = parse_args()

    req_host = args.listen
    req_port = args.port

    if args.syslog != '':
        sys_target = args.syslog
        if ":" in args.syslog:
            sys_target = args.syslog.spserverlit(":", 1)
        sys_handler = logging.handlers.SysLogHandler(
            address=sys_target,
            facility=args.syslog_facility,
        )
        sys_handler.setLevel(logging.DEBUG)
        fmt = "nds2alt[%(process)d]:" + logging.BASIC_FORMAT
        sys_handler.setFormatter(logging.Formatter(fmt))
        logging.getLogger().addHandler(sys_handler)
        logging.getLogger().setLevel(logging.DEBUG)

    fmt = '{%(process)d}:' + logging.BASIC_FORMAT
    logging.basicConfig(format=fmt, level=logging.DEBUG)
    logger = logging.getLogger(__name__)

    # we need to set these up before we bind the server port
    launch_monitor_process = False
    concurrent_uri = None
    if args.reader == "":
        concurrent_uri = args.concurrent
        launch_monitor_process = True
    concurrent_obj = concurrent_factory(concurrent_uri)
    if launch_monitor_process:
        pass
        # do_monitor_process(concurrent_obj)

    server = ForkingServer((req_host, req_port), NDS2Handler)
    ip, port = server.server_address

    server.chan_info_uri = args.channelinfo
    server.memory_cache_uri = args.memcache
    server.frame_lookup_uri = args.framelookup
    server.stats_server_uri = args.stats
    server.online_uri = args.online
    server.reader_socket = args.reader
    server.concurrent = concurrent_obj
    server.remote_proxy_uri = args.remoteproxy
    server.epochs = args.epochs
    server.high_latency_path = args.high_latency_path

    logger.info("NDS2 server starting at {0}:{1}".format(ip, port))
    logger.info("Channel info server: {0}".format(server.chan_info_uri))
    logger.info("Memory cache server: {0}".format(server.memory_cache_uri))
    logger.info("Frame lookup server: {0}".format(server.frame_lookup_uri))
    logger.info("Stats server: {0}".format(server.stats_server_uri))
    logger.info("Online service: {0}".format(server.online_uri))
    logger.info("FrameReader: {0}".format(server.reader_socket))
    logger.info("ConcurrentIO: {0}".format(args.concurrent))
    logger.info("RemoteProxy: {0}".format(server.remote_proxy_uri))
    server.max_children = 4000

    server.allow_reuse_address = True
    server.serve_forever()


if __name__ == '__main__':
    # import cProfile
    # cProfile.run("main()")
    main()

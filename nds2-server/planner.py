# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from . import daqd
from . import errors
from .helpers import intersects


class PlanBundle:
    def __init__(self, channels, frame_type):
        """
        :param channels: A list of channel names
        :param frame_type: A frame type
        """
        self.channels = channels
        self.frame_type = frame_type


class PlanSegment:
    """
    A section of time with a mapping of frames to frame types
    """

    def __init__(self, bundles, gps_start, gps_stop):
        """
        :param bundles: A collection of channel -> frame mappings
        :param gps_start: Start time inclusive
        :param gps_stop: Stop time exclusive
        """
        self.bundles = bundles
        self.gps_start = gps_start
        self.gps_stop = gps_stop

    def get_bundle(self, channel):
        """
        Given a channel return the bundle that contains it.
        :param channel: Channel name as a string
        :return: The PlanBundle containing it, or None
        """
        for entry in self.bundles:
            if channel in entry.channels:
                return entry
        return None

    def get_frame_type(self, channel):
        """
        Given a channel, return the frame type it should be read from
        :param channel: Channel name as string
        :return: Frame type as string that it should be read from,
         "" if not found.
        """
        bundle = self.get_bundle(channel)
        if bundle is not None:
            return bundle.frame_type
        return ""


class Plan:
    """
    A plan object represents the plan to fetch data
    """

    def __init__(self, segments):
        """
        :param segments: A list of PlanSegments
        """
        self.segments = segments

    def sub_plan(self, gps_start, gps_stop):
        """
        Return a sub plan for the given time frame.
        [gps_start, gps_stop) must be within the global plans [start,stop)
        :param gps_start: sub plan start (inclusive)
        :param gps_stop: sub plan stop (exclusive)
        :return: a list of PlanSegments
        """
        return [seg for seg in self.segments
                if intersects(gps_start, gps_stop,
                              seg.gps_start, seg.gps_stop)]


class PlannerMatrixCell():
    """
    A cell in a PlannerMatrixRow
    """

    def __init__(self, start=0, stop=0, frame_types=()):
        """
        Initialize the matrix cell
        :param start: Start time (inclusive) for the cell (default 0)
        :param stop: Stop time (exclusive) for the cell (default 0)
        :param frame_types: List of frame types for the cell (default [])
        """
        self.start = start
        self.stop = stop
        types = set(frame_types)
        if None in types:
            types.discard(None)
        self.type = list(types)

    def clone_dim(self):
        """
        :return: Return a new cell that is a copy of this cell.
        With an empty frame type list
        """
        return PlannerMatrixCell(self.start, self.stop, [])

    def add_frame_type(self, frame_type):
        """
        Add a frame type to the frame
        :param frame_type: Frame type to add
        :return: Nothing
        """
        if frame_type is None:
            return
        if frame_type not in self.type:
            self.type.append(frame_type)

    def __eq__(self, other):
        """
        :param other: Another PlannerMatrixCell
        :return: True if the objects represented by self and other
        have the same values.  Ordering of frame types does not matter
        """
        if self.start != other.start or self.stop != other.stop:
            return False
        set1 = set(self.type)
        set2 = set(other.type)
        return set1 == set2


class PlannerMatrixRow():
    """
    A row in a PlannerMatrix
    """

    @staticmethod
    def find_boundaries(avails):
        point_set = set([])
        for avail in avails:
            for segment in avails[avail]:
                point_set.add(segment['start'])
                point_set.add(segment['stop'])
        points = list(point_set)
        points.sort()
        return points

    def __init__(self, channel, shape_points=[]):
        self.channel = channel
        self.columns = []
        if len(shape_points) > 1:
            start = shape_points[0]
            for end in shape_points[1:]:
                self.columns.append(PlannerMatrixCell(start, end))
                start = end

    def add_frame_type(self, gps_start, gps_stop, frame_type):
        for entry in self.columns:
            if intersects(gps_start, gps_stop, entry.start, entry.stop):
                entry.add_frame_type(frame_type)

    # def clone_structure(self, new_channel):
    #     """
    #     :param new_channel: The channel for the new row
    #     :return: Return a new PlannerMatrixRow with the same structure,
    #     but with a new channel, and no frame type assignments.
    #     """
    #     results = PlannerMatrixRow(new_channel)
    #     for col in self.columns:
    #         results.columns.append(col)

    def order_cells(self):
        """
        Ensure that the cells are properly ordered
        :return: Nothing
        """
        self.columns.sort(key=lambda cell: cell.start)

    def is_contiguous(self):
        """
        :return: True if there are cells, there are no gaps between cells, and
        each cell has a frame type.  Otherwise False
        """
        if len(self.columns) == 0:
            return False
        i = 0
        prev = 0
        for cell in self.columns:
            if len(cell.type) < 1:
                return False
            if i == 0:
                prev = cell.start
            if cell.start != prev:
                return False
            prev = cell.stop
            i += 1
        return True

    def __len__(self):
        return len(self.columns)

    def __getitem__(self, item):
        return self.columns.__getitem__(item)


class PlannerMatrix():
    """
    The planner matrix helps organize a lookup plan
    """

    def __init__(self, channels, avail_list={}):
        """
        Create a planning matrix
        :param channels: The list of channels, in order, that will be used
        """
        if len(channels) < 1:
            raise Exception(
                "Schedule planning matrix requires at least 1 channel")
        self.channels = tuple(channels)
        self.rows = []
        self.__finalized = False
        self.__contiguous = True
        self.__gps_start = 0
        self.__gps_stop = 0

        if len(avail_list) > 0:
            points = PlannerMatrixRow.find_boundaries(avail_list)
            for chan in channels:
                row = PlannerMatrixRow(chan, points)
                if chan in avail_list:
                    for segment in avail_list[chan]:
                        row.add_frame_type(segment['start'],
                                           segment['stop'],
                                           segment['frame_type'])
                self.rows.append(row)
            self.finalize_matrix()
        else:
            for chan in channels:
                self.rows.append(PlannerMatrixRow(chan))

    def finalize_matrix(self):
        """
        Signal that the matrix is complete
        :return: Nothing
        """
        self.__finalized = True
        contiguous = True
        for row in self.rows:
            row.order_cells()
            contiguous = contiguous and row.is_contiguous()
        self.__contiguous = contiguous
        if len(self.rows[0]) > 0:
            self.__gps_start = self.rows[0][0].start
            self.__gps_stop = self.rows[0][-1].stop

    def is_contiguous(self):
        if not self.__finalized:
            raise RuntimeError(
                "continuity check on the planning matrix must"
                " happen after it is finalized")
        return self.__contiguous

    def gps_start(self):
        """
        :return: The gps start time of the matrix rows
        :note: The matrix must be finalized
        """
        if not self.__finalized:
            raise RuntimeError(
                "gps start time is not available until the"
                " planning matrix is finalized")
        return self.__gps_start

    def gps_stop(self):
        """
        :return: The gps start time of the matrix rows
        :note: The matrix must be finalized
        """
        if not self.__finalized:
            raise RuntimeError(
                "gps stop time is not available until the"
                " planning matrix is finalized")
        return self.__gps_stop

    def get_column_frame_priority(self, index):
        """
        Return an ordered list of frame_type priorities
        for column index of the matrix
        :param index: Column index
        :return:
        """
        types = {}
        for row in self.rows:
            for frame_type in row[index].type:
                if frame_type in types:
                    types[frame_type] += 1
                else:
                    types[frame_type] = 1
        priority_list = list(types.keys())
        priority_list.sort(key=lambda x: types[x], reverse=True)
        return priority_list

    def get_cell(self, chan, index):
        """
        :param chan: Channel in the matrix
        :param index: Column in the matrix (must be >= 0 && < get_column_count)
        :return: A PlannerMatrixCell for the given channel at the given column
        """
        if not self.__finalized:
            raise RuntimeError(
                "Cannot get cell frame types until the matrix is finalized")
        row_index = self.channels.index(chan)
        row = self.rows[row_index]
        return row[index]

    def get_column_count(self):
        """
        :return: The number of columns in a finalized matrix
        :note: finalize_matrix must be called first
        """
        if not self.__finalized:
            raise RuntimeError(
                "column count is not available until the"
                " planning matrix is finalized")
        return len(self.rows[0])


class Planner:
    """
    The Planner class takes channel lists and availability lists and creates
    a plan/schedule for retrieving the data attempting to minimize
    the number of frames opened.
    """

    def __init__(self):
        pass

    def plan(self, channels, avail_lists, gps_start, gps_stop):
        """
        Given a list of channel availabilities, and a [start,stop)
        timeframe schedule out the frames to be read.
        :param channels: list/tuple of channels
        :param avail_lists: An availability list
        :param gps_start: start time (inclusive)
        :param gps_stop: stop time (exclusive)
        :return:
        """
        plan_matrix = PlannerMatrix(channels, avail_lists)
        if not plan_matrix.is_contiguous():
            raise errors.DAQDException(daqd.DAQD_NOT_FOUND)
        if plan_matrix.gps_start() != gps_start or \
                        plan_matrix.gps_stop() != gps_stop:
            raise errors.DAQDException(daqd.DAQD_NOT_FOUND)
        columns = plan_matrix.get_column_count()
        plan_segments = []
        for i in range(columns):
            priority = plan_matrix.get_column_frame_priority(i)
            schedule = {}
            start = None
            stop = None
            for chan in channels:
                cur_cell = plan_matrix.get_cell(chan, i)
                if start is None:
                    start = cur_cell.start
                    stop = cur_cell.stop
                for ref_type in priority:
                    if ref_type in cur_cell.type:
                        ch_list = schedule.get(ref_type, [])
                        ch_list.append(chan)
                        schedule[ref_type] = ch_list
                        break
            if len(schedule) > 0:
                bundles = []
                for frame_type in schedule.keys():
                    bundles.append(PlanBundle(
                        schedule[frame_type], frame_type))
                plan_segments.append(PlanSegment(bundles, start, stop))
        return Plan(plan_segments)
        # return Plan([PlanSegment([PlanBundle(channels, 'H-H1_R'), ],
        # gps_start, gps_stop), ])

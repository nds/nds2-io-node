# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import os.path

from .errors import DAQDSyntaxError


def range_contains(start, stop, value):
    """
    :param start: Inclusive start value
    :param stop:  Exclusive stop value
    :param value: Test value
    :return: Return True if value in [start,stop) else False
    """
    return value >= start and value < stop


def intersects(start1, stop1, start2, stop2):
    """
    :param start1: Inclusive start value for range 1
    :param stop1: Exclusive stop value for range 1
    :param start2: Inclusive start value for range 2
    :param stop2: Exclusive stop value for range 2
    :return: True if [start1, stop1) intersects/overlaps
    [start2, stop2) else False
    """
    if stop1 <= start2 or stop2 <= start1:
        return False
    return True


def intersection(start1, stop1, start2, stop2):
    """
    :param start1: Inclusive start value for range 1
    :param stop1: Exclusive stop value for range 1
    :param start2: Inclusive start value for range 2
    :param stop2: Exclusive stop value for range 2
    :return: Return the intersection of [start1,stop1) [start2,stop2)
    (None, None) if there is no intersection
    """
    # order them so start1 <= start2
    if start1 > start2:
        tmp = start1
        start1 = start2
        start2 = tmp
        tmp = stop1
        stop1 = stop2
        stop2 = tmp
    if stop1 <= start2:
        return (None, None)
    return (max(start1, start2), min(stop1, stop2))


def dtype_to_bytes(chan):
    """
    Given a channel return the size in bytes of a sample
    :param chan: a channel dict with at least the field 'data_type'
    :return: int value with the size, or exception if an invalid type
    """
    _dtype_to_bytes = {
        'int_2': 2,
        'int_4': 4,
        'int_8': 8,
        'real_4': 4,
        'real_8': 8,
        'uint_4': 4,
    }
    return _dtype_to_bytes[chan["data_type"]]


def seconds_to_samples(chan, seconds):
    """
    For a given channel give a count of seconds determine
    how may samples are present
    :param chan:
    :param seconds:
    :return:
    """
    if chan['rate'] < 1.0:
        return int(seconds / 60)
    return int(seconds * chan['rate'])


def samples_to_seconds(chan, samples):
    """
    For a given channel given a sample count
    return the number of seconds involved
    :param chan:
    :param samples:
    :return:
    """
    if chan['rate'] < 1.0:
        return samples * 60
    return int(samples / chan['rate'])


def samples_to_bytes(chan, samples):
    """
    For a given channel given a sample count return the number of bytes used
    :param chan:
    :param samples:
    :return:
    """
    return samples * dtype_to_bytes(chan)


def bytes_to_samples(chan, byte_count):
    """
    For a given channel given a byte count return the number of samples
    :param chan:
    :param byte_count:
    :return:
    """
    return int(byte_count / dtype_to_bytes(chan))


def get_frame_name_info(fname):
    """
    Given a frame filename return the [start, stop) times
    as encoded in the filename.
    :param fname: The frame filename
    :return: (frame_type, start, stop) on success or
    (None, None, None) on error
    """
    basename = os.path.basename(fname)
    rootname = os.path.splitext(basename)[0]
    parts = rootname.split('-')
    if len(parts) != 4:
        return None, None, None
    frame_start = int(parts[2])
    frame_stop = frame_start + int(parts[3])
    return "-".join(parts[0:2]), frame_start, frame_stop


def fix_frame_names(fnames):
    """
    Given a list of frame names, make sure they have the proper
    extension.
    :param fnames: list of paths to frame files.
    :return: List of paths, all will have .gwf extensions
    """
    results = []
    for fname in fnames:
        fname = fname.strip()
        if fname == "":
            continue
        base = os.path.splitext(fname)[0].rstrip('.')
        results.append(base + '.gwf')
    return results


def optimize_frame_selection(fname, path_exists=os.path.exists):
    archive = '/archive/frames/'
    if fname.startswith(archive):
        new_name = '/ceph/frames/' + fname[len(archive):]
        if path_exists(new_name):
            return new_name
    return fname


def parse_channel_name(channel_name):
    """
    Parse a channel name and break it down to its component parts
    :param channel_name: A channel name
    :return:  a tuple (base_name, chtype, rate)
    chtype or rate may be None if they are not specified
    rate will be None or a float
    :note: Will raise an exception for a invalid channel name
    """

    def is_float(val):
        try:
            float(val)
        except:
            return False
        return True

    channel_name = channel_name.replace('%', ',')
    parts = channel_name.split(',')
    if len(parts) > 3:
        raise Exception("Invalid name")
    base = parts[0]
    parts = parts[1:]

    chtype = None
    rate = None
    for part in parts:
        if is_float(part):
            if rate is not None:
                raise Exception("Invalid name, double rate")
            rate = float(part)
        elif chtype is not None:
            raise Exception("Invalid name, double type")
        else:
            chtype = part
    return base, chtype, rate


def parse_full_channel_name(channel_name):
    """
    Parse a full channel name and break it down to its component parts
    :param channel_name: A channel name
    :return:  a tuple (base_name, chtype, dtype, rate)
    chtype, dtype, or rate may be None if they are not specified
    rate will be None or a float
    :note: Will raise an exception for a invalid channel name
    """

    def is_float(val):
        try:
            float(val)
        except:
            return False
        return True

    dtypes = ('int_2', 'int_4', 'int_8', 'real_4', 'real_8', 'uint_4')

    channel_name = channel_name.replace('%', ',')
    parts = channel_name.split(',')
    if len(parts) > 4:
        raise Exception("Invalid name")
    base = parts[0]
    parts = parts[1:]

    chtype = None
    dtype = None
    rate = None
    for part in parts:
        if is_float(part):
            if rate is not None:
                raise Exception("Invalid name, double rate")
            rate = float(part)
        elif part in dtypes:
            if dtype is not None:
                raise Exception("Invalid name, double data type")
            dtype = part
        elif chtype is not None:
            raise Exception("Invalid name, double type")
        else:
            chtype = part
    return base, chtype, dtype, rate


def get_frame_intersection_info(fname, gps_start, gps_stop):
    """
    :param fname: full path to frame
    :param gps_start: start of request
    :param gps_stop: end of request
    :return: None on error, or {
        'frame_type':
        'frame_start':
        'frame_stop':
        'gps_start': Where in the request this frame starts
        'gps_stop': where in the request this frame stops
        'filename': path to frame
        'url': None for now
    }
    """
    cur_frame_type, frame_start, frame_stop = get_frame_name_info(
        fname)
    if cur_frame_type is None:
        return None

    cur_gps_start, cur_gps_stop = intersection(
        frame_start, frame_stop, gps_start, gps_stop)
    if cur_gps_start is None:
        return None

    entry = {
        'frame_type': cur_frame_type,
        'frame_start': frame_start,
        'frame_stop': frame_stop,
        'gps_start': cur_gps_start,
        'gps_stop': cur_gps_stop,
        'filename': fname,
        'url': None,
    }
    return entry


def parse_get_data_line(line):
    """Parse the input for a get/check data command
    :param line: the command line, should be of the form
    start stop [stride] {channels}
    :return: A tuple (start, stop, stride, [channels])
    May raise DAQDSyntaxError
    """
    idx = line.find('{')
    if idx < 0 or line[-1] != ';' or line[-2] != '}':
        raise DAQDSyntaxError()
    parts = line[:idx - 1].strip().split()
    channels = line[idx + 1:-2].strip().split()

    if len(parts) < 2 or len(parts) > 3:
        raise DAQDSyntaxError()
    stride = 0
    try:
        if len(parts) == 3:
            stride = int(parts[2])
        start = int(parts[0])
        stop = int(parts[1])
    except:
        raise DAQDSyntaxError
    if start > stop:
        raise DAQDSyntaxError()
    return (start, stop, stride, channels)


def parse_get_frame_data_line(line):
    """Parse the input for a _get-frame-data command
    :param line: the command line, should be of the form
    frame_type start stop {channels}
    The channel entries must be qualified with data type, rate
    :return: A tuple(frame_type, start, stop, [channels])
    May raise DAQDSyntaxError
    """
    idx = line.find('{')
    if idx < 0 or line[-1] != ';' or line[-2] != '}':
        raise DAQDSyntaxError()
    parts = line[:idx - 1].strip().split()
    channel_list = line[idx + 1:-2].strip().split()
    if len(parts) != 3 or len(channel_list) == 0:
        raise DAQDSyntaxError
    try:
        frame_type = parts[0]
        start = int(parts[1])
        stop = int(parts[2])
    except:
        raise DAQDSyntaxError
    if start > stop:
        raise DAQDSyntaxError
    channels = []
    for entry in channel_list:
        parts = entry.replace("%", ",").split(",")
        name = parts[0]
        rate = 0.0
        class_type = "unknown"
        data_type = "unknown"
        for part in parts[1:]:
            try:
                rate = float(part)
                continue
            except ValueError:
                pass
            if part in ("int_2", "int_4", "int_8", "real_4", "real_8", "uint_4"):
                data_type = part
            elif part in ("raw", "online", "m-trend", "s-trend", "reduced", "static", "test-pt"):
                class_type = part
            else:
                raise DAQDSyntaxError("Channels must be fully specified and must not have any unknown/unrecognized parts")
        if rate == 0.0 or data_type == "unknown":
            raise DAQDSyntaxError("Channels must be fully specified and must not have any unknown/unrecognized parts")
        channels.append({"channel_name": name,
                         "data_type": data_type,
                         "channel_type": class_type,
                         "rate": rate,
                         "full_name": "{0},{1},{2},{3}".format(name, class_type, data_type, rate)})

    return (frame_type, start, stop, channels)

def parse_get_online_data_line(line):
    """Parse the input for a get/check data command
    :param line: the command line, should be of the form
    stride {channels}
       or
    stop stride {channels}
    :return: A tuple (start, stop, stride, [channels])
    May raise DAQDSyntaxError
    """
    idx = line.find('{')
    if idx < 0 or line[-1] != ';' or line[-2] != '}':
        raise DAQDSyntaxError()
    parts = line[:idx - 1].strip().split()
    channels = line[idx + 1:-2].strip().split()

    if len(parts) < 1 or len(parts) > 2:
        raise DAQDSyntaxError()
    start = 0
    try:
        if len(parts) == 2:
            stop = int(parts[0])
            stride = int(parts[1])
        else:
            stop = 0
            stride = int(parts[0])
    except:
        raise DAQDSyntaxError
    if start > stop:
        raise DAQDSyntaxError()
    return (start, stop, stride, channels)


def calc_cache_max_block_size(chan, cache):
    """
    Return the maximum block size in seconds that can be cached for
    the given channel
    :param chan: channel to construct the block size for
    :return: the maximum block size in seconds (as an int)
    """
    sample_size = dtype_to_bytes(chan)
    max_samples = int(cache.max_block_size() // sample_size)
    if chan["rate"] >= 1.0:
        return int(max_samples / chan["rate"])
    return max_samples * 60

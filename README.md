# NDS2 IO Node #
The nds2-io-node is part of a rearchitecturing of the NDS2 server.  This architecture breaks up the nds2 server into a series of compontents:

 * Channel Metadata server
 * IO Node
 * Cache
 * Frame tracker

This software is the IO Node.  It speaks the nds2 wire protocol and can read from frame (and other) data sources.
The IO node is a relatively light weight service that does not contain a channel or frame database.
Instead, it requires that these resources be provided by other external servers.

## Other Components ##

### Channel Information Server ###

This the heart of the NDS2 system, it understands the channel history and maintains the current channel availability lists, which it servers out to the NDS2 server.

This is provided by the nds-metadata-server.

### DiskCacheAPI Server ###

The LDAS disk cache API server is used to resolve final frame file names from channel availability information (frame type and gps times).

### Network Aware Cache ###
 
[Memcache](https://memcached.org/) is to provide a shared cache system.  When data is requested the NDS2 server takes a data request (start, stop, channels) and determins how that maps to a cache lookup index.  Once the index has been computed memcache is checked to see if the data is already cached.  If so the read can come from the cache, otherwise data is read from the disk and pushed into the cache.

By defining a consistent cache lookup/key system, the cache can be used by more than one service.

Memcache instances should not be publicly accessible.

## Installing ##

### Python Dependancies ###

The current implementation is done in python.  It has been tested on Rocky Linux 8 and Debian Linux 10 &amp; 11.

clone the source

> $ git clone git@git.ligo.org:jonathan-hanks/nds2-alt-architecture.git

create a virtual environment and activate it (this can be put most anywhere).

> $ virtualenv --system-site-packages -p python3 nds2-io-node
> $ . nds2-alt-ve/bin/activate

Install the requirements using pip

> $ pip install -r nds2-alt-architecture/requires.txt

This also requires framecpp.  But does not pull that through pip.  FrameCPP should be installed using the native package manager.  The virtualenv setup above will include it.

> apt-get install python3-ldas-tools-framecpp

## Running the nds2-io-node ##

The nds2-io-node needs to have the various component parts specified, this can be done on the command line, or via a remote host.

Here is one example:

> /usr/bin/nds2-io-node  --listen 0.0.0.0 --port 31200 --memcache memcached://127.0.0.1 --framelookup dcache://127.0.0.1:11300 --channelinfo nds2://127.0.0.1:31201 --concurrent 6 --online daq://daqd_stream

This listens on port 31200 of all interfaces.  It contacts a local memcache.  Frame locations are looked up using a local instance of diskcache.  The metadata server is running locally at port 31201.  It can read live IFO data via the daqd_stream server.

## Running Tests ##

The nds2 server has a series of python unittests that can be run, and the requirements file includes the coverage package for code coverage.

These tests do not require network or external services to run, however they do require the ability to create and destroy a temporary directory.

To run tests:

> cd nds2-server

> python -m unittest discover

To get code coverage information

> cd nds2-server

> coverage run -m unittest discover

> coverage html

> firefox htmlcov/index.html

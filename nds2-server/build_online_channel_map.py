# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import argparse
import glob
import json
import os.path
import sys
import typing

import re

try:
    import frameCPP
except ImportError:
    from LDAStools import frameCPP


class UnknownDataType(Exception):
    def __init__(self, msg=""):
        super(UnknownDataType).__init__(msg)


def usage(parser, msg=None):
    parser.print_help()
    if msg is not None:
        print("\n\n" + msg)
    sys.exit(1)


def fcpp_to_dtype(fcpp_type):
    if fcpp_type == frameCPP.FrVect.FR_VECT_1U:
        return "byte_1"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_2S:
        return "int_2"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_4S:
        return "int_4"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_8S:
        return "int_8"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_4R:
        return "real_4"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_8R:
        return "real_8"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_8C:
        return "complex_8"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_4U:
        return "uint_4"
    elif fcpp_type == frameCPP.FrVect.FR_VECT_16C:
        return "complex_16"
    raise UnknownDataType("Unknown type {0}".format(fcpp_type))


def create_channel_map(ch_type, path):
    def no_frames():
        print("For frame type {0} at {1} no frames found/or invalid filenames".format(ch_type, path))
        sys.exit(1)

    fname_re = re.compile("^(.+)-[0-9]+-[0-9]+.gwf$")
    frame_names = glob.glob(os.path.join(path, "*.gwf"))
    if len(frame_names) == 0:
        no_frames()
    frame_names.sort(reverse=True)
    sample_frame = frame_names[0]
    basename = os.path.basename(sample_frame)
    match = fname_re.match(basename)
    if not match:
        no_frames()
    prefix = match.group(1)

    f = frameCPP.IFrameFStream(sample_frame)
    toc = f.GetTOC()
    adc = toc.GetADC()
    proc = toc.GetProc()

    channels = {}

    for ch_entry in adc.keys():

        ch_data = f.ReadFrAdcData(0, ch_entry)
        rate = ch_data.GetSampleRate()
        ch_type = ch_data.RefData()[0].GetType()
        try:
            dtype = fcpp_to_dtype(ch_type)
        except UnknownDataType:
            continue

        channels[ch_entry] = {
            "data_type": dtype,
            "sample_rate": rate,
        }
    for ch_entry in proc:
        ch_data = f.ReadFrProcData(0, ch_entry)
        ref_data = ch_data.RefData()[0]
        ch_type = ref_data.GetType()
        rate = 1.0 / ref_data.GetDim(0).GetDx()
        try:
            dtype = fcpp_to_dtype(ch_type)
        except UnknownDataType:
            continue

        channels[ch_entry] = {
            "data_type": dtype,
            "sample_rate": rate,
        }
    return {
        "location": path,
        "prefix": prefix,
        "channels": channels,
    }


def get_output_file(fname):
    if fname == '-':
        return sys.stdout
    return open(fname, 'wt')


def close_output(f):
    if f != sys.stdout:
        f.close()


def dump_chan_list(chan_list: str, channel_map):
    with open(chan_list, "wt") as f:
        for key in channel_map:
            site = channel_map[key]
            f.write("# {0} ({1}) Channels from {2}\n".format(key, site['prefix'], site['location']))
            for channel_name in site['channels']:
                channel = site['channels'][channel_name]
                f.write("{0} online 0 {1} {2} {3}\n".format(channel_name, channel['data_type'], int(channel['sample_rate']), key))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="""
Build a list of online channels suitable to consumption by the
nds2alt service.  Arguments are colon separated pairs data_type:path data_type:path""")
    parser.add_argument("channel_type", help="Pairs of colon delemited channel_type:path entries",
                        nargs='*')
    parser.add_argument("--allow-empty", action="store_true",
                        help="Treat empty/non-existent directories as empty channel lists.")
    parser.add_argument("-o", "--output", default="-", help="output file, '-' for stdout")
    parser.add_argument("--chan-list", default="", help="output to channel list file, optional")
    args = parser.parse_args()

    channel_map = {}

    for entry in args.channel_type:
        parts = entry.split(":")
        if len(parts) != 2:
            usage(parser)
        ch_type, path = parts
        if ch_type in channel_map:
            usage(parser, "The channel type {0} has already been used.".format(ch_type))
        if not os.path.exists(path) or not os.path.isdir(path):
            if not args.allow_empty:
                usage(parser, "The path component {0} does not exist or is not a directory".format(path))
            else:
                continue
        channel_map[ch_type] = {
            "location": path,
        }

    for ch_type in channel_map:
        path = channel_map[ch_type]["location"]
        channel_map[ch_type] = create_channel_map(ch_type, path)

    out = get_output_file(args.output)
    try:
        out.write(json.dumps(channel_map, indent=2))
        if args.chan_list != "":
            dump_chan_list(args.chan_list, channel_map)
    finally:
        close_output(out)
    sys.exit(0)

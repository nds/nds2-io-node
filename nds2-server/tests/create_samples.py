# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import math
import os
import os.path
import sys

try:
    from LDAStools import frameCPP
except:
    import frameCPP


def _default_slope():
    """Thre is a bug in frameCPP 2.6.10-2.6.11 bindings that will lead to the
    slope being set to 0 in a python generated frame.  So just detect what the
    default is for testing purposes"""
    channel = frameCPP.FrAdcData("X1:SIN1", 0, 5, 16, 16.0)
    return channel.slope


class Range:
    """Taken from the frameCPP test code"""

    def __init__(self, DataType, Start, Stop, Inc):
        self.data_type = DataType
        self.start = Start
        self.stop = Stop
        self.inc = Inc

    def Samples(self):
        retval = int((self.stop - self.start) / self.inc)
        return retval

    type_map = {
        frameCPP.FrVect.FR_VECT_2S: '2S',
        frameCPP.FrVect.FR_VECT_2U: '2U',
        frameCPP.FrVect.FR_VECT_4S: '4S',
        frameCPP.FrVect.FR_VECT_4U: '4U',
        frameCPP.FrVect.FR_VECT_8S: '8S',
        frameCPP.FrVect.FR_VECT_8U: '8U',
        frameCPP.FrVect.FR_VECT_4R: '4R',
        frameCPP.FrVect.FR_VECT_8R: '8R',
        ## \todo add default case of unknown
    }


class SineSignal:
    def __init__(self, scale=1.0, time_multiplier=1.0, offset=0.0):
        self.scale = scale
        self.time_mult = time_multiplier
        self.offset = offset

    def sample(self, t):
        return math.sin(t * self.time_mult) * self.scale + self.offset


def create_ramp_data(Name, Range):
    """Create data for a ramp, taken from frameCPP test code"""
    # --------------------------------------------------------------------
    # Create a new vector object
    # --------------------------------------------------------------------
    samples = Range.Samples()
    sampleRate = 1.0 / samples
    if sampleRate < 0:
        sampleRate = 0.0166667
    dims = frameCPP.Dimension(samples, sampleRate, "UnitX", 0.0)

    vector = frameCPP.FrVect(Name,
                             Range.data_type,
                             1, dims,
                             "UnitY")
    datap = vector.GetDataArray()
    step = 1
    # --------------------------------------------------------------------
    # Ramp the data
    # --------------------------------------------------------------------
    x = Range.start
    n = 0
    while x < Range.stop:
        datap[n] = x
        if step > 1:
            datap[n + 1] = x
        n += step
        x += Range.inc
    return vector


def create_sub_frame_1(frame_start, frame_step, frame_index):
    start = frameCPP.GPSTime(frame_start, 0)
    frame = frameCPP.FrameH()
    frame.SetName("Test")
    frame.SetRun(-1)
    frame.SetGTime(start)
    frame.SetDt(frame_step)

    ch1_start = frame_index * (frame_step * 16.0)
    ch1_end = ch1_start + frame_step * 16.0
    range = Range(frameCPP.FrVect.FR_VECT_4R,
                  ch1_start,
                  ch1_end,
                  1.0)

    name = "X1:PROC-TEST_1"
    channel = frameCPP.FrProcData(name,
                                  "Sample proc data test 1",
                                  frameCPP.FrProcData.TIME_SERIES,
                                  frameCPP.FrProcData.UNKNOWN_SUB_TYPE,
                                  0,  # time offset
                                  frame_step,  # duration of sampled data
                                  0.,  # shift
                                  0,  # phase
                                  0,  # freq range
                                  0,  # BW res
                                  )
    vect = create_ramp_data(name, range)
    channel.AppendData(vect)
    frame.AppendFrProcData(channel)
    return frame


def create_64k_subframe(frame_start, frame_step, frame_index):
    start = frameCPP.GPSTime(frame_start, 0)
    frame = frameCPP.FrameH()
    frame.SetName("Test")
    frame.SetRun(-1)
    frame.SetGTime(start)
    frame.SetDt(frame_step)

    name = "X1:PROC-TEST_64K"
    channel = frameCPP.FrProcData(name,
                                  "Sample proc data test 64k",
                                  frameCPP.FrProcData.TIME_SERIES,
                                  frameCPP.FrProcData.UNKNOWN_SUB_TYPE,
                                  0,  # time offset
                                  frame_step,  # duration of sampled data
                                  0.,  # shift
                                  0,  # phase
                                  0,  # freq range
                                  0,  # BW res
                                  )

    # --------------------------------------------------------------------
    # Create a new vector object
    # --------------------------------------------------------------------
    samples = 64*1024.0*frame_step
    sampleRate = 64*1024.0

    dims = frameCPP.Dimension(int(samples), sampleRate, "UnitX", 0.0)

    vector = frameCPP.FrVect(name,
                             frameCPP.FrVect.FR_VECT_8R,
                             1, dims,
                             "UnitY")
    datap = vector.GetDataArray()
    step = 1
    # --------------------------------------------------------------------
    # Ramp the data
    # --------------------------------------------------------------------
    offset = 0
    for sec in range(frame_step):
        cur_gps = sec + frame_start
        for i in range(64*1024):
            datap[offset] = cur_gps
            offset += 1

    channel.AppendData(vector)
    frame.AppendFrProcData(channel)
    return frame

def create_aggregate_frame(dir, ag_frame_start, ag_frame_duration, ag_frame_step, create_sub_frame=create_sub_frame_1):
    basename = "X-X1_TEST-{0}-{1}.gwf".format(ag_frame_start, ag_frame_duration)
    fullname = os.path.join(dir, basename)
    fs = frameCPP.OFrameFStream(fullname)

    if ag_frame_duration % ag_frame_step != 0:
        raise RuntimeError("frame duration is not a multiple of frame step")

    start = ag_frame_start
    end = start + ag_frame_duration
    step = ag_frame_step
    cur = start
    i = 0
    while cur < end:
        sub_frame = create_sub_frame(cur, step, i)
        fs.WriteFrame(sub_frame, frameCPP.FrVect.RAW, 0)
        cur += step
        i += 1
    return fullname


def create_aggrigate_frame1(dir):
    return create_aggregate_frame(dir, ag_frame_start=1000000000, ag_frame_duration=128, ag_frame_step=4)

def create_aggregate_64k_frame(dir):
    return create_aggregate_frame(dir, ag_frame_start=1100000000, ag_frame_duration=128, ag_frame_step=16, create_sub_frame=create_64k_subframe)


def create_bad_aggrigate_frame2(dir):
    """
    Create a 'bad' frame file to follow immediately after
    the frame created in create_aggrigate_frame1
    :param dir: The directory to create the file in.
    :return: the filename
    """
    basename = "X-X1_TEST-1000000128-128.gwf"
    fullname = os.path.join(dir, basename)
    with open(fullname, "wt") as f:
        f.write("not a frame")
    return fullname


def create_sample_m_trend1(dir):
    def create_sub_frame(frame_start, frame_step, frame_index):
        start = frameCPP.GPSTime(frame_start, 0)
        frame = frameCPP.FrameH()
        frame.SetName("TestMin")
        frame.SetRun(-1)
        frame.SetGTime(start)
        frame.SetDt(frame_step)

        ch1_start = 0.0
        ch1_end = 3600 / 60
        range = Range(frameCPP.FrVect.FR_VECT_8R,
                      ch1_start,
                      ch1_end,
                      1.0)

        name = "X1:PROC-TEST_1.min"
        channel = frameCPP.FrProcData(name,
                                      "Sample proc data test 1",
                                      frameCPP.FrProcData.TIME_SERIES,
                                      frameCPP.FrProcData.UNKNOWN_SUB_TYPE,
                                      0,  # time offset
                                      frame_step,  # duration of sampled data
                                      0,  # shift
                                      0,  # phase
                                      0,  # freq range
                                      0,  # BW res
                                      )
        vect = create_ramp_data(name, range)
        channel.AppendData(vect)
        frame.AppendFrProcData(channel)
        return frame

    basename = "X-X1_TESTM-1000000000-3600.gwf"
    fullname = os.path.join(dir, basename)
    fs = frameCPP.OFrameFStream(fullname)

    start = 1000000000
    step = 3600

    sub_frame = create_sub_frame(start, step, 0)
    fs.WriteFrame(sub_frame, frameCPP.FrVect.RAW, 0)

    return fullname


def create_sample_realtime_frame(dir, start_time, dt=4):
    def create_vect(name, sample_rate, start_time, duration, source):
        samples = int(sample_rate * duration)
        dims = frameCPP.Dimension(samples, sample_rate, "UnitX", 0.0)
        vector = frameCPP.FrVect(name,
                                 frameCPP.FrVect.FR_VECT_4R,
                                 1,
                                 dims,
                                 "samples")
        datap = vector.GetDataArray()
        cur_time = float(start_time)
        time_step = 1.0 / sample_rate
        for i in range(samples):
            datap[i] = source.sample(cur_time)
            cur_time += time_step
        return vector

    def create_sub_frame(frame_start, duration):
        start = frameCPP.GPSTime(frame_start, 0)
        frame = frameCPP.FrameH()
        frame.SetName("TestRT")
        frame.SetRun(-1)
        frame.SetGTime(start)
        frame.SetDt(duration)
        chinfos = (
            ('X1:SIN_1', 'sine1', 1.0),
            ('X1:SIN_2', 'sine2', 10.0),
        )
        chan_index = 0
        for chinfo in chinfos:
            channel = frameCPP.FrAdcData(chinfo[0],
                                         0,
                                         chan_index,
                                         16,
                                         16.0)
            # channel = frameCPP.FrProcData(chinfo[0],
            #                               chinfo[1],
            #                               frameCPP.FrProcData.TIME_SERIES,
            #                               frameCPP.FrProcData.UNKNOWN_SUB_TYPE,
            #                               0,  # time offset
            #                               duration,  # duration of sampled data
            #                               0,  # shift
            #                               0,  # phase
            #                               0,  # freq range
            #                               0,  # BW res
            #                               )
            src = SineSignal(chinfo[2])
            vect = create_vect(chinfo[0], 16.0, frame_start, duration, src)
            channel.AppendData(vect)
            frame.AppendFrAdcData(channel)
            chan_index += 1
        return frame

    basename = "X-X1_realtime-{0}-{1}.gwf".format(start_time, dt)
    fullname = os.path.join(dir, basename)
    fs = frameCPP.OFrameFStream(fullname)
    sub_frame = create_sub_frame(start_time, dt)
    fs.WriteFrame(sub_frame, frameCPP.FrVect.RAW, 0)
    return fullname


if __name__ == "__main__":
    #
    # If you run this it will run and continually update a cache directory with sample frames.
    # Entries will come in in a regular cadence and be cleaned up in a regular cadence.
    #
    import time
    import glob

    from .tempdir import TempDir


    def sync_to_dt_w_delay():
        while True:
            gps.Now()
            now = gps.GetSeconds() - delay
            if now % dt != 0:
                time.sleep(.25)
            else:
                return now


    def sleep_until_w_delay(target):
        while True:
            gps.Now()
            now = gps.GetSeconds() - delay
            if now < target:
                time.sleep(.25)
            else:
                return now


    delay = 4
    dt = 4
    gps = frameCPP.GPSTime()
    gps.Now()

    cache_dir = sys.argv[1]

    glob_mask = os.path.join(cache_dir, "*.gwf")

    with TempDir(cache_dir) as tmp_dir:
        start_time = sync_to_dt_w_delay()

        print("Starting run at {0}".format(start_time))

        while True:
            fname = create_sample_realtime_frame(tmp_dir.get(), start_time, dt)
            # print("Created frame as " + fname)
            target_fname = os.path.join(cache_dir, os.path.basename(fname))
            os.rename(fname, target_fname)
            # print("created " + target_fname)

            frame_list = glob.glob(glob_mask)
            frame_list.sort(reverse=True)

            while len(frame_list) > 40:
                cull = frame_list.pop()
                os.unlink(cull)
                # print("removed " + cull)

            start_time += dt
            sleep_until_w_delay(start_time)

# fs = frameCPP.IFrameFStream('X-X1_TEST-1000000000-128.gwf')
# for i in range(fs.GetNumberOfFrames()):
#     fr_cur = fs.ReadFrameN(i)
#     print ("Frame #{0} GPS {1}:{2}".format(i, fr_cur.GetGTime()[0], fr_cur.GetDt()))
#     proc_data = fs.ReadFrProcData(i, 'X1:PROC-TEST_1')
#     print (proc_data.data[0].GetDataArray())

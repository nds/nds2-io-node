# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import bisect

from . import signal_units
from .errors import DAQDFrameIOError
from .errors import DAQDFrameErrorChannelNotFound
from .helpers import bytes_to_samples
from .helpers import samples_to_bytes
from .helpers import samples_to_seconds
from .helpers import seconds_to_samples


def cache_key(frame_type, frame_start_gps, channel, offset):
    """
    Generate a cache key
    :param frame_type: Type of frame as a string
    :param frame_start_gps: GPS for the start of the frame
    :param channel: Channel (dict containing at lest "name")
    :param offset:  time offset into the frame
    :return: a string (possibly binary) for the cache entry
    """
    key = "{0},{1},{2},{3}".format(
        frame_type,
        frame_start_gps,
        channel["channel_name"],
        offset,
    )
    return key


def save_block_to_cache(frame_type,
                        frame_start_gps,
                        channel,
                        max_block_size,
                        block,
                        block_start_gps,
                        units,
                        cache):
    """
    Given a block of samples from a frame,
    push as much of it into the cache as possible.
    :param frame_type: The frame type
    :param frame_start_gps: The start of the frame in GPS seconds
    :param channel: The channel whos data is in the input block
    :param max_block_size: The block size of the cache for this
    channel in seconds
    :param block: The channel data block
    :param block_start_gps: The gps second of the first
    sample in the block
    :param units a signal_units block associated w/ this data block
    :param cache: the cache
    :return: A tuple (offset, residual data), which will
    be (0, "") if no data was left over
    """
    samples_in_block = seconds_to_samples(channel, max_block_size)
    bytes_in_block = samples_to_bytes(channel, samples_in_block)

    if len(block) < bytes_in_block:
        return block_start_gps, block

    offset_gps = block_start_gps - frame_start_gps
    if offset_gps % max_block_size != 0:
        skip = max_block_size - (offset_gps % max_block_size)
        skip_samples = seconds_to_samples(channel, skip)
        skip_bytes = samples_to_bytes(channel, skip_samples)
        block = block[skip_bytes:]
        block_start_gps += skip

    unit_blob = signal_units.encode(units)
    while len(block) >= bytes_in_block:
        key = cache_key(frame_type,
                        frame_start_gps,
                        channel,
                        block_start_gps - frame_start_gps)
        cache.set(key, unit_blob + block[:bytes_in_block])
        block = block[bytes_in_block:]
        block_start_gps += max_block_size
    return block_start_gps, block


def read_from_frame(fs, frame_type, chan, frame_start_gps, offset, cache_stride, cache, frame_toc=None):
    """
    Read from a frame file into the cache, starting from offset offset.
    This will read the contents of whatever subframe holds given offset
    into the cache
    :param fs: frame file object
    :param frame_type: The frame type
    :param chan: channel to read
    :param frame_start_gps: start gps time of the frame
    :param offset: start time in seconds relative to start of the frame
    :param cache_stride: max number of seconds in a cache block
    :param cache: cache object
    :param frame_toc: table of contents for the frame, if None read from frame
    :return: The data associated with the requested cache key or None
    """
    if frame_toc is None:
        frame_toc = fs.GetTOC()
    frame_count = fs.GetNumberOfFrames()
    frame_gps = frame_toc.GetGTimeS()

    skip_cache_strides = offset // cache_stride
    cache_start_offset_gps = skip_cache_strides * cache_stride
    frame_index = 0

    start_gps = frame_start_gps + cache_start_offset_gps
    can_finish = False

    _cache_data = ""
    _cache_gps = 0

    have_written_something = False
    subframes_processed = 0

    samples_in_cache_stride = seconds_to_samples(chan, cache_stride)
    bytes_in_cache_stride = samples_to_bytes(
        chan, samples_in_cache_stride)

    # find our start sub-frame
    frame_index = max(0, bisect.bisect_right(frame_gps, start_gps) - 1)
    fr_cur_gps = frame_gps[frame_index]

    units = signal_units.default()
    while not can_finish:
        subframes_processed += 1
        if frame_index >= frame_count:
            # are always finished if we run out of
            # sub frames
            if len(_cache_data) > 0:
                # if we have partial data, store it
                _cache_key = cache_key(frame_type,
                                       frame_start_gps,
                                       chan,
                                       _cache_gps - frame_start_gps)
                cache.set(_cache_key, signal_units.encode(units) + _cache_data)
            return

        try:
            try:
                frame_data = fs.ReadFrAdcData(
                    frame_index, chan['channel_name'])
                #units = frame_data.GetUnits()
                units = signal_units.signal_units(
                    frame_data.bias,
                    frame_data.slope,
                    frame_data.data[0].unitY
                )
            except IndexError:
                frame_data = fs.ReadFrProcData(
                    frame_index, chan['channel_name'])
                units = signal_units.signal_units(
                    0.0,
                    1.0,
                    frame_data.data[0].unitY
                )
        except IndexError:
            raise DAQDFrameErrorChannelNotFound("Requested channel '{0}' not found in the frame".format(chan['channel_name']))
        except:
            raise DAQDFrameIOError(
                "Error reading '{0}' from the frame"
                    .format(chan['channel_name']))
        raw_data = frame_data.data[0].GetDataArray()
        raw_data.byteswap(True)
        raw_data = raw_data.tostring('C')

        if len(_cache_data) > 0:
            # we have residual data from a previous iteration
            # we need to add in data from the current block
            max_needed = bytes_in_cache_stride - len(_cache_data)
            if max_needed > len(raw_data):
                max_needed = len(raw_data)
            _cache_data += raw_data[:max_needed]
            raw_data = raw_data[max_needed:]
            needed_s = samples_to_seconds(
                chan, bytes_to_samples(chan, max_needed))
            fr_cur_gps += needed_s
            # fr_cur_dt -= needed_s

            # if we can, push this into a block
            if len(_cache_data) >= bytes_in_cache_stride:
                _cache_gps, _cache_data = save_block_to_cache(
                    frame_type,
                    frame_start_gps,
                    chan,
                    cache_stride,
                    _cache_data,
                    _cache_gps,
                    units,
                    cache)
                have_written_something = True
        if len(raw_data) > 0:
            _cache_gps, _cache_data = save_block_to_cache(
                frame_type,
                frame_start_gps,
                chan,
                cache_stride,
                raw_data,
                fr_cur_gps,
                units,
                cache)
            if len(_cache_data) != len(raw_data):
                have_written_something = True
        # we have pushed all the data from this sub-frame
        #  into the cache, nothing else to do
        if len(_cache_data) == 0:
            return

        can_finish = subframes_processed > 0 and have_written_something
        frame_index += 1
        if frame_index < frame_count:
            fr_cur_gps = frame_gps[frame_index]

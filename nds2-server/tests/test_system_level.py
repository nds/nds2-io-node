# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import json
import struct
from binascii import hexlify
from unittest import TestCase

from .. import daqd
from .. import errors
from .. import nds2_server
from ..epochs import default_epoch
from ..chan_info import register_chan_info
from ..concurrent import concurrent_factory
from ..disk_cache import register_frame_lookup
from ..memory_cache import register_memory_cache_lookup
from ..online_interface import register_online
from ..stats import StatsIgnoreHandler
from ..remote_proxy import register_remote_proxy_lookup

if not 'NDS2ServerLogic' in dir(nds2_server):
    del nds2_server
    import nds2_server.nds2_server as nds2_server

from . import dummy_chan_info
from . import dummy_disk_cache
from . import dummy_cache
from . import dummy_online
from . import dummy_remote_proxy
from .. import signal_units
from .create_samples import _default_slope
from .create_samples import create_sample_realtime_frame
from .create_samples import create_aggrigate_frame1
from .create_samples import create_bad_aggrigate_frame2
from .create_samples import SineSignal
from .dummy_socket import DummySocket
from .tempdir import TempDir
from ..helpers import seconds_to_samples


# FIXME: wire up the remote proxy
class DummyServer:
    def __init__(self, chan_info, frame_lookup, memory_cache, online, stats_server_uri="", reader_socket="", remote_proxy=None):
        register_chan_info(chan_info.get_uri(), chan_info)
        register_frame_lookup(frame_lookup.get_uri(), frame_lookup)
        register_memory_cache_lookup(memory_cache.get_uri(), memory_cache)
        register_online(online.get_uri(), online)
        if remote_proxy is not None:
            register_remote_proxy_lookup(remote_proxy.get_uri(), remote_proxy)
        self.chan_info_uri = chan_info.get_uri()
        self.frame_lookup_uri = frame_lookup.get_uri()
        self.memory_cache_uri = memory_cache.get_uri()
        self.stats_server_uri = stats_server_uri
        self.online_uri = online.get_uri()
        self.reader_socket = reader_socket
        self.concurrent = concurrent_factory(None)
        self.remote_proxy_uri = remote_proxy.get_uri()
        self.epochs = default_epoch()
        self.high_latency_path = ""


def setup_server(stream, chan_info=None, frame_lookup=None, memory_cache=None, stats_obj=None, online=None,
                 reader_socket="", epochs=None):
    """
    Create a NDS2ServerLogic object with the given configuration
    :param stream: The socket/stream object to do I/O over
    :param chan_info: A Channel Info server object
    :param frame_lookup: A frame lookup/disk cache server object
    :param memory_cache: A cache objects
    :param stats_obj: A stats object
    :param online: An OnlineFiles object
    :param reader_socket: path to the reader socket
    :return: The output stream, without the response to the
             initial implied authorize command
    """

    if chan_info is None:
        chan_info = dummy_chan_info.DummyChanInfo()
    if frame_lookup is None:
        frame_lookup = dummy_disk_cache.DummyDiskCacheClient()
    if memory_cache is None:
        memory_cache = dummy_cache.DummyCache()
    if stats_obj is None:
        stats_obj = StatsIgnoreHandler()
    if epochs is None:
        epochs = default_epoch()
    return nds2_server.NDS2ServerLogic(stream,
                                       chan_info_obj=chan_info,
                                       frame_lookup_obj=frame_lookup,
                                       mem_cache_obj=memory_cache,
                                       stats_obj=stats_obj,
                                       online_obj=online,
                                       reader_socket=reader_socket,
                                       epochs=epochs
                                       )


def run_server(input, chan_info=None, frame_lookup=None,
               memory_cache=None, online=None, remote_proxy=None):
    """
    Run the given command stream against an NDS2 server
    :param input: Command stream w/o the leading authorize and
                  trailing 'quit;' commands.
                  Commands should be delimited by ';\n'
    :param chan_info: A Channel Info server object
    :param frame_lookup: A frame lookup/disk cache server object
    :param memory_cache: A cache objects
    :return: The output stream, without the response to the
             initial implied authorize command
    """
    import os.path
    import threading
    from ..reader.frame_reader import FrameReaderHandler
    from ..reader.frame_reader import FrameReaderServer

    lock = threading.Lock()
    done = False
    reader_server = None

    def reader_loop():
        is_done = False
        while not is_done:
            reader_server.handle_request()
            lock.acquire()
            is_done = done
            lock.release()

    cmd_stream = "authorize\n" + input + "quit;\n"

    if chan_info is None:
        chan_info = dummy_chan_info.DummyChanInfo()
    if frame_lookup is None:
        frame_lookup = dummy_disk_cache.DummyDiskCacheClient()
    if memory_cache is None:
        memory_cache = dummy_cache.DummyCache()
    if online is None:
        online = dummy_online.DummyOnline()
    if remote_proxy is None:
        remote_proxy = dummy_remote_proxy.DummyRemoteProxy()

    with TempDir() as tmp:
        socket_path = os.path.join(tmp.get(), "frame_io_socket")
        reader_server = FrameReaderServer(socket_path, FrameReaderHandler)
        reader_server.setup_sync_params(concurrent_jobs=4, cache_uri=memory_cache.get_uri())

        reader_thread = threading.Thread(target=reader_loop)
        reader_thread.daemon = True
        reader_thread.start()

        # reader_socket = socket_path
        srv_obj = DummyServer(chan_info, frame_lookup, memory_cache, online, reader_socket="", remote_proxy=remote_proxy)
        srv_req = DummySocket(cmd_stream.encode())

        nds2_server.NDS2Handler(srv_req, (), srv_obj)
        out = srv_req.out_buffer()

        with lock:
            done = True

        if out[0:4] != b'0000':
            raise Exception("Authorize failed in test")
        return out[4:]


class TestNDS2Handler(TestCase):
    def test_handle_list_epochs(self):
        out = run_server("list-epochs;\n")
        expects = ""
        expects += "ALL=0-1999999999 "
        expects += "ER10=1163173888-1164554240 "
        expects += "ER14=1235746816-1238163456 "
        expects += "ER2=1025636416-1028563232 "
        expects += "ER3=1042934416-1045353616 "
        expects += "ER4=1057881616-1061856016 "
        expects += "ER5=1073606416-1078790416 "
        expects += "ER6=1102089216-1102863616 "
        expects += "ER7=1116700672-1116800000 "
        expects += "ER8=1123856384-1123900032 "
        expects += "ER9=1151848448-1152169216 "
        expects += "NONE=0-0 "
        expects += "O1=1126621184-1137254400 "
        expects += "O2=1164554240-1188081664 "
        expects += "O3=1238163456-1269616640 "
        expects += "S5=815153408-880920032 "
        expects += "S6=930960015-971654415 "
        expects += "S6a=930960015-935798415 "
        expects += "S6b=937785615-947203215 "
        expects += "S6c=947635215-961545615 "
        expects += "S6d=961545615-971654415"
        expects = b"0000" + struct.pack("!I", len(expects)) + expects.encode()
        self.assertEqual(hexlify(expects), hexlify(out))

    def test_handle_set_epoch_resp_code(self):
        out = run_server("""set-epoch 0-1000;
set-epoch ER2;
set-epoch 1000000000:3600;
set-epoch nonexistant;
set-epoch 1000000000:-5;
set-epoch 1000000000-900000000;
""")
        exp = daqd.DAQD_OK * 3 + daqd.DAQD_COMMAND_SYNTAX * 3
        self.assertEqual(exp, out)

    def test_handle_set_epoch_internal_state(self):
        from ..errors import DAQDSyntaxError
        samples = (
            ("0-1000", 0, 1000, True),
            ("ER2", 1025636416, 1028563232, True),
            ("1000000000:3600", 1000000000, 1000003600, True),
            ("nonexistant", 0, 1999999999, False),
            ("1000000000:-5", 0, 1999999999, False),
            ("1000000000-900000000", 0, 1999999999, False),
        )
        for sample in samples:
            s = DummySocket("")
            nds2 = setup_server(s)
            if sample[3]:
                nds2.handle_set_epoch(sample[0])
            else:
                self.assertRaises(DAQDSyntaxError, nds2.handle_set_epoch, sample[0])
            self.assertEqual(nds2.epoch_start, sample[1])
            self.assertEqual(nds2.epoch_stop, sample[2])

    def test_handle_get_source_data(self):
        expects = b"H1:PSL-PWR_HPL_DC_OUT_DQ { H-H1_R:1112572800-1112572900}"
        expects = b"0000" + struct.pack("!I", len(expects)) + expects
        info = dummy_chan_info.DummyChanInfo(avail_proxy=[
            expects,
        ],
        )
        out = run_server(
            "get-source-data 1112572800 {H1:PSL-PWR_HPL_DC_OUT_DQ};\n",
            info
        )

        self.assertEqual(expects, out)

    def test_handle_get_channels(self):
        info = dummy_chan_info.DummyChanInfo(channel_proxy=["proxied", "proxied info", "still proxied", "really proxied", "static"])
        out = run_server("""get-channels 0;
get-channels 0 unknown;
get-channels 0 raw;
get-channels 0 test-pt;
get-channels 0 static;
""", info)
        expected = b"proxiedproxied infostill proxiedreally proxiedstatic"
        self.assertEqual(expected, out)

    def test_handle_count_channels(self):
        counts = [55, 55, 42, 1, 5, 6]
        info = dummy_chan_info.DummyChanInfo(counts=counts)
        out = run_server("""count-channels 0;
count-channels 0 unknown;
count-channels 0 raw {*PEM*};
count-channels 0 reduced {H1:GDS-CALIB_STRAIN};
count-channels 0 test-pt {H1:GRD*};
count-channels 0 static {X1:*};
""", info)
        expects = b""
        for entry in counts:
            expects += daqd.DAQD_OK + struct.pack("!I", entry)
        self.assertEqual(expects, out)

    def test_handle_get_channel_crc(self):
        info = dummy_chan_info.DummyChanInfo(channel_crc=("1234",))
        out = run_server("""get-channel-crc;
""", chan_info=info)
        self.assertEqual(b"00001234", out)

    def test_handle_get_channel_crc_bad(self):
        info = dummy_chan_info.DummyChanInfo(channel_crc=("123",))
        out = run_server("""get-channel-crc;
""", chan_info=info)
        self.assertEqual(b"0001", out)

    def test_handle_get_source_list(self):
        source_proxy = b"proxy value"
        source_proxy = b"0000" + struct.pack("!I", len(source_proxy)) + source_proxy
        info = dummy_chan_info.DummyChanInfo(source_proxy=(source_proxy,))
        out = run_server("""get-source-list 0 {SOME_CHANNEL};
""", chan_info=info)
        self.assertEqual(out, source_proxy)


class TestServerProtocolRevision(TestCase):
    def test_protocol_ver_5(self):
        cmd = "server-protocol-revision;\n"
        out = run_server(cmd)
        self.assertEqual(out, b"0000" + struct.pack("!I", 5))

    def test_protocol_ver_6(self):
        cmd = "server-protocol-revision 6;\n"
        out = run_server(cmd)
        self.assertEqual(out, b"0000" + struct.pack("!I", 6))

    def test_protocol_ver_4_invalid(self):
        cmd = "server-protocol-revision 4;\n"
        out = run_server(cmd)
        self.assertEqual(out, b"0019")

    def test_protocol_ver_7_invalid(self):
        cmd = "server-protocol-revision 7;\n"
        out = run_server(cmd)
        self.assertEqual(out, b"0019")


class TestGetDataFromFrame(TestCase):

    def read_header(self, byte_stream):
        header_len = struct.unpack(">i", byte_stream[0:4])[0]
        byte_stream = byte_stream[4:]
        header_bytes = byte_stream[0:header_len]
        return json.loads(header_bytes.decode('utf8')), byte_stream[header_len:]

    def read_channel_blob(self, byte_stream):
        header_len = struct.unpack(">i", byte_stream[0:4])[0]
        byte_stream = byte_stream[4:]
        header_bytes = byte_stream[0:header_len]
        byte_stream = byte_stream[header_len:]
        header = json.loads(header_bytes.decode('utf8'))
        return header, byte_stream[0:header["d"]], byte_stream[header["d"]:]

    def do_test(self, paths):
        disk_cache = dummy_disk_cache.DummyDiskCacheClient(filenames=(paths,))
        cmd = "__get-frame-data X-X1_realtime 1000000004 1000000020 { X1:BAD,real_4,raw,16 X1:SIN_1,real_4,raw," \
              "16 X1:SIN_2,real_4,raw,16};\n"
        out = run_server(cmd, frame_lookup=disk_cache)
        daq_code = out[0:4]
        out = out[4:]
        self.assertEqual(daq_code, b"0000")

        expected_headers = [
            {
                "start": 1000000004,
                "stop": 1000000008,
                "channels": 3,
            },
            {
                "start": 1000000016,
                "stop": 1000000020,
                "channels": 3,
            },
        ]
        actual_headers = []
        while True:
            header, out = self.read_header(out)
            if header["channels"] == 0:
                break
            actual_headers.append(header)

            for i in range(header["channels"]):
                chan_header, data, out = self.read_channel_blob(out)
                print(chan_header)
                if i in (1, 2):
                    time_sec = header["stop"] - header["start"]
                    self.assertEqual(chan_header["s"], 0, "Require status 0 on the channel")
                    expected_data_size = 16*4*time_sec
                    self.assertEqual(expected_data_size, chan_header["d"], "The size of the channel data must be 16*4*span size")
                    samples=struct.unpack(">" + "f"*16*time_sec, data)
                    multipliers = [0, 1, 10]
                    generator = SineSignal(multipliers[i])
                    expected = [generator.sample(float(t)/16.0) for t in range(header["start"]*16,header["stop"]*16)]
                    print(tuple(expected))
                    print(samples)
                    for entry in zip(expected,samples):
                        self.assertAlmostEqual(entry[0], entry[1], places=5, msg="The data should have the right values")
                else:
                    self.assertEqual(chan_header["s"], 2, "Require status 2 on the first channel")
                    self.assertEqual(chan_header["d"], 0, "Data length must be 0 on the first channel")
                    self.assertEqual(data, b"", "The data blob must be empty on the first channel")
                self.assertEqual(chan_header["i"], i, "The indexes must be in order")

        self.assertEqual(expected_headers, actual_headers, "The frame headers must match")
        print(out)

    def test_get_data_from_frame(self):
        with TempDir() as tmp:
            dt = 8
            frame1 = create_sample_realtime_frame(tmp.get(), 1000000000, dt=dt)
            frame2 = create_sample_realtime_frame(tmp.get(), 1000000000 + dt * 2, dt=dt)
            self.do_test((frame1, frame2))


class TestGetRemoteData(TestCase):
    def test_get_remote_data(self):
        gps_1 = {
                'channel_name': 'X1:GPS_1',
                'channel_type': 'raw',
                'data_type': 'real_4',
                'full_name': 'X1:GPS_1,raw,16',
                'rate': 16,
            }

        info = dummy_chan_info.DummyChanInfo(
            channels=([gps_1,],),
            avails=(
                {
                "X1:GPS_1,raw,16": [
                    {
                        "frame_type": "(L1)X-X1_TEST",
                        "start": 1000000127,
                        "stop": 1000000129,
                    },
                ],
                },
            ),
        )
        raw_data = []
        for i in range(0, 16):
            raw_data.append(1000000127)
        for i in range(0, 16):
            raw_data.append(1000000128)
        remote = dummy_remote_proxy.DummyRemoteProxy(
            data_sets = [
                dummy_remote_proxy.RemoteProxyData("(L1)X-X1_TEST", gps_1, 1000000127, 1000000129, struct.pack("32f", *raw_data), 32, signal_units.signal_units(0.0, 1.0, "counts"))
            ]
        )
        out = run_server("get-data 1000000127 1000000129 2 {X1:GPS_1};\n", info, remote_proxy=remote)
        header = struct.pack("!IIIII", (2*16*4)+16, 2, 1000000127, 0, 0)
        ch_header = struct.pack("!LIII",  0xffffffff, 1, 2, 3)
        ch_header += struct.pack("!IIHHfff",
                                 2*16*4,
                                 0,
                                 2,
                                 4,
                                 16,
                                 0,
                                 1.0)
        ch_header = struct.pack("!I", len(ch_header)) + ch_header
        expected = b"0000" + b"00000001" + struct.pack("!I", 1) + ch_header + header + struct.pack("32f", *raw_data)
        print("expecting: {0}".format(expected))
        print("got:       {0}".format(out[:len(expected)]))
        self.assertEqual(out[:len(expected)], expected)
        out = out[len(expected):]
        print(out)


class TestGetData(TestCase):
    def test_get_data_aggrigate_1(self):
        with TempDir() as tmp:
            frame1 = create_aggrigate_frame1(tmp.get())
            frame2 = create_bad_aggrigate_frame2(tmp.get())
            self.do_test_get_data1(frame1, working_cache=True)
            self.do_test_get_data1(frame1, working_cache=False)
            self.do_test_get_data_mid_aggrigate(frame1)
            self.do_test_get_data_mid_aggrigate(frame1, expect_units=True)
            self.do_test_check_read_error(frame1, frame2)
            self.do_test_get_data_large_channel_list()

    def do_test_get_data_large_channel_list(self):
        chans = ["X1:CHANNEL_{0}".format(x) for x in range(600)]
        cmd = "get-data 999999999 1000000001 {" + " ".join(chans) + "};\n"
        out = run_server(cmd)
        self.assertEqual(out, b"0019")

    def do_test_check_read_error(self, fname1, fname2):
        info = dummy_chan_info.DummyChanInfo(
            channels=([
                          {
                              'channel_name': 'X1:PROC-TEST_1',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_1,reduced,16',
                              'rate': 16.0,
                          },
                      ],),
            avails=(
                {
                    "X1:PROC-TEST_1,reduced,16": [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000127,
                            "stop": 1000000129,
                        },
                    ],
                },
            ),
        )
        lookup = dummy_disk_cache.DummyDiskCacheClient(
            (
                (fname1, fname2),
                (fname2),
            ),
        )
        out = run_server("""get-data 1000000127 1000000129 2 {X1:PROC-TEST_1};
""", info, lookup)
        expected = b"0000" + b"00000001" + struct.pack("!I", 1)
        expected += struct.pack("!I", 16)
        expected += struct.pack("!IIII", 2, 1000000127, 0, 0)
        self.assertEqual(out, expected)

    def do_test_get_data_mid_aggrigate(self, fname, expect_units=False):
        info = dummy_chan_info.DummyChanInfo(
            channels=([
                          {
                              'channel_name': 'X1:PROC-TEST_1',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_1,reduced,16',
                              'rate': 16.0,
                          },
                      ],),
            avails=(
                {
                    "X1:PROC-TEST_1,reduced,16": [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000014,
                            "stop": 1000000015,
                        },
                    ],
                },
            ),
        )
        lookup = dummy_disk_cache.DummyDiskCacheClient(
            (
                (fname,),
            ),
        )
        test_mem_cache = dummy_cache.DummyCacheTracking(max_block_size=16 * 2 * 8)
        cmd = "get-data 1000000014 1000000015 1 {X1:PROC-TEST_1};\n"
        if expect_units:
            cmd = "server-protocol-revision 6;\n" + cmd
        out = run_server(cmd, info, lookup, memory_cache=test_mem_cache)
        self.assertEqual(out[0:4], b"0000")
        self.assertListEqual(test_mem_cache.sets,
                             ['X-X1_TEST,1000000000,X1:PROC-TEST_1,12'])
        expected = b""
        header_block_size = 40
        if expect_units:
            expected += b"0000" + struct.pack("!I", 6)
            header_block_size = 49
        # DAQD_OK + writer id + 1 for offline
        expected += b"0000" + b"00000001" + struct.pack("!I", 1)
        # header block (reconfig block)
        expected += struct.pack("!IIIII", header_block_size, 0xffffffff, 1, 2, 3)
        # for each channel
        # status, data offset, data type, rate, offset, slope
        # status is length
        # data type is chan_type << 16 | data_type
        expected += struct.pack("!IIHHfff", 16 * 4, 0, 3, 4, 16.0, 0.0, 1.0)
        if expect_units:
            expected += struct.pack("!I5s", 5, b"UnitY")
        # header block
        expected += struct.pack("!IIIII", 80, 1, 1000000014, 0, 0)
        for i in range(16):
            expected += struct.pack("!f", float((16 * 14) + i))
        self.assertEqual(hexlify(out), hexlify(expected))

    def do_test_get_data1(self, fname, working_cache):
        info = dummy_chan_info.DummyChanInfo(
            channels=([
                          {
                              'channel_name': 'X1:PROC-TEST_1',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_1,reduced,16',
                              'rate': 16.0,
                          }
                      ], [
                          {
                              'channel_name': 'X1:PROC-TEST_1',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_1,reduced,16',
                              'rate': 16.0,
                          }
                      ],),
            channels_args=(
                (["X1:PROC-TEST_1"],
                 1000000000,
                 1000000001,
                 "unknown"),
            ),
            avails=(
                {
                    "X1:PROC-TEST_1,reduced,16": [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000000,
                            "stop": 1000000001,
                        }
                    ]
                },
                {
                    "X1:PROC-TEST_1,reduced,16": [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000001,
                            "stop": 1000000002,
                        }
                    ]
                },
            )
        )
        lookup = dummy_disk_cache.DummyDiskCacheClient(
            (
                (fname,),
                (fname,),
            ),
        )
        test_mem_cache = dummy_cache.DummyCache
        if not working_cache:
            test_mem_cache = dummy_cache.DummyCacheLikeaSeive
        out = run_server("""get-data 1000000000 1000000001 1 {X1:PROC-TEST_1};
""", info, lookup, memory_cache=test_mem_cache())
        # DAQD_OK + writer id + 1 for offline
        expected = b"0000" + b"00000001" + struct.pack("!I", 1)
        # header block (reconfig block)
        expected += struct.pack("!IIIII", 40, 0xffffffff, 1, 2, 3)
        # for each channel
        # status, data offset, data type, rate, offset, slope
        # status is length
        # data type is chan_type << 16 | data_type
        expected += struct.pack("!IIHHfff", 16 * 4, 0, 3, 4, 16.0, 0.0, 1.0)
        # header block
        expected += struct.pack("!IIIII", 80, 1, 1000000000, 0, 0)
        for i in range(16):
            expected += struct.pack("!f", float(i))
        self.assertEqual(hexlify(out), hexlify(expected))
        out = run_server("""get-data 1000000001 1000000002 1 {X1:PROC-TEST_1};
            """, info, lookup)
        # DAQD_OK + writer id + 1 for offline
        expected = b"0000" + b"00000001" + struct.pack("!I", 1)
        # header block (reconfig block)
        expected += struct.pack("!IIIII", 40, 0xffffffff, 1, 2, 3)
        # for each channel
        # status, data offset, data type, rate, offset, slope
        expected += struct.pack("!IIHHfff", 16 * 4, 0, 3, 4, 16.0, 0.0, 1.0)
        # header block
        expected += struct.pack("!IIIII", 80, 1, 1000000001, 0, 0)
        for i in range(16):
            expected += struct.pack("!f", float(i + 16))
        self.assertEqual(out, expected)


class TestGetOnlineData(TestCase):
    def test_get_online_data_no_online_data_available(self):
        out = run_server("""get-online-data { X1:SIN_1 };
""")
        self.assertEqual(out[0:4], b"000d")

    def test_get_online_data(self):
        from .create_samples import create_sample_realtime_frame
        from .create_samples import SineSignal

        def run_test(command, use_unicode=False, expect_units=False):

            def consume(data, size):
                return data[0:size], data[size:]

            def seed_files(ignored=None):
                create_sample_realtime_frame(tmp.get(), params['cur_gps_time'], params['time_step'])
                params['cur_gps_time'] += params['time_step']

            def generate_sample_span(signal_generator, t, delta, steps):
                span = []
                for x in range(steps):
                    sample = signal_generator.sample(t)
                    if type(sample) is float:
                        sample = struct.unpack("!f", struct.pack("!f", sample))[0]
                    span.append(sample)
                    t += delta
                return tuple(span)

            if expect_units:
                command = "server-protocol-revision 6;\n" + command

            sin1_gen = SineSignal()
            with TempDir() as tmp:
                if use_unicode:
                    online = dummy_online.create_online_object(tmp.get(), sleep=seed_files)
                else:
                    online = dummy_online.create_online_object_unicode(tmp.get(), sleep=seed_files)
                params = {'cur_gps_time': 1000000000, 'time_step': 4}
                out = run_server(command, online=online)
                if expect_units:
                    status, out = consume(out, 4)
                    self.assertEqual(status, b"0000")
                    proto_rev, out = consume(out, 4)
                    self.assertEqual(struct.unpack("!I", proto_rev)[0], 6)

                status, out = consume(out, 4)
                self.assertEqual(status, b"0000")
                writter_id, out = consume(out, 8)
                self.assertEqual(writter_id, b"00000001")
                online_word, out = consume(out, 4)
                self.assertEqual(online_word, struct.pack("!I", 0))

                header_block_len = 40
                if expect_units:
                    header_block_len += 11
                block_header, out = consume(out, 5 * 4)
                self.assertEqual(hexlify(block_header),
                                 hexlify(struct.pack("!IIIII", header_block_len, 0xffffffff, 1, 2, 3)))
                if not expect_units:
                    chan_info_block, out = consume(out, 6 * 4)
                    expected = struct.pack("!IIHHfff", 16 * 1 * 4, 0, 1, 4, 16.0, 0.0, _default_slope())
                else:
                    chan_info_block, out = consume(out, 6 * 4 + 11)
                    expected = struct.pack("!IIHHfffI7s", 16 * 1 * 4, 0, 1, 4, 16.0, 0.0, _default_slope(), 7, b"samples")
                self.assertEqual(hexlify(chan_info_block), hexlify(expected))

                block_header, out = consume(out, 5 * 4)
                self.assertEqual(block_header, struct.pack("!IIIII", 16 * 1 * 4 + 16, 1, 1000000000, 0, 0))
                data, out = consume(out, 16 * 4)
                actual_data = struct.unpack("!" + "f" * 16, data)
                expected_data = generate_sample_span(sin1_gen, 1000000000.0, 1.0 / 16.0, 16)
                self.assertTupleEqual(actual_data, expected_data)

                block_header, out = consume(out, 5 * 4)
                self.assertEqual(block_header, struct.pack("!IIIII", 16 * 1 * 4 + 16, 1, 1000000001, 0, 1))
                data, out = consume(out, 16 * 4)
                expected_data = generate_sample_span(sin1_gen, 1000000001.0, 1.0 / 16.0, 16)
                self.assertTupleEqual(struct.unpack("!" + "f" * 16, data), expected_data)

                block_header, out = consume(out, 5 * 4)
                self.assertEqual(block_header, struct.pack("!IIIII", 16 * 1 * 4 + 16, 1, 1000000002, 0, 2))
                data, out = consume(out, 16 * 4)
                expected_data = generate_sample_span(sin1_gen, 1000000002.0, 1.0 / 16.0, 16)
                self.assertTupleEqual(struct.unpack("!" + "f" * 16, data), expected_data)

                block_header, out = consume(out, 5 * 4)
                self.assertEqual(block_header, struct.pack("!IIIII", 16 * 1 * 4 + 16, 1, 1000000003, 0, 3))
                data, out = consume(out, 16 * 4)
                expected_data = generate_sample_span(sin1_gen, 1000000003.0, 1.0 / 16.0, 16)
                self.assertTupleEqual(struct.unpack("!" + "f" * 16, data), expected_data)

                block_header, out = consume(out, 5 * 4)
                self.assertEqual(block_header, struct.pack("!IIIII", 16 * 1 * 4 + 16, 1, 1000000004, 0, 4))
                data, out = consume(out, 16 * 4)
                expected_data = generate_sample_span(sin1_gen, 1000000004.0, 1.0 / 16.0, 16)
                self.assertTupleEqual(struct.unpack("!" + "f" * 16, data), expected_data)

                self.assertEqual(out, b"")

        run_test("get-online-data 5 1 { X1:SIN_1 };\n")
        run_test("get-data 0 5 1 { X1:SIN_1 };\n")
        run_test("get-online-data 5 1 { X1:SIN_1 };\n", use_unicode=True)
        run_test("get-data 0 5 1 { X1:SIN_1 };\n", use_unicode=True)

        run_test("get-online-data 5 1 { X1:SIN_1 };\n", expect_units=True)
        run_test("get-data 0 5 1 { X1:SIN_1 };\n", expect_units=True)
        run_test("get-online-data 5 1 { X1:SIN_1 };\n", use_unicode=True, expect_units=True)
        run_test("get-data 0 5 1 { X1:SIN_1 };\n", use_unicode=True, expect_units=True)


class TestGetChannelInfo(TestCase):
    def test_fail_with_online(self):
        s = DummySocket("")
        nds2 = setup_server(s)
        self.assertRaises(errors.DAQDSyntaxError, nds2.source._get_channel_info, ['X1:CHAN_1,online'], nds2.epoch_start,
                          nds2.epoch_stop)


class TestCheckData(TestCase):
    def do_test_check_frame_existence(self, fname):
        s = DummySocket("")
        lookup = dummy_disk_cache.DummyDiskCacheClient(
            (
                (fname,),
            ),
        )
        nds2 = setup_server(s, frame_lookup=lookup)
        nds2.source._check_frame_existence("X-X1_TEST", 1000000000, 1000000001)

    def do_test_check_frame_existance_fail(self, fname):
        s = DummySocket("")
        lookup = dummy_disk_cache.DummyDiskCacheClient(
            (
                (fname + "_nonexistant",),
            ),
        )
        nds2 = setup_server(s, frame_lookup=lookup)
        self.assertRaises(errors.DAQDException, nds2.source._check_frame_existence,
                          "X-X1_TEST", 1000000000, 1000000001)

    def do_test_check_data_good(self, fname):
        info = dummy_chan_info.DummyChanInfo(
            channels=([
                          {
                              'channel_name': 'X1:PROC-TEST_1',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_1,reduced,16',
                              'rate': 16.0,
                          }
                      ],),
            avails=(
                {
                    "X1:PROC-TEST_1,reduced,16": [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000000,
                            "stop": 1000000001,
                        }
                    ]
                },
            )
        )
        lookup = dummy_disk_cache.DummyDiskCacheClient(
            (
                (fname,),
            ),
        )
        out = run_server("""check-data 1000000000 1000000001 {X1:PROC-TEST_1};
""", info, lookup)
        expected = b"0000"
        self.assertEqual(out, expected)

    def do_test_check_data_bad(self, fname):
        info = dummy_chan_info.DummyChanInfo(
            channels=([
                          {
                              'channel_name': 'X1:PROC-TEST_1',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_1,reduced,16',
                              'rate': 16.0,
                          }
                      ],),
            avails=(
                {
                    "X1:PROC-TEST_1,reduced,16": [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000000,
                            "stop": 1000000001,
                        }
                    ]
                },
            )
        )
        lookup = dummy_disk_cache.DummyDiskCacheClient(
            (
                (fname,),
            ),
        )
        out = run_server("""check-data 999999999 1000000001 {X1:PROC-TEST_1};
""", info, lookup)
        self.assertEqual(out, b"000d")

    def do_test_check_read_error(self, frame1, frame2):
        pass

    def do_test_check_data_large_channel_list(self):
        chans = ["X1:CHANNEL_{0}".format(x) for x in range(600)]
        cmd = "check-data 999999999 1000000001 {" + " ".join(chans) + "};\n"
        out = run_server(cmd)
        self.assertEqual(out, b"0019")

    def test_check_data(self):
        with TempDir() as tmp:
            frame1 = create_aggrigate_frame1(tmp.get())
            self.do_test_check_frame_existence(frame1)
            self.do_test_check_frame_existance_fail(frame1)
            self.do_test_check_data_good(frame1)
            self.do_test_check_data_bad(frame1)
            self.do_test_check_data_large_channel_list()


class TestGetLastMessage(TestCase):

    def test_get_last_message(self):
        out = run_server("""get-last-message;
unknown-command;
get-last-message;
""")
        err_msg = b":Invalid command"
        expected = b"0000" + struct.pack("!I", 0) + b"0019"
        expected += b"0000" + struct.pack("!I", len(err_msg)) + err_msg
        self.assertEqual(out, expected)


class TestDataPlanCaching(TestCase):
    def test_cache_data_plan(self):
        input = ""
        s = DummySocket(input)
        chan_info = dummy_chan_info.DummyChanInfo(
            channels=([
                          {
                              'channel_name': 'X1:PROC-TEST_1',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_1,reduced,16',
                              'rate': 16.0,
                          },
                          {
                              'channel_name': 'X1:PROC-TEST_2',
                              'channel_type': 'reduced',
                              'data_type': 'real_4',
                              'full_name': 'X1:PROC-TEST_2,reduced,16',
                              'rate': 16.0,
                          },
                      ],
            ),
            avails=(
                {
                    'X1:PROC-TEST_1,reduced,16': [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000000,
                            "stop": 1000000004,
                        },
                    ],
                    'X1:PROC-TEST_2,reduced,16': [
                        {
                            "frame_type": "X-X1_TEST",
                            "start": 1000000000,
                            "stop": 1000000004,
                        },
                    ],
                },
            ),
        )
        server = setup_server(s, chan_info=chan_info)
        p1 = server.source.create_data_plan(1000000000, 1000000004, 4, ['X1:PROC-TEST_1', 'X1:PROC-TEST_2'])
        p2 = server.source.create_data_plan(1000000000, 1000000004, 4, ['X1:PROC-TEST_1', 'X1:PROC-TEST_2'])
        self.assertTupleEqual(p1, p2)

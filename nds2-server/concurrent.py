# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE


from ctypes import c_int32
from multiprocessing import dummy
from multiprocessing import Lock
from multiprocessing import Semaphore
from multiprocessing import Value


class DummyConncurrent(object):
    def __init__(self):
        self.get_data_semaphore = dummy.Semaphore(5)
        self._get_data_waiting = dummy.Value(c_int32, 0, lock=None)
        self._get_data_running = dummy.Value(c_int32, 0, lock=None)

    def get_data_waiting(self):
        pass

    def get_data_running(self):
        pass

    def get_data_done(self):
        pass

    def get_data_counts(self):
        return (0, 0)


class ProcessConcurrent(object):
    def __init__(self, setup):
        config = setup.split(':')
        max_get_data = int(config[0])

        self.get_data_semaphore = Semaphore(max_get_data)
        self._get_data_lock = Lock()
        self._get_data_waiting = Value(c_int32, 0, lock=None)
        self._get_data_running = Value(c_int32, 0, lock=None)

    def get_data_waiting(self):
        with self._get_data_lock:
            self._get_data_waiting.value += 1

    def get_data_running(self):
        with self._get_data_lock:
            self._get_data_waiting.value -= 1
            self._get_data_running.value += 1

    def get_data_done(self):
        with self._get_data_lock:
            self._get_data_running.value -= 1

    def get_data_counts(self):
        with self._get_data_lock:
            return self._get_data_waiting.value, self._get_data_running.value

_aux_concurrent_lookup = {}

def register_concurrent(key, concurrent):
    """
    Register a concurrent object on a specific uri.  Mostly for debugging
    :param key:  Key value
    :param concurrent: concurrent object
    :return: 
    """
    global _aux_concurrent_lookup
    _aux_concurrent_lookup[key] = concurrent


def concurrent_factory(uri):
    global _aux_concurrent_lookup
    if uri == "" or uri == "0" or uri is None:
        return DummyConncurrent()
    if uri in _aux_concurrent_lookup:
        return _aux_concurrent_lookup[uri]
    return ProcessConcurrent(uri)

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase


class TestLocationCache(TestCase):
    def test_location_cache(self):
        from ..location_cache import LocationCache
        lc = LocationCache()

        self.assertIsNone(lc.get('H-H1_R', 5))
        lc.add('H-H1_R', 0, 10, 'file1.gwf')

        self.assertEqual(lc.get('H-H1_R', 0), "file1.gwf")
        self.assertEqual(lc.get('H-H1_R', 5), "file1.gwf")
        self.assertIsNone(lc.get('H-H1_R', 10))
        self.assertIsNone(lc.get('H-H1_C', 10))

        lc.add('H-H1_R', 10, 20, 'file2.gwf')
        self.assertEqual(lc.get('H-H1_R', 10), "file2.gwf")

        lc.add('H-H1_C', 0, 10, 'file3.gwf')
        self.assertEqual(lc.get('H-H1_C', 0), "file3.gwf")
        self.assertEqual(lc.get('H-H1_C', 5), "file3.gwf")
        self.assertIsNone(lc.get('H-H1_C', 10))

        self.assertEqual(lc.get('H-H1_R', 5), "file1.gwf")

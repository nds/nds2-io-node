# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from .. import daqd


class DummyChanInfo:
    """The dummy chan info class acts as a testing/mock class.
    It is created with a series of responses and simply parrots
    back these responses, (consuming and discarding them as it goes)
    to its client"""

    def __init__(self, avails=(), avail_proxy=(),
                 source_proxy=(), channels=(),
                 channel_proxy=(), counts=(),
                 channels_args=(), channel_crc=()):
        self.avails = list(avails)
        self.avail_proxy = list(avail_proxy)
        self.source_proxy = list(source_proxy)
        self.channels = list(channels)
        self.channel_proxy = list(channel_proxy)
        self.counts = list(counts)
        self.channels_args = list(channels_args)
        self.channel_crcs = list(channel_crc)

    def __check_args(self, exp_args_stack, args):
        """
        Check a arguments tuple against a possible reference tuple
        :param exp_args_stack: A list of argument tuples.  The first
        entry (if present) is popped off and if it is not empty it is
        checked against the argument tuple.
        :param args: the arguments tuple to check
        :return: The list with the first entry removed, or []
        """
        if len(exp_args_stack) == 0:
            return []
        exp = exp_args_stack[0]
        result = exp_args_stack[1:]
        if len(exp) > 0:
            assert (len(exp) == len(args))
            for i in range(len(exp)):
                assert (exp[i] == args[i])
        return result

    def get_uri(self):
        """Get uri returns a pseudo random string that represents this object.
        It is provided so that a specific channel info handler can be provided
        for testing."""
        return str(hash(self))

    def connect(self):
        pass

    def close(self):
        pass

    def channel_avail_info(self, channels, gps, epoch_start, epoch_stop):
        data = self.avails[0]
        self.avails = self.avails[1:]
        return data

    def channel_avail_info_passthrough(self, channels, gps, epoch_start, epoch_stop, dest):
        data = self.avail_proxy[0]
        self.avail_proxy = self.avail_proxy[1:]
        dest.write(data)

    def channel_source_info_passthrough(self, channels, gps, epoch_start, epoch_stop, dest):
        data = self.source_proxy[0]
        self.source_proxy = self.source_proxy[1:]
        dest.write(data)

    def channel_crc(self):
        data = self.channel_crcs[0]
        self.avail_proxy = self.channel_crcs[1:]
        if len(data) != 4:
            raise Exception('invalid crc')
        return data

    def channel_list(self, pattern, epoch_start, epoch_stop,
                     channel_type="unknown"):
        self.channels_args = self.__check_args(self.channels_args,
                                               (pattern,
                                                epoch_start,
                                                epoch_stop,
                                                channel_type))
        data = self.channels[0]
        self.channels = self.channels[1:]
        return data

    def channel_list_passthrough(self, gps, channel_type, pattern,
                                 epoch_start, epoch_stop, dest):
        data = self.channel_proxy[0]
        self.channel_proxy = self.channel_proxy[1:]
        dest.write(data.encode())

    def channel_count_passthrough(self, gps, channel_type, pattern,
                      epoch_start, epoch_stop, dest):
        count = self.counts[0]
        self.counts = self.counts[1:]
        if count < 0:
            dest.write(daqd.DAQD_ERROR)
        else:
            dest.write(daqd.DAQD_OK)
            dest.write_int32(count)

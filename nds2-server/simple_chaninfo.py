# Copyright 2019 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import collections


simple_chan_buffer = collections.namedtuple('simple_chan_buffer', ['units', 'data'])


class data_types(object):
    INT16 = 0
    INT32 = 1
    INT64 = 2
    FLOAT32 = 3
    FLOAT64 = 4
    COMPLEX32 = 5
    UINT32 = 6
    UNKNOWN = 7

    __names = ["int_2", "int_4", "int_8", "real_4", "real_8", "complex_8", "uint_4", "unknown"]
    __sizes = [2, 4, 8, 4, 8, 8, 4, 0]

    @staticmethod
    def name(dtype):
        return data_types.__names[dtype]

    @staticmethod
    def size(dtype):
        return data_types.__sizes[dtype]

    @staticmethod
    def to_data_type(name):
        return data_types.__names.index(name)


class class_types(object):
    ONLINE = 0
    MTREND = 1
    STREND = 2
    RAW = 3
    REDUCED = 4
    TESTPT = 5
    STATIC = 6
    UNKNOWN = 7

    __names = ["online", "m-trend", "s-trend", "raw", "reduced", "test-pt", "static", "unknown"]

    @staticmethod
    def name(ctype):
        return class_types.__names[ctype]

    @staticmethod
    def to_class_type(name):
        return class_types.__names.index(name)

class simple_chan_info(object):
    """This is a base class to define the interface that the simple nds2 server
    uses to its channel list and object.

    This is the main abstraction that a callee must provide to the simple server.

    @:note The class methods may be called from multiple threads, it is the responsibility
    of the sub class designer to ensure that this is safe to do.
    """
    def __init__(self, name, data_type, class_type, sample_rate, signal_info):
        self.__name = name
        self.__signal_info = signal_info
        self.__data_type = data_type
        self.__class_type = class_type
        if self.__class_type == class_types.MTREND:
            self.__sample_rate = 1/60.
        elif self.__class_type == class_types.STREND:
            self.__sample_rate = 1.
        else:
            self.__sample_rate = sample_rate

    def name(self):
        """@:returns the signal name of the channel"""
        return self.__name

    def data_type(self):
        return self.__data_type

    def class_type(self):
        return self.__class_type

    def sample_rate(self):
        return self.__sample_rate

    def __getitem__(self, key):
        if key == 'rate':
            return self.__sample_rate
        raise KeyError("Unknown key {0}".format(key))

    def info(self):
        """@:returns the signal info of the channel as a signal_units object"""
        return self.__signal_info

    def rough_availability(self):
        """@:brief Return the rough availability of an object, this should be
        broad strokes, channel list changes.  It does not need to be detailed
        gap information.

        Return a list of [start, stop) tuples where the channel is expected to
        be available.
        @:note this must be overriden by subclasses, the default returns []
        @:note this will not be called for online channels"""
        return []

    def detailed_availability(self, start, stop):
        """@:brief Return the detailed availability for the channel.  This should handle
        all gaps.  Any time range returned should yeild good data
        @:returns a list of [start, stop) times.

        @:note this must be overriden by subclasses, the default returns []
        @:note this will not be called for online channels"""
        return []

    def on_tape(self, start, stop):
        """@:brief return true if the data should be viewed as being on tape
        @:returns true if it is on tape, else false
        @:note returns False by default"""
        return False

    def get_data(self, start, stop):
        """@:brief Return data for the channel
        @:param start the gps start time (inclusive) for the data
        @:param stop the gps stop time (exclusive) for the data
        @returns a simple_chan_buffer
        @note: the buffer must contain enough samples to fill [start, stop)"""
        raise NotImplementedError("The channel's data retrieval method is not implemented yet")

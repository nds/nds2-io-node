#!/usr/bin/env python

from __future__ import print_function

import nds2
import time

def check_dups(channels):
    cache = {}
    for entry in channels:
        if entry in cache:
            print("Dup found at ", entry)
        cache[entry] = entry

NDS_HOST, NDS_PORT = 'nds-alt.ligo.caltech.edu:31200'.split(':')
#NDS_HOST, NDS_PORT = 'nds-alt.ldas.cit:31200'.split(':')
#NDS_HOST, NDS_PORT = 'nds.ldas.cit:31200'.split(':')
#NDS_HOST, NDS_PORT = 'localhost:31200'.split(':')
NDS_PORT = int(NDS_PORT)

IFO = 'H1'
start = 1137203769
# start = 1159499968
stop = start + 4

conn = nds2.connection(NDS_HOST, NDS_PORT)
conn.set_epoch(start, stop)
channels = conn.find_channels('{}:*_DQ'.format(IFO))
print("Searching through {0} channels".format(len(channels)))
cur = 0
step = 500
while cur < len(channels):
    chans = [c.name for c in channels[cur:cur + step]]

    check_dups(chans)

    fetch_start = time.time()
    bufs = conn.fetch(start, stop, chans)
    print("Retrieved {0} buffers in {1}s\n".format(len(bufs), time.time()-fetch_start))
    samples = 0
    for buf in bufs:
        samples += buf.Samples()
    print("Retrieved {0} samples\n".format(samples))
    cur += step
print ("done")

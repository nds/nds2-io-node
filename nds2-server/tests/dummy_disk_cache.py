# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE


class DummyDiskCacheClient():
    """The dummy chan info class acts as a testing/mock class.
    It is created with a series of responses and simply parrots
    back these responses, (consuming and discarding them as it goes)
    to its client"""

    def __init__(self, filenames=()):
        self.filenames = list(filenames)

    def get_uri(self):
        """Get uri returns a pseudo random string that represents this object.
        It is provided so that a specific disk cache handler can be provided
        for testing."""
        return str(hash(self))

    def query(self, frame_type, gps_start, gps_stop):
        data = self.filenames[0]
        self.filenames = self.filenames[1:]
        return data

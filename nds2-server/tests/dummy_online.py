# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from .. import online_interface


class DummyOnline:
    def __init__(self):
        pass

    def get_uri(self):
        return ""


def create_online_object(path, sleep=None):
    def noop_sleep(ignored=None):
        pass

    if sleep is None:
        sleep = noop_sleep

    return online_interface.OnlineFiles({
        "LHO_Data": {
            "location": path,
            "prefix": "X-X1_realtime",
            "channels": {
                "X1:SIN_1": {
                    "data_type": "real_4",
                    "sample_rate": 16.0
                },
                "X1:SIN_2": {
                    "data_type": "real_4",
                    "sample_rate": 16.0
                }
            }
        }
    }, sleep)


def create_online_object_unicode(path, sleep=None):
    def noop_sleep(ignored=None):
        pass

    if sleep is None:
        sleep = noop_sleep

    return online_interface.OnlineFiles({
        "LHO_Data": {
            "location": path,
            "prefix": "X-X1_realtime",
            "channels": {
                "X1:SIN_1": {
                    "data_type": "real_4",
                    "sample_rate": 16.0
                },
                "X1:SIN_2": {
                    "data_type": "real_4",
                    "sample_rate": 16.0
                }
            }
        }
    }, sleep)

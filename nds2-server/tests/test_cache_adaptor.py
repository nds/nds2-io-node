# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase

from ..cache_adaptor import CacheAdaptor


class DummyCountingCache:
    def __init__(self):
        self.__get_count = {}
        self.__set_count = {}
        self.__cache = {}

    def set(self, key, value):
        self.__set_count[key] = self.__set_count.get(key, 0) + 1
        self.__cache[key] = value

    def get(self, key):
        self.__get_count[key] = self.__get_count.get(key, 0) + 1
        return self.__cache.get(key, None)

    def set_count(self, key):
        return self.__set_count.get(key, 0)

    def get_count(self, key):
        return self.__get_count.get(key, 0)

    def clear_contents(self):
        self.__cache = {}

    def max_block_size(self):
        return 1024


class TestCacheAdaptor(TestCase):
    def test_set(self):
        mc = DummyCountingCache()
        ca = CacheAdaptor(mc)
        k = 'one'
        v = 'onevalue'
        ca.set(k, v)
        self.assertEqual(mc.set_count(k), 1)
        self.assertEqual(mc.get(k), v)

    def test_get_set_w_local_key(self):
        mc = DummyCountingCache()
        ca = CacheAdaptor(mc)
        k = 'one'
        k2 = 'two'
        k3 = 'three'
        v = 'onevalue'
        v2 = 'twovalue'
        v3 = 'threevalue'
        ca.keep_local(k)
        ca.set(k, v)
        self.assertEqual(ca.get(k), v)
        mc.clear_contents()
        self.assertEqual(mc.get(k), None)
        self.assertEqual(ca.get(k), v)
        ca.set(k, v2)
        self.assertEqual(ca.get(k), v2)
        ca.keep_local(k)
        self.assertEqual(ca.get(k), v2)
        ca.keep_local(k2)
        self.assertEqual(ca.get(k2), None)

        ca.keep_local(k3)
        self.assertEqual(ca.get(k3), None)
        mc.set(k3, v3)
        self.assertEqual(ca.get(k3), v3)

    def test_get(self):
        mc = DummyCountingCache()
        ca = CacheAdaptor(mc)
        k = 'one'
        v = 'onevalue'
        mc.set(k, v)
        self.assertEqual(ca.get(k), v)
        self.assertEqual(mc.get_count(k), 1)

    def test_local_key(self):
        mc = DummyCountingCache()
        ca = CacheAdaptor(mc)
        self.assertEqual(ca.keep_local_key(), None)
        ca.keep_local('abc')
        self.assertEqual(ca.keep_local_key(), 'abc')
        ca.keep_local(None)
        self.assertEqual(ca.keep_local_key(), None)

    def test_max_block_size(self):
        mc = DummyCountingCache()
        ca = CacheAdaptor(mc)
        self.assertEqual(ca.max_block_size(), mc.max_block_size())

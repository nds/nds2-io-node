# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import time
from unittest import TestCase


class TestCacheEntry(TestCase):
    def test_cache_entry(self):
        from ..cache_entry import CacheEntry

        entry = CacheEntry(frame="foo", toc="bar")
        access_time1 = entry.access_time()
        self.assertEqual(entry.use_count(), 0)
        self.assertEqual(entry.total_use_count(), 0)

        self.assertTrue(abs(time.time() - access_time1) < 0.25)
        self.assertTupleEqual(("foo", "bar"), entry.get())
        self.assertEqual(entry.use_count(), 1)
        self.assertEqual(entry.total_use_count(), 1)
        access_time2 = entry.access_time()
        self.assertTrue(access_time2 > access_time1)

        entry.release()
        self.assertEqual(entry.use_count(), 0)
        self.assertEqual(access_time2, entry.access_time())

        entry.release()
        self.assertEqual(entry.use_count(), 0)

        entry.get()
        self.assertEqual(entry.total_use_count(), 2)
        entry.get()
        self.assertEqual(entry.total_use_count(), 3)
        self.assertEqual(entry.use_count(), 2)

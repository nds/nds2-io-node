# Copyright 2020 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import typing
from collections import namedtuple

from ..errors import DAQDException

RemoteProxyData = namedtuple('RemoteProxyData', ['frame_type', 'chan', 'start', 'stop', 'data', 'samples', 'units'])

class DummyRemoteProxy(object):

    def __init__(self, data_sets: typing.List[RemoteProxyData] = []):
        self.__data = data_sets

    def remote_data(self, frame_type, chan, start, stop):
        if len(self.__data) == 0:
            DAQDException("No more remote proxy data to send")
        next = self.__data[0]
        if next.frame_type != frame_type or next.chan["channel_name"] != chan["channel_name"] or \
            next.start != start or next.stop != stop:
            raise DAQDException("Unexpected request")
        self.__data = self.__data[1:]
        return next.data, next.samples, next.units

    def get_uri(self):
        """Get uri returns a pseudo random string that represents this object.
        It is provided so that a specific remote proxy can be provided
        for testing."""
        return str(hash(self))
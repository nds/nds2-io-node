# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import struct
from collections import namedtuple

signal_units = namedtuple('signal_units', ['offset', 'slope', 'units'])

__signal_units_fixed_length = struct.calcsize(b"!bff")


def default():
    return signal_units(0.0, 1.0, "counts")


def encode(units):
    unit_str = units.units[:40]
    unit_len = len(unit_str)
    return struct.pack("!b{0}sff".format(unit_len), unit_len, unit_str.encode(), units.offset, units.slope)


def decode_signal_unit_length(buffer):
    #return struct.unpack("!b", buffer[0])[0] + __signal_units_fixed_length
    return buffer[0] + __signal_units_fixed_length


def extract_signal_units(buffer):
    req_len = decode_signal_unit_length(buffer)
    str_len = req_len - __signal_units_fixed_length
    parts = struct.unpack("!b{0}sff".format(str_len), buffer[:req_len])
    return signal_units(parts[2], parts[3], parts[1].decode()), buffer[req_len:]

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase
import struct

from .. import signal_units
from ..frame_io import cache_key, save_block_to_cache

from .dummy_cache import DummyCacheTracking


class TestCacheKey(TestCase):
    def test_cache_key(self):
        ch1 = {'channel_name': 'H1:SYS_SOMETHING'}
        key = cache_key("H-H1_R", 1000000000, ch1, 0)
        self.assertEqual(key, "H-H1_R,1000000000,H1:SYS_SOMETHING,0")
        key = cache_key("H-H1_R", 1000000000, ch1, 5)
        self.assertEqual(key, "H-H1_R,1000000000,H1:SYS_SOMETHING,5")
        key = cache_key("L-L1_R", 1000000000, ch1, 5)
        self.assertEqual(key, "L-L1_R,1000000000,H1:SYS_SOMETHING,5")

        ch2 = {'channel_name': 'X1:SYS_SOMETHING_WITH_A_LONGER_NAME'}
        key = cache_key("X-online", 10000000000, ch2, 5)
        exp = "X-online,10000000000,X1:SYS_SOMETHING_WITH_A_LONGER_NAME,5"
        self.assertEqual(key, exp)


class TestSaveBlockToCache(TestCase):
    def test_save_block_to_cache(self):
        channel = {
            "channel_name": "H1:TEST",
            "data_type": "real_4",
            "rate": 1,
        }
        units = signal_units.default()
        cache = DummyCacheTracking()
        data = struct.pack("!f", 1.0) * 4
        off, residual = save_block_to_cache("H-H1_R", 100000000, channel, 4, data, 1000000004, units, cache)
        self.assertEqual(off, 1000000008)
        self.assertEqual(residual, b"")

        data = struct.pack("!f", 1.0) * 5
        off, residual = save_block_to_cache("H-H1_R", 100000000, channel, 4, data, 1000000004, units, cache)
        self.assertEqual(off, 1000000008)
        self.assertEqual(residual, struct.pack("!f", 1.0))

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import time


class CacheEntry(object):
    def __init__(self, frame, toc):
        self.__frame = frame
        self.__toc = toc
        self.__time = time.time()
        self.__use_count = 0
        self.__total_use_count = 0

    def use_count(self):
        return self.__use_count

    def total_use_count(self):
        return self.__total_use_count

    def get(self):
        self.__use_count += 1
        self.__total_use_count += 1
        self.__time = time.time()
        return self.__frame, self.__toc

    def release(self):
        if self.__use_count > 0:
            self.__use_count -= 1

    def access_time(self):
        return self.__time

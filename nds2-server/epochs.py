
def default_epoch():
    return {
        "ALL": (0, 1999999999),
        "ER2": (1025636416, 1028563232),
        "ER3": (1042934416, 1045353616),
        "ER4": (1057881616, 1061856016),
        "ER5": (1073606416, 1078790416),
        "ER6": (1102089216, 1102863616),
        "ER7": (1116700672, 1116800000),
        "ER8": (1123856384, 1123900032),
        "ER9": (1151848448, 1152169216),
        "ER10": (1163173888, 1164554240),
        "ER14": (1235746816, 1238163456),
        "O1": (1126621184, 1137254400),
        "O2": (1164554240, 1188081664),
        "O3":  (1238163456, 1269616640),
        "NONE": (0, 0),
        "S5": (815153408, 880920032),
        "S6": (930960015, 971654415),
        "S6a": (930960015, 935798415),
        "S6b": (937785615, 947203215),
        "S6c": (947635215, 961545615),
        "S6d": (961545615, 971654415),
    }


def parse_epochs(reader):
    epochs = {}
    for line in reader:
        txt = line.split('#')[0].strip()
        if txt == "":
            continue
        parts = txt.split()
        if len(parts) != 3:
            continue
        key = parts[1]
        if '-' in parts[2]:
            times = parts[2].split('-', 1)
            epochs[key] = (int(times[0]), int(times[1]))
        elif ':' in parts[2]:
            times = parts[2].split(':', 1)
            start = int(times[0])
            duration = int(times[1])
            epochs[key] = (start, start+duration)
    epochs['ALL'] = (0, 1999999999)
    epochs['NONE'] = (0, 0)
    return epochs


def cleanup_json_epochs(data):
    epochs = {}
    for key in data:
        epochs[key] = (int(data[key][0]), int(data[key][1]))
    epochs['ALL'] = (0, 1999999999)
    epochs['NONE'] = (0, 0)
    return epochs


def parse_epochs_from_file(fname:str):
    with open(fname, 'rt') as f:
        return parse_epochs(f)

#!/usr/bin/env python

# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from __future__ import print_function

import os.path
import time
import sqlite3

from flask import Flask, request, g, render_template
from jsonschema import validate

command_schema = {
    "type": "object",
    "properties": {
        "command": {"type": "string"},
        "pid": {"type": "number"},
    }
}

stat_schema = {
    "type": "array",
    "items": {
        "type": "object",
        "properties": {
            "key": {"type": "string"},
            "value": {"type": "number"},
        }
    },
    "minItems": 1,
    "uniqueItems": True,
}

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'stats.db'),
))


def connect_db():
    db = sqlite3.connect(app.config['DATABASE'])
    db.row_factory = sqlite3.Row
    return db


def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


def init_db():
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


@app.cli.command('initdb')
def initdb_command():
    """Initialize the database"""
    init_db()
    print("Inititialized the database.")


@app.teardown_appcontext
def close_db(error):
    """close the database at the end of the session"""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route('/')
def summary():
    db = get_db()
    commands = db.execute("SELECT max(id), * FROM recent_commands WHERE timestamp > ? GROUP BY server",
                          [time.time() - 3600])
    stats = db.execute("SELECT stat, value FROM stats ORDER BY stat ASC")
    # print(dir(request))
    return render_template('summary.html', commands=commands, stats=stats)


@app.route('/ws/running_command/', methods=['POST'])
def add_recent_command():
    db = get_db()
    timestamp = int(time.time())
    data = request.get_json(force=True)
    validate(data, command_schema)

    server = "{0}[{1}]".format(request.remote_addr, data['pid'])

    db.execute("insert into recent_commands (server, command, timestamp) values (?, ?, ?)",
               [server, data['command'][0:25], timestamp])
    db.commit()
    print("Added a command to the database {0} {1} {2}".format(
        request.remote_addr,
        data['command'],
        timestamp,
    ))
    return "ok"


@app.route('/ws/stat/', methods=['POST'])
def add_stat():
    db = get_db()
    data = request.get_json(force=True)
    validate(data, stat_schema)
    for entry in data:
        # try:
        print(entry)
        row = db.execute("SELECT id, value FROM stats WHERE stat = ?", [entry['key']]).fetchone()
        if row is None:
            db.execute("INSERT INTO stats (stat, value) values (?, ?)", [entry['key'], entry['value']])
        else:
            value = row['value'] + int(entry['value'])
            db.execute("UPDATE stats SET value = ? WHERE id = ?", [value, row['id']])
            # except:
            #
    db.commit()
    return "ok"

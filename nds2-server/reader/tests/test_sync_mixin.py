# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

from unittest import TestCase


class TestServerSyncMixin(TestCase):
    def test_server_sync_mixin(self):
        from ..frame_reader import SyncServerMixin

        server_mixin = SyncServerMixin()
        server_mixin.setup_sync_params(concurrent_jobs=16, cache_uri="a_cache")
        self.assertEqual(server_mixin.get_sync_param('concurrent_jobs'), 16)
        self.assertEqual(server_mixin.get_sync_param('cache_uri'), 'a_cache')
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 0)

        server_mixin.start_job()
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 1)
        server_mixin.start_job()
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 2)
        server_mixin.stop_job()
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 1)
        server_mixin.stop_job()
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 0)
        server_mixin.stop_job()
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 0)

    def test_server_mixin_mem_cache(self):
        from ..frame_reader import SyncServerMixin
        from ...tests.dummy_cache import DummyCache
        from ...memory_cache import register_memory_cache_lookup

        ref_cache = DummyCache()
        register_memory_cache_lookup(ref_cache.get_uri(), ref_cache)

        server_mixin = SyncServerMixin()
        server_mixin.setup_sync_params(concurrent_jobs=16, cache_uri=ref_cache.get_uri())

        cache = server_mixin.get_cache()
        self.assertEqual(cache, ref_cache)

    def test_server_mixin_run_job(self):
        from ..frame_reader import SyncServerMixin

        server_mixin = SyncServerMixin()
        server_mixin.setup_sync_params(concurrent_jobs=16, cache_uri='')

        def job():
            pass

        self.assertEqual(server_mixin.get_sync_param('jobs_waiting'), 0)
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 0)
        with server_mixin.run_job() as srv:
            self.assertEqual(srv, server_mixin)
            self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 1)
            self.assertEqual(server_mixin.get_sync_param('jobs_waiting'), 0)
        self.assertEqual(server_mixin.get_sync_param('jobs_in_process'), 0)

    def test_concurrent_job_limit(self):
        from ..frame_reader import SyncServerMixin
        from threading import Lock
        from threading import Thread
        import time

        max_concurrent = 16

        server_mixin = SyncServerMixin()
        server_mixin.setup_sync_params(concurrent_jobs=max_concurrent, cache_uri='')

        class JobStatus:
            def __init__(self, server):
                self.lock = Lock()
                self.server = server
                self.succeeded = False
                self.done = False
                self.max_concurrent = server.get_sync_param('concurrent_jobs')
                self.abort_time = time.time() + 2.0
                self.message = ""

            def fail(self, message=""):
                self.done = True
                self.succeeded = False
                self.message = "Error: {0}".format(message)

            def succeed(self):
                self.done = True
                self.succeeded = True
                self.message = ""

            def should_keep_going(self):
                with self.lock:
                    if self.done:
                        return False
                    if time.time() > self.abort_time:
                        self.fail("Timed out")
                    elif self.server.get_sync_param('jobs_in_process') > self.max_concurrent:
                        self.fail("too many jobs in process")
                    elif self.server.get_sync_param('jobs_waiting') > 2 and self.server.get_sync_param(
                            'jobs_in_process') == self.max_concurrent:
                        self.succeed()
                    return not self.done

            def is_done(self):
                with self.lock:
                    return self.done

            def get_success_status(self):
                with self.lock:
                    return self.succeeded, self.message

            def __call__(self):
                with self.server.run_job():
                    while self.should_keep_going():
                        pass

        job_test = JobStatus(server=server_mixin)
        for i in range(max_concurrent + 5):
            t = Thread(target=job_test)
            t.daemon = True
            t.start()
        while not job_test.is_done():
            pass
        success, msg = job_test.get_success_status()
        self.assertEqual(msg, "")
        self.assertTrue(success)

    def test_frame_cache_size(self):
        from ..frame_reader import SyncServerMixin

        sync_server1 = SyncServerMixin()
        sync_server1.setup_sync_params(concurrent_jobs=16, cache_uri="")
        self.assertEqual(sync_server1.get_sync_param("frame_cache_max_size"), 16)

        sync_server2 = SyncServerMixin()
        sync_server2.setup_sync_params(concurrent_jobs=8, cache_uri="")
        self.assertEqual(sync_server2.get_sync_param("frame_cache_max_size"), 8)

        sync_server3 = SyncServerMixin()
        sync_server3.setup_sync_params(concurrent_jobs=16, cache_uri="", frame_cache_max_size=32)
        self.assertEqual(sync_server3.get_sync_param("frame_cache_max_size"), 32)

        sync_server4 = SyncServerMixin()
        self.assertRaises(Exception, sync_server4.setup_sync_params, concurrent_jobs=16, cache_uri="",
                          frame_cache_max_size=5)

    def test_frame_cache(self):
        from ..frame_reader import SyncServerMixin
        from ...tests.tempdir import TempDir
        from ...tests.create_samples import create_aggregate_frame
        from ...tests.create_samples import create_sample_m_trend1

        max_cache_size = 3
        sync_server = SyncServerMixin()
        sync_server.setup_sync_params(concurrent_jobs=2, cache_uri='', frame_cache_max_size=max_cache_size)

        with TempDir() as tmp:
            ag1_path = create_aggregate_frame(tmp.get(), 1000000000, 128, 4)
            ag2_path = create_aggregate_frame(tmp.get(), 1000000128, 128, 4)
            ag3_path = create_aggregate_frame(tmp.get(), 1000000256, 128, 4)
            ag4_path = create_aggregate_frame(tmp.get(), 1000000512, 128, 4)
            trend1_path = create_sample_m_trend1(tmp.get())
            all_paths = [ag1_path, ag2_path, ag3_path, ag4_path, trend1_path]

            self.assertTrue(max_cache_size < len(all_paths))

            self.assertEqual(sync_server.get_sync_param('frame_cache_size'), 0)

            fr1, toc1 = sync_server.acquire_frame(ag1_path)
            self.assertEqual(sync_server.query_frame_cache(ag1_path), 1)
            self.assertEqual(sync_server.get_sync_param('frame_cache_size'), 1)

            fr1a, toc1a = sync_server.acquire_frame(ag1_path)
            self.assertEqual(sync_server.query_frame_cache(ag1_path), 2)
            self.assertEqual(sync_server.get_sync_param('frame_cache_size'), 1)
            self.assertEqual(fr1, fr1a)
            self.assertEqual(toc1, toc1a)

            sync_server.release_frame(ag1_path)
            self.assertEqual(sync_server.query_frame_cache(ag1_path), 1)
            sync_server.release_frame(ag1_path)
            self.assertEqual(sync_server.query_frame_cache(ag1_path), 0)
            sync_server.release_frame(ag1_path)
            self.assertEqual(sync_server.query_frame_cache(ag1_path), 0)

            # should be safe for things not in the cache
            sync_server.release_frame(ag2_path)
            self.assertEqual(sync_server.query_frame_cache(ag2_path), -1)

            for path in all_paths:
                sync_server.acquire_frame(path)
                sync_server.release_frame(path)
                self.assertEqual(sync_server.query_frame_cache(path), 0)
                self.assertLessEqual(sync_server.get_sync_param('frame_cache_size'), max_cache_size)
            self.assertEqual(sync_server.get_sync_param('frame_cache_size'), max_cache_size)

            for i in range(max_cache_size + 1):
                sync_server.frame_cache_eject_a_frame()
            self.assertEqual(sync_server.get_sync_param('frame_cache_size'), 0)

            for path in all_paths:
                # make a hot cache
                sync_server.acquire_frame(path)
                # hold on to the oldest frame
                if path != all_paths[0]:
                    sync_server.release_frame(path)
            self.assertEqual(sync_server.query_frame_cache(all_paths[0]), 1)
            sync_server.release_frame(all_paths[0])

            for i in range(max_cache_size):
                sync_server.acquire_frame(all_paths[i])
            self.assertEqual(sync_server.get_sync_param('frame_cache_size'), max_cache_size)
            self.assertRaises(Exception, sync_server.acquire_frame, all_paths[max_cache_size])
            for path in all_paths:
                sync_server.release_frame(path)

            self.assertTrue(sync_server.query_frame_cache(ag1_path) < 1)
            with sync_server.use_frame(ag1_path) as (fs, toc):
                self.assertEqual(sync_server.query_frame_cache(ag1_path), 1)
            self.assertTrue(sync_server.query_frame_cache(ag1_path) < 1)

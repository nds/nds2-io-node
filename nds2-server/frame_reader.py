# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import logging
import socket

from . import daqd
from .cache_adaptor import CacheAdaptor
from .errors import DAQDException
from .errors import DAQDFrameIOError
from .frame_io import read_from_frame
from .sock_io import BufferedSocket
from .helpers import optimize_frame_selection

try:
    import frameCPP
except ImportError:
    from LDAStools import frameCPP

_aux_frame_reader_lookup = {}


class local_frame_reader(object):
    def __init__(self):
        self.frame_cache = {}

    def get_frame(self, frame_info):
        """
        Return frame objects for the given frame
        :param frame_info: description of the frame to load
        :return: tuple (frame stream, frametoc)
        """
        fname = frame_info['filename']
        if fname is None:
            raise DAQDException(daqd.DAQD_ERROR)
        if fname in self.frame_cache:
            return self.frame_cache[fname]
        if len(self.frame_cache) > 1:
            del self.frame_cache[list(self.frame_cache.keys())[0]]
        fs = None
        optimized_name = optimize_frame_selection(fname)
        names = [optimized_name]
        if optimized_name != fname:
            names.append(fname)
        final_name = ''
        for file_name in names:
            try:
                # print("Opening frame '{0}' for '{1}'".format(file_name, fname))
                fs = frameCPP.IFrameFStream(file_name)
                final_name = file_name
                break
            except:
                pass
        if fs is None:
            raise DAQDFrameIOError("Unable to open frame '{0}'".format(fname))
        toc = fs.GetTOC()
        self.frame_cache[final_name] = (fs, toc)
        return fs, toc

    def read_from_frame(self, target_frame, chan, key, cache_start, max_block_size, mem_cache):
        cache = CacheAdaptor(mem_cache)
        fs, toc = self.get_frame(target_frame)
        cache.keep_local(key)
        read_from_frame(fs, target_frame['frame_type'], chan, target_frame['frame_start'],
                        cache_start, max_block_size, cache, toc)
        data = cache.get(key)
        cache.keep_local(None)
        return data

    def clear_cache(self):
        self.frame_cache = {}


class shared_frame_reader(object):
    def __init__(self, reader_socket):
        self.reader_socket = reader_socket
        self.logger = logging.getLogger(__name__+'frame_reader')

    def read_from_frame(self, target_frame, chan, key, cache_start, max_block_size, mem_cache):
        self.logger.info("Reading a shared frame")
        channel = "{0}%{1}%{2}%{3}".format(chan['channel_name'],
                                           chan['data_type'],
                                           chan['channel_type'],
                                           chan['rate'])
        cmd = ['read', target_frame['filename'], str(cache_start), '1', channel]
        cmdtext = "\t".join(cmd) + "\n"
        s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        s.connect(self.reader_socket)
        try:
            sb = BufferedSocket(s)
            sb.write(cmdtext.encode())
            dat_len = sb.read_int32()
            if dat_len == 0:
                raise DAQDFrameIOError("Unable to read data")
            data = sb.read(dat_len)
            sb.write(b"quit\n")
        finally:
            s.close()
        return data

    def clear_cache(self):
        pass


def register_frame_reader_lookup(key, frame_reader):
    """
    Register a frame reader object on a specific uri.  Mostly for debugging
    :param key:  Key value
    :param frame_reader: frame reader object
    :return: 
    """
    global _aux_frame_reader_lookup
    _aux_frame_reader_lookup[key] = frame_reader


def frame_reader_factory(uri):
    global _aux_frame_reader_lookup
    if uri == "":
        return local_frame_reader()
    if uri in _aux_frame_reader_lookup:
        return _aux_frame_reader_lookup[uri]
    return shared_frame_reader(uri)

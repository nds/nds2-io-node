# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import os.path

from unittest import TestCase

from .tempdir import TempDir
from .create_samples import create_sample_realtime_frame
from .dummy_online import create_online_object


class TestOnlineMappingVerification(TestCase):
    def test_verify_mapping(self):
        from ..online_interface import verify_mapping
        import tempfile

        verify_mapping({})
        self.assertRaises(Exception, verify_mapping, {"LHO_Data": {}})
        self.assertRaises(Exception, verify_mapping, {"LHO_Data": {
            "location": tempfile.gettempdir(),
            "prefix": "",
            "channels": "",
        }})
        self.assertRaises(Exception, verify_mapping, {"LHO_Data": {
            "location": tempfile.gettempdir(),
            "prefix": "X-X1_online",
            "channels": "",
        }})
        verify_mapping({"LHO_Data": {
            "location": tempfile.gettempdir(),
            "prefix": "X-X1_online",
            "channels": {},
        }})
        self.assertRaises(Exception, verify_mapping, {"LHO_Data": {
            "location": tempfile.gettempdir(),
            "prefix": "X-X1_online",
            "channels": {
                "invalid channel": {}
            },
        }})
        verify_mapping({"LHO_Data": {
            "location": tempfile.gettempdir(),
            "prefix": "X-X1_online",
            "channels": {
                "sample_channel": {
                    "sample_rate": 16.0,
                    "data_type": "real_4",
                }
            },
        }})
        self.assertRaises(Exception, verify_mapping, {"LHO_Data": {
            "location": tempfile.gettempdir(),
            "prefix": "X-X1_online",
            "channels": {
                "invalid channel": {
                    "data_type": "real_4",
                }
            },
        }})


class TestOnlineInterface(TestCase):

    def test_verify_channels(self):
        online = create_online_object("")
        online.verify_channels(['X1:SIN_1', ])
        online.verify_channels(['X1:SIN_1', 'X1:SIN_2'])
        online.verify_channels(['X1:SIN_1,online', ])
        online.verify_channels(['X1:SIN_1%16', ])
        online.verify_channels(['X1:SIN_1,16,online', ])
        self.assertRaises(Exception, online.verify_channels, ['X1:OFFLINE', ])
        self.assertRaises(Exception, online.verify_channels, ['X1:SIN_1,reduced', ])
        self.assertRaises(Exception, online.verify_channels, ['X1:SIN_1,32,online', ])

    def test_find_last_available_frame_and_get_latest_gps_time(self):
        def test_find_last_available_frame():
            latest = online._find_last_available_frame({
                'location': tmp.get(),
                'prefix': 'X-X1_realtime',
            })
            self.assertEqual(os.path.basename(latest), 'X-X1_realtime-1000000064-64.gwf')
            self.assertRaises(Exception,
                              online._find_last_available_frame,
                              {
                                  'location': tmp.get(),
                                  'prefix': 'H-MISSING',
                              })

        def test_get_latest_gps_time():
            gps = online._get_latest_gps_time({
                'location': tmp.get(),
                'prefix': 'X-X1_realtime',
            }, 0)
            self.assertEqual(gps, 1000000064)
            gps = online._get_latest_gps_time({
                'location': tmp.get(),
                'prefix': 'X-X1_realtime',
            }, 2000000000)
            self.assertEqual(gps, 2000000000)
            gps = online._get_latest_gps_time({
                'location': tmp.get(),
                'prefix': 'H-MISSING',
            }, 10000000)
            self.assertEqual(gps, 10000000)

        def test_get_frame_containing_gps():
            fname = online._get_frame_containing_gps({
                'location': tmp.get(),
                'prefix': 'X-X1_realtime',
                'cur_gps': 1000000008,
            })
            self.assertEqual(os.path.basename(fname), 'X-X1_realtime-1000000000-64.gwf')
            fname = online._get_frame_containing_gps({
                'location': tmp.get(),
                'prefix': 'X-X1_realtime',
                'cur_gps': 1000000100,
            })
            self.assertEqual(os.path.basename(fname), 'X-X1_realtime-1000000064-64.gwf')
            fname = online._get_frame_containing_gps({
                'location': tmp.get(),
                'prefix': 'X-X1_realtime',
                'cur_gps': 1000000200,
            })
            self.assertIsNone(fname)

        with TempDir() as tmp:
            online = create_online_object(tmp.get())
            for tmpname in ['X-X1_realtime-1000000000-64.gwf', 'X-X1_realtime-1000000064-64.gwf']:
                with open(os.path.join(tmp.get(), tmpname), 'wt') as ftmp:
                    ftmp.write(' ')
            test_find_last_available_frame()
            test_get_latest_gps_time()
            test_get_frame_containing_gps()

    def test_get_gps_start_for_plan(self):
        def choice_sleep(ignored=None):
            sleep_choice(ignored)

        def do_nothing(val):
            pass

        def seed(val):
            with open(os.path.join(tmp1.get(), 'X-X1_realtime-1000000000-64.gwf'), 'wt') as ftmp:
                ftmp.write(' ')

        sleep_choice = seed
        with TempDir() as tmp1:
            with TempDir() as tmp2:
                plan = {
                    'X1_Online': {
                        'location': tmp1.get(),
                        'prefix': 'X-X1_realtime',
                        'channels': [('chan1', 0), ]
                    },
                    'X2_Online': {
                        'location': tmp2.get(),
                        'prefix': 'X-X2_realtime',
                        'channels': [('chan2', 1), ]
                    },
                }
                online = create_online_object(tmp1.get(), choice_sleep)
                sleep_choice = do_nothing
                self.assertRaises(Exception, online.get_gps_start_for_plan, plan)
                sleep_choice = seed
                self.assertEqual(1000000000, online.get_gps_start_for_plan(plan))
                sleep_choice = do_nothing
                self.assertEqual(1000000000, online.get_gps_start_for_plan(plan))

    def test_get_online_segment_no_data(self):
        def do_nothing(val):
            pass

        with TempDir() as tmp:
            online = create_online_object(tmp.get(), sleep=do_nothing)
            channel_list = ['X1:SIN_1', ]
            plan, channel_info = online._resolve_channels(channel_list)
            self.assertRaises(Exception, online.get_segment, plan, channel_info, 0, 1)

    def test_get_online_segment(self):
        from .. import data_buffer
        from . import dummy_cache

        def seed_files(ignored=None):
            create_sample_realtime_frame(tmp.get(), params['cur_gps_time'], params['time_step'])
            params['cur_gps_time'] += params['time_step']

        def do_nothing(ignored=None):
            pass

        def choice_sleep(ignored=None):
            sleep_choice(ignored)

        sleep_choice = seed_files

        params = {'cur_gps_time': 1000000000, 'time_step': 4}
        with TempDir() as tmp:
            online = create_online_object(tmp.get(), choice_sleep)
            channel_list = ['X1:SIN_1', ]

            # start with no files
            with data_buffer.Closer() as out_bufs:
                out_bufs.add(data_buffer.DataBuffer(1 * 16 * 4))
                cache = dummy_cache.DummyCacheTracking()
                plan, channel_info = online._resolve_channels(channel_list)
                sleep_choice = seed_files
                online.get_segment(plan, channel_info, 0, 1, cache, out_bufs)

            # start with existing files
            with data_buffer.Closer() as out_bufs:
                out_bufs.add(data_buffer.DataBuffer(1 * 16 * 4))
                cache = dummy_cache.DummyCacheTracking()
                plan, channel_info = online._resolve_channels(channel_list)
                sleep_choice = seed_files
                online.get_segment(plan, channel_info, 0, 1, cache, out_bufs)

            # existing files, but no updates
            with data_buffer.Closer() as out_bufs:
                out_bufs.add(data_buffer.DataBuffer(5 * 16 * 4))
                cache = dummy_cache.DummyCacheTracking()
                plan, channel_info = online._resolve_channels(channel_list)
                sleep_choice = do_nothing
                self.assertRaises(Exception, online.get_segment, plan, channel_info, 0, 5, cache, out_bufs)

        return
        # this is wrong, I don't know what the return type should
        # be yet.  Need to have something where we can give back
        # a buffer, or set of buffers, and ensure that they get closed
        # buffer, gps = online.get_segment(channel_list, 0, 1)

    def test_create_online_object(self):
        self.assertIsNotNone(create_online_object(""))

    def test_resolve_channels(self):
        """This tests an internal interface"""
        online = create_online_object("")
        channel_list = ['X1:SIN_1', ]
        out = online._resolve_channels([])
        self.assertDictEqual({}, out[0])
        self.assertTupleEqual((), out[1])
        dat = online._resolve_channels(channel_list)
        expected = ({
            'LHO_Data': {
                'location': '',
                'prefix': 'X-X1_realtime',
                'channels': [
                    ('X1:SIN_1', 0),
                ],
            },
                    },
                    (
                        {
                            'channel_name': 'X1:SIN_1',
                            'rate': 16.0,
                            'data_type': 'real_4',
                            'channel_type': 'online',
                        },
                    ))
        self.assertDictEqual(expected[0], dat[0])
        self.assertTupleEqual(expected[1], dat[1])

    def test_get_plan(self):
        online = create_online_object("")
        channel_list = ['X1:SIN_1', ]
        out, stride = online.get_plan([], 1)
        self.assertDictEqual({}, out[0])
        self.assertTupleEqual((), out[1])
        self.assertEqual(stride, 1)
        dat, stride = online.get_plan(channel_list, 1)
        expected = ({
                        'LHO_Data': {
                            'location': '',
                            'prefix': 'X-X1_realtime',
                            'channels': [
                                ('X1:SIN_1', 0),
                            ],
                        },
                    },
                    (
                        {
                            'channel_name': 'X1:SIN_1',
                            'rate': 16.0,
                            'data_type': 'real_4',
                            'channel_type': 'online',
                        },
                    ))
        self.assertDictEqual(expected[0], dat[0])
        self.assertTupleEqual(expected[1], dat[1])
        self.assertEqual(stride, 1)

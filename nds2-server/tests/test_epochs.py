from unittest import TestCase
from tempfile import TemporaryDirectory
import os.path

from .. import epochs

class TestEpochFile(TestCase):
    defaults = [entry + "\n" for entry in """#
#  This file contains definitions of symbolic name of Ligo epochs for use by
#  the nds2 server. The time format may be either 
#      <start>-<end>
#  or
#     <start>:<duration>
#
#  The epoch names can be any string starting with a non-numeric character.
#  ALL (0-inf) and NONE (0:0) are defined by default.
#
epoch.define S5   815153408-880920032
epoch.define S6   930960015-971654415
epoch.define S6a  930960015-935798415
epoch.define S6b  937785615-947203215
epoch.define     S6c  947635215-961545615
epoch.define S6d  961545615-971654415
epoch.define ER2 1025636416-1028563232
epoch.define ER3       1042934416-1045353616
epoch.define ER4 1057881616-1061856016
epoch.define ER5 1073606416-1078790416
    epoch.define     ER6 1102089216-1102863616
epoch.define ER7 1116700672-1116800000
epoch.define ER8 1123856384-1123900032
epoch.define O1  1126621184-1137254400


epoch.define ER9 1151848448-1152169216
epoch.define ER10 1163173888-1164554240
epoch.define O2  1164554240-1188081664
epoch.define ER14 1235746816-1238163456

epoch.define O3  1238163456-1269616640

""".split('\n')]

    defaults_expected = {
        "ALL": (0, 1999999999),
        "NONE": (0, 0),
        "S5":  (815153408, 880920032),
        "S6": (930960015, 971654415),
        "S6a": (930960015, 935798415),
        "S6b": (937785615, 947203215),
        "S6c": (947635215, 961545615),
        "S6d": (961545615, 971654415),
        "ER2": (1025636416, 1028563232),
        "ER3": (1042934416, 1045353616),
        "ER4": (1057881616, 1061856016),
        "ER5": (1073606416, 1078790416),
        "ER6": (1102089216, 1102863616),
        "ER7": (1116700672, 1116800000),
        "ER8": (1123856384, 1123900032),
        "O1":  (1126621184, 1137254400),
        "ER9": (1151848448, 1152169216),
        "ER10": (1163173888, 1164554240),
        "O2": (1164554240, 1188081664),
        "ER14": (1235746816, 1238163456),
        "O3": (1238163456, 1269616640),
    }

    def test_parse_epochs_from_file(self):
        with TemporaryDirectory() as base:
            fname = os.path.join(base, "epochs.txt")
            with open(fname, 'wt') as f:
                f.write("\n".join(TestEpochFile.defaults))
            results = epochs.parse_epochs_from_file(fname)
            self.assertDictEqual(TestEpochFile.defaults_expected, results, "expected and actual must match")

    def test_parse_epochs(self):
        results = epochs.parse_epochs(TestEpochFile.defaults)
        self.assertDictEqual(TestEpochFile.defaults_expected, results, "expected and actual must match")

    def test_cleanup_json_epochs(self):
        test_cases = [
            {
                "input": {},
                "expected": {
                    "ALL": (0, 1999999999),
                    "NONE": (0, 0),
                }
            },
            {
                "input": {
                    "ALL": [0, 1999999999],
                    "NONE": [0, 0],
                    "S5":  [815153408, 880920032],
                    "S6": [930960015, 971654415],
                    "S6a": [930960015, 935798415],
                    "S6b": [937785615, 947203215],
                },
                "expected": {
                    "ALL": (0, 1999999999),
                    "NONE": (0, 0),
                    "S5":  (815153408, 880920032),
                    "S6": (930960015, 971654415),
                    "S6a": (930960015, 935798415),
                    "S6b": (937785615, 947203215),
                }
            },
        ]
        for test_case in test_cases:
            results = epochs.cleanup_json_epochs(test_case["input"])
            self.assertDictEqual(test_case["expected"], results, "test case must match")



    def test_default_epoch(self):
        expected = TestEpochFile.defaults_expected
        actual = epochs.default_epoch()

        self.assertDictEqual(expected, actual, "expected and actual must match")

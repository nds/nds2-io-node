# Copyright 2023 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

import struct

from unittest import TestCase

from .create_samples import create_aggregate_64k_frame
from .dummy_cache import DummyCacheLikeaSeive
from .tempdir import TempDir
from ..frame_io import cache_key
from ..frame_reader import local_frame_reader
from ..helpers import seconds_to_samples, samples_to_bytes
from ..signal_units import extract_signal_units
from ..signal_units import decode_signal_unit_length
from ..signal_units import signal_units



def cache_validator1(key, value):
    parts = key.split(',')
    chan = parts[2]
    if chan != "X1:PROC-TEST_64K":
        return
    frame_start = int(parts[1])
    offset = int(parts[3])
    unit_len = decode_signal_unit_length(value)
    data_len = len(value) - unit_len
    # print(f"cache_validator1 - {key}  samples: {data_len // 8}, bytes: {data_len}")
    data = struct.unpack(f">d", value[unit_len:unit_len + 8])[0]
    expected = float(frame_start + offset)
    if data != expected:
        raise AssertionError(f"expected {expected} got {data}")



class TestLocalFrameReader(TestCase):

    def test_local_frame_reader(self):
        with TempDir() as tmp:
            frame1 = create_aggregate_64k_frame(tmp.get())
            self.do_read_all_frame(frame1)

    def do_read_all_frame(self, frame1):
        target_frame = {'frame_type': 'X-X1_TEST',
                        'frame_start': 1100000000,
                        'frame_stop': 1100000128,
                        'gps_start': 1100000000,
                        'gps_stop': 1100000128,
                        'filename': frame1,
                        'url': None}
        chan = {'channel_name': 'X1:PROC-TEST_64K',
                'channel_type': 'reduced',
                'data_type': 'real_8',
                'full_name': 'X1:PROC-TEST_64K,reduced,65536', 'rate': 65536.0,
                'units': signal_units(offset=0.0, slope=1.0, units='')}
        max_block_size = 2
        mem_cache = DummyCacheLikeaSeive(data_validator=cache_validator1)
        reader = local_frame_reader()

        for start in range(target_frame['frame_start'], target_frame['frame_stop'], 2):
            gps_offset = start - target_frame['frame_start']
            # the cache_ values are relative to the start of the frame
            if gps_offset % max_block_size == 0:
                cache_start = gps_offset
                cache_offset_samples = 0
            else:
                cache_start = int(gps_offset / max_block_size) * max_block_size
                cache_offset_samples = seconds_to_samples(
                    chan, gps_offset % max_block_size)
            cache_offset_bytes = samples_to_bytes(chan, cache_offset_samples)
            cache_end = cache_start + max_block_size
            key = cache_key(target_frame['frame_type'],
                            target_frame['frame_start'], chan, cache_start)

            print(f"{start}\n{target_frame}\n{chan}\n{key}\n{cache_start}")
            data = reader.read_from_frame(target_frame, chan, key, cache_start, max_block_size, mem_cache)
            print("")
            self.assertIsNotNone(data, "data should not be None")
            print(f"got {len(data)} bytes back")
            unit_len = decode_signal_unit_length(data)
            self.assertEqual(2*8*64*1024 + unit_len, len(data),
                             "should have one cache block returned + encoded signal units")
            units, raw_data = extract_signal_units(data)
            values = struct.unpack(">131072d", raw_data)
            for i in range(2):
                offset = i * 64 * 1024
                expected = float(target_frame['frame_start'] + cache_start + i)
                for sample in range(64*1024):
                    if values[offset + sample] != expected:
                        raise AssertionError(f"Wrong data value read back from frame wanted {expected} got {values[offset + sample]}")
# Copyright 2016 California Institute of Technology
#
# You should have received a copy of
# the licensing terms for this software included
# in the file "LICENSE" located in the top level directory
# of this package.  If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE


class DummySocket:
    def __init__(self, input):
        self.__in_buffer = input
        self.__out_buffer = b''

    def recv(self, count):
        if len(self.__in_buffer) <= 0:
            raise Exception("Out of test data")
        if count > len(self.__in_buffer):
            count = len(self.__in_buffer)
        val = self.__in_buffer[:count]
        self.__in_buffer = self.__in_buffer[count:]
        return val

    def sendall(self, data):
        self.__out_buffer += data

    def out_buffer(self):
        return self.__out_buffer
